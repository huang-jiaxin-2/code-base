#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
//#include <opencv2/opencv.hpp>
#include <winsock2.h>//套接字头文件
#include <windows.h>
#pragma comment(lib, "ws2_32.lib")//套接字库
#pragma warning(disable:4996)
#pragma warning(disable:6386)
//using namespace cv;
using namespace std;


int main()
{	
	//初始化
	WORD sockVersion = MAKEWORD(2, 2);
	WSADATA wsdata;
	if (WSAStartup(sockVersion, &wsdata) != 0)
	{
		return 1;
	}

	//创建套接字
	SOCKET clientSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (clientSocket == INVALID_SOCKET)
	{
		cout << "Socket error" << endl;
		return 1;
	}

	sockaddr_in sockAddr;
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_port = htons(8888);
	sockAddr.sin_addr.S_un.S_addr = inet_addr("10.16.0.99");//127.0.0.1


		char buffer[1024];
		int ret = 0;
		while (true)
		{
			connect(clientSocket, (SOCKADDR*)&sockAddr, sizeof sockAddr);
			recv(clientSocket, buffer, sizeof buffer, 0);
			ret = atoi(buffer);

			if (ret == 1)
			{
				send(clientSocket, "关机成功", 8, 0);
				cout << "你的电脑将在60秒后关机" << endl;
				system("shutdown -s -t 60");
			}
			else if (ret == 2)
			{
				send(clientSocket, "休眠成功", 8, 0);
				system("shutdown -h");
				break;
			}
			else if (ret == 3)
			{
				send(clientSocket, "重启成功", 8, 0);
				system("shutdown -r -t 60");
				break;
			}
			else if (ret == 5)
			{
				send(clientSocket, "取消成功", 8, 0);
				system("shutdown -a");
				cout << "关机已取消" << endl;
			}
			else if (ret == 6)
			{
				/*send(clientSocket, "相机打开成功", 8, 0);
				cout << "你的相机已被打开" << endl;
				VideoCapture cap(0);
				Mat frame;
				while (1)
				{
					cap >> frame;
					imshow("调用摄像头", frame);
					waitKey(30);
				}*/
			}
			//system("pause");
		}


	//回收资源
	WSACleanup();
	return 0;
}