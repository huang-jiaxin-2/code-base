#include <iostream>
#include <vector>
using namespace std;

//void qsort(vector<int>& nums, int begin, int end)
//{
//	if (begin >= end) return;
//	int left = begin, right = end;
//	int keyi = left;
//	while (left < right)
//	{
//		while (left < right && nums[right] >= nums[keyi]) right--;
//		while (left < right && nums[left] <= nums[keyi]) left++;
//		
//		swap(nums[left], nums[right]);
//	}
//	swap(nums[keyi], nums[left]);
//	keyi = left;
//	qsort(nums, begin, keyi - 1);
//	qsort(nums, keyi + 1, end);
//}

void qsort(vector<int>& nums, int begin, int end)
{
	if (begin >= end) return;
	int prev = begin, cur = prev + 1;
	int keyi = begin;
	while (cur <= end)
	{
		if (nums[cur] <= nums[keyi] && ++prev != cur)
		{
			swap(nums[prev], nums[cur]);
		}

		cur++;
	}

	swap(nums[prev], nums[keyi]);
	keyi = prev;
	qsort(nums, begin, keyi - 1);
	qsort(nums, keyi + 1, end);
}

int main()
{
	vector<int> nums = { 1, 4, 2, 3, 5, 7, 6, 8, 9 };
	qsort(nums, 0, 8);

	for (auto num : nums)
	{
		cout << num << " ";
	}
	return 0;
}