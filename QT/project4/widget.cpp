#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QPushButton>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

//    connect(this, &Widget::MySignal1, this, &Widget::MySlot1);
//    connect(this, &Widget::MySignal1, this, &Widget::MySlot2);
//    connect(this, &Widget::MySignal1, this, &Widget::MySlot3);
    //connect(ui->pushButton, &QPushButton::clicked, this, &Widget::handleClicked);

    QPushButton* button = new QPushButton(this);
    button->setText("按钮3");
    //button->move(200, 200);
    connect(button, &QPushButton::clicked, this, [=](){
        qDebug() << "lambda 被执行了";
        button->move(100, 100);
        this->setWindowTitle("lambda");

    });
}

Widget::~Widget()
{
    delete ui;
}

void Widget::MySlot1()
{
    qDebug() << "MySlot1";
}

void Widget::MySlot2()
{
    qDebug() << "MySlot2";
}

void Widget::MySlot3()
{
    qDebug() << "MySlot3";
}

void Widget::handleClicked()
{
    this->setWindowTitle("设置标题1");
    qDebug() << "clicked1";
}

void Widget::handleClicked2()
{
    this->setWindowTitle("设置标题2");
    qDebug() << "clicked2";
}


void Widget::on_pushButton_2_clicked()
{
    disconnect(ui->pushButton, &QPushButton::clicked, this, &Widget::handleClicked);
    connect(ui->pushButton, &QPushButton::clicked, this, &Widget::handleClicked2);
}
