#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    connect(this, &Widget::mySignal, this, &Widget::headelMySignal);

}

Widget::~Widget()
{
    delete ui;
}

void Widget::headelMySignal(const QString& text)
{
    this->setWindowTitle(text);
}


void Widget::on_pushButton_clicked()
{
    emit mySignal("设置标题为标题1");
}

void Widget::on_pushButton_2_clicked()
{
    emit mySignal("设置标题为标题2");
}
