#include "widget.h"
#include "ui_widget.h"
#include <QPushButton>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

//    QPushButton* button = new QPushButton(this);
//    button->setText("关闭");
//    button->move(200, 200);

    //connect(button, &QPushButton::clicked, this, &QWidget::close);
//    connect(button, &QPushButton::clicked, this, &Widget::handleClicked);
    //connect(ui->pushButton, &QPushButton::clicked, this, &Widget::handleClicked);

    connect(this, &Widget::mySignal, this, &Widget::handleMySignal);

//    emit mySignal();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::handleClicked()
{
    this->close();
}


void Widget::on_pushButton_clicked()
{
//    this->setWindowTitle("按钮已经被按下");
    emit mySignal("设置标题为标题1");
}

void Widget::handleMySignal(const QString& text)
{
    this->setWindowTitle(text);
}

void Widget::on_pushButton_2_clicked()
{
    emit mySignal("设置标题为标题2");
}
