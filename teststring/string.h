#pragma once
#include <iostream>
#include <assert.h>
#include <algorithm>
using namespace std;

class MyString
{
public:
	//迭代器
	typedef char* iterator;
	typedef char* const_iterator;

	iterator begin()
	{
		return _str;
	}

	iterator end()
	{
		return _str + _size;
	}

	const_iterator begin()const
	{
		return _str;
	}

	const_iterator end()const
	{
		return _str + _size;
	}

	//构造函数
	MyString(const char* str = "")
	{
		_size = strlen(str);
		_capacity = _size;
		_str = new char[_capacity + 1];

		strcpy(_str, str);
	}

	////拷贝构造——传统写法
	//MyString(const MyString& s)
	//	:_str(new char[s._capacity + 1])
	//	,_size(s._size)
	//	,_capacity(s._capacity)
	//{
	//	strcpy(_str, s._str);
	//}

	////赋值 s = s2——传统写法
	//MyString& operator=(const MyString& s)
	//{
	//	if (this != &s)
	//	{
	//		char* tmp = new char[s._capacity + 1];
	//		strcpy(tmp, s._str);
	//		delete[] _str;
	//		_str = tmp;
	//		_size = s._size;
	//		_capacity = s._capacity;
	//	}

	//	return *this;
	//}

	void swap(MyString& tmp)
	{
		::swap(_str, tmp._str);
		::swap(_size, tmp._size);
		::swap(_capacity, tmp._capacity);
	}

	//拷贝构造——现代写法
	MyString(const MyString& s)
		:_str(nullptr)
		, _size(0)
		, _capacity(0)
	{
		MyString tmp(s._str);//调用构造
		swap(tmp);
	}

	//赋值 s = s2——现代写法
	//MyString& operator=(const MyString& s)
	//{
	//	MyString tmp(s);//调用拷贝构造
	//	swap(tmp);
	//	
	//	return *this;
	//}

	//现代写法
	MyString& operator=(MyString s)
	{
		swap(s);
		return *this;
	}

	size_t size()const
	{
		return _size;
	}

	const char& operator[](size_t pos)const
	{
		assert(pos < _size);
		return _str[pos];
	}

	char& operator[](size_t pos)
	{
		assert(pos < _size);
		return _str[pos];
	}

	const char* c_str()const 
	{
		return _str;
	}

	void reserve(size_t n)
	{
		if (n > _capacity)
		{
			char* tmp = new char[n + 1];//'\0'
			strcpy(tmp, _str);
			delete[] _str;
			_str = tmp;
			_capacity = n;
		}
	}

	void resize(size_t n, char ch = '\0')
	{
		if (n > _size)
		{
			//插入数据
			reserve(n);
			for (size_t i = _size; i < n; i++)
			{
				_str[i] = ch;
			}
			_str[n] = '\0';
		}
		else
		{
			//删除数据
			_str[n] = '\0';
			_size = n;
		}
	}

	void push_back(char ch)
	{
		if (_size == _capacity)
		{
			reserve(_capacity == 0 ? 4 : _capacity * 2);
		}
		_str[_size++] = ch;
		_str[_size] = '\0';//一定要把'\0'加上
	}

	MyString& operator+=(char ch) 
	{
		push_back(ch);
		return *this;
	}

	void append(const char* str)
	{
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		strcpy(_str + _size, str);
		_size += len;
	}

	MyString& operator+=(const char* str)
	{
		append(str);
		return *this;
	}

	void append(const MyString& s)
	{
		append(s._str);
	}

	void append(size_t n, char ch)
	{
		reserve(_size + n);
		for (int i = 0; i < n; i++)
		{
			push_back(ch);
		}
	}

	MyString& insert(size_t pos, char ch)
	{
		assert(pos <= _size);
		if (_size == _capacity)
		{
			reserve(_capacity == 0 ? 4 : _capacity * 2);
		}

		//挪动数据
		size_t end = _size + 1;
		while (end > pos)
		{
			_str[end] = _str[end - 1];
			end--;
		}

		_str[pos] = ch;
		_size++;
		return *this;
	}

	MyString& insert(size_t pos, const char* str)
	{
		assert(pos <= _size);
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}

		//挪动数据
		size_t end = _size + len;
		while (end >= pos + len)
		{
			_str[end] = _str[end - len];
			end--;
		}
		strncpy(_str + pos, str, len);
		_size += len;
		return *this;
	}

	void erase(size_t pos, size_t len = npos)
	{
		assert(pos < _size);
		if (len == npos || pos + len >= _size)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			strcpy(_str + pos, _str + pos + len);
			_size -= len;
		}
	}

	size_t find(char ch, size_t pos = 0)const
	{
		assert(pos < _size);
		for (size_t i = pos; i < _size; i++)
		{
			if (ch == _str[i])
			{
				return i;
			}
		}
		return npos;
	}

	size_t find(const char* sub, size_t pos = 0)const
	{
		assert(sub);
		assert(pos < _size);
		const char* ptr = strstr(_str + pos, sub);
		if (ptr == nullptr)
		{
			return npos;
		}
		else
		{
			return ptr - _str;//通过指针-指针的方式来获取下标
		}
	}

	//提取子串
	MyString substr(size_t pos, size_t len = npos)const
	{
		assert(pos < _size);
		size_t realLen = len;
		if (len == npos || len + pos >= _size)
		{
			realLen = _size - pos;
		}
		MyString str;
		for (size_t i = 0; i < realLen; i++)
		{
			str += _str[i + pos];
		}
		return str;
	}

	void clear()
	{
		_str[0] = '\0';
		_size = 0;
	}

	bool operator > (const MyString& s)const
	{
		return strcmp(_str, s._str) > 0;
	}

	bool operator == (const MyString& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool operator >= (const MyString& s)const
	{
		return *this > s || *this == s;
	}

	bool operator <= (const MyString& s)const
	{
		return !(*this > s);
	}

	bool operator < (const MyString& s)const
	{
		return !(*this >= s);
	}

	bool operator != (const MyString& s)const
	{
		return !(*this == s);
	}

	//析构函数
	~MyString()
	{
		delete[] _str;
		_str = nullptr;
		_size = _capacity = 0;
	}
private:
	char* _str;
	size_t _size;//有效数据的个数
	size_t _capacity;//容量

public:
	//const static特殊直接可以当成定义初始化
	const static size_t npos = -1;
};

ostream& operator<<(ostream& out, const MyString& s)
{
	for (int i = 0; i < s.size(); i++)
	{
		out << s[i];
	}
	return out;
}

istream& operator>>(istream& in, MyString& s)
{
	s.clear();
	char ch;
	ch = in.get();
	const size_t N = 64;
	char buff[N];
	size_t i = 0;
	while (ch != ' ' && ch != '\n')
	{
		buff[i++] = ch;
		if (i == N - 1)
		{
			buff[i] = '\0';
			s += buff;
			i = 0;
		}
		ch = in.get();
	}

	buff[i] = '\0';
	s += buff;
	return in;
}