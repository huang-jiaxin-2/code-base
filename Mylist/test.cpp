#include "list.h"
#include <algorithm>

struct Coord
{
	Coord(int a1 = 0, int a2 = 0)
		:_a1(a1)
		,_a2(a2)
	{}
	int _a1;
	int _a2;
};

//int main()
//{
//	hjx::list<Coord> lt;
//	lt.push_back(Coord(10, 20));//匿名对象
//	lt.push_back(Coord(21, 11));//匿名对象
//	auto it = lt.begin();
//	while (it != lt.end())
//	{
//		cout << it->_a1 << ":" << it->_a2 << endl;
//		it++;
//	}
//	return 0;
//}

int main()
{
	hjx::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	hjx::list<int> lt1(lt);
	lt1 = lt;
	hjx::list<int>::iterator it = lt1.begin();
	
	while (it != lt1.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	for (auto e : lt)
	{
		cout << e << " ";
	}
	return 0;
}