#pragma once
#include <iostream>
#include <assert.h>
#include <algorithm>

using namespace std;

//套用命名空间
namespace hjx
{
	template<class T>
	struct list_node
	{
		T _data;
		list_node<T>* _next;
		list_node<T>* _prev;

		list_node(const T& x = T())
			:_data(x)
			, _next(nullptr)
			, _prev(nullptr)
		{}
	};

	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef list_node<T> Node;
		typedef __list_iterator<T, Ref, Ptr> iterator;

		Node* _node;

		__list_iterator(Node* node)
			:_node(node)
		{}

		bool operator!=(const iterator& it)const
		{
			return _node != it._node;
		}

		bool operator==(const iterator& it)const
		{
			return _node == it._node;
		}

		//*it
		Ref operator*()
		{
			return _node->_data;
		}

		//it->
		Ptr operator->()
		{
			return &(operator*());
		}

		//++it
		iterator& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		//it++
		iterator& operator++(int)
		{
			iterator tmp(*this);
			_node = _node->_next;
			return tmp;
		}

		//--it
		iterator& operator--()
		{
			_node = _node->_prev;
			return *this;
		}

		//it--
		iterator& operator--(int)
		{
			iterator tmp(*this);
			_node = _node->_prev;
			return tmp;
		}
	};

	template<class T>
	class list
	{
		typedef list_node<T> Node;
	public:
		//迭代器
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;
		iterator begin()
		{
			return iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);
		}

		const_iterator begin()const
		{
			return const_iterator(_head->_next);
		}

		const_iterator end()const 
		{
			return const_iterator(_head);
		}

		//对哨兵位的头结点进行初始化
		void empty_Init()
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
		}
		
		list()
		{
			empty_Init();
		}

		//迭代器区间构造
		template<class Inputiterator>
		list(Inputiterator first, Inputiterator last)
		{
			empty_Init();
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		void swap(list& x)
		{
			std::swap(_head, x._head);
		}

		list(const list<T>& lt)
		{
			empty_Init();
			list<T> tmp(lt.begin(), lt.end());
			swap(tmp);
		}

		list<T>& operator=(list<T> lt)
		{
			swap(lt);
			return *this;
		}

		void push_back(const T& x)
		{
			/*Node* newnode = new Node(x);
			Node* tail = _head->_prev;

			_head->_prev = newnode;
			newnode->_next = _head;
			tail->_next = newnode;
			newnode->_prev = tail;*/
			insert(end(), x);
		}

		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;

			Node* newnode = new Node(x);

			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;

			return iterator(newnode);
		}

		void pop_back()
		{
			erase(--end());
		}

		void pop_front()
		{
			erase(begin());
		}

		iterator erase(iterator pos)
		{
			assert(pos != end());
			Node* cur = pos._node;
			Node* next = cur->_next;
			Node* prev = cur->_prev;

			prev->_next = next;
			next->_prev = prev;

			delete cur;

			return iterator(next);
		}

		size_t size()const
		{
			Node* cur = _head->_next;
			size_t count = 0;
			while (cur != _head)
			{
				count++;
				cur = cur->_next;
			}

			return count;
		}


		void clear()
		{
			//写法一
			//Node* cur = _head->_next;
			////头删
			//while (cur != _head)
			//{
			//	_head->_next = cur->_next;
			//	delete cur;
			//	cur = _head->_next;
			//}
			//_head->_next = _head->_prev = nullptr;

			//写法二
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);//erase返回的是下一个位置的迭代器
			}
		}

		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}
	private:
		Node* _head;
	};
}

