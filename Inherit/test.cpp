#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
using namespace std;
//class Person
//{
//public:
//	void Print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//	string _name = "peter"; // 姓名
//	int _age = 18; // 年龄
//};
//class Student : public Person
//{
//private:
//	int _stuid;
//};
//class Teacher : public Person
//{
//private:
//	int _jobid;
//};
//不可见-- 隐身了，类里面外面都不可访问

//class A
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		cout << "func(int i)->" << i << endl;
//	}
//};

//int main()
//{
//	/*Person p;
//	p.Print();
//	Student s;
//	s._name = "张三";
//	s.Print();*/
//	/*Teacher T;
//	T._name = "赵老师";
//	T.Print();*/
//	/*B b;
//	b.fun(10);
//	b.A::fun();*/
//	return 0;
//}

//class Person
//{
//protected:
//	int _num = 100;
//	string _name = "小李子";
//
//};
//
//class Student : public Person
//{
//public:
//	void Print()
//	{
//		cout << _num << endl;
//		cout << _name << endl;
//	}
//protected:
//	int _num = 10;
//};
//
//int main()
//{
//	/*Student s;
//	s.Print();*/
//
//	Student sobj;
//	//子类对象可以赋值给父类对象、指针、引用
//	//这里虽然是不同类型，但不是隐式类型转换
//	//这里算是一个特殊支持，是语法天然支持的
//	Person Pobj = sobj;
//	Person* Pobj1 = &sobj;
//	Person& pobj2 = sobj;
//	cout << sizeof(Student) << endl;
//	cout << sizeof(Person) << endl;
//
//	//父类暂时不可以赋值给子类，少了东西（后面会讲）
//
//
//	return 0;
//}

//class Person
//{
//public:
//	//Person(const char* name = "peter")
//	Person(const char* name)
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//public:
//	Student(const char* name, int num)
//		:Person(name)
//		,_num(num)
//	{
//
//	}
//
//	Student(const Student& s)
//		:Person(s)
//		,_num(s._num)
//	{
//
//	}
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			Person::operator=(s);
//			_num = s._num;
//		}
//
//		return *this;
//	}
//
//	//子类的析构函数跟父类的析构函数构成隐藏
//	//由于后面多态的需要，析构函数名字会统一处理成destructor()
//	~Student()
//	{
//		//不需要显示调用父类析构函数
//		// 每个子类析构函数后面，会自动调用父类析构函数，这样才能保证先析构子类，再析构父类
//		// 要符合栈的后进先出
//		//Person::~Person();
//
//		cout << "~Student()" << endl;
//	}
//	Student* operator&()
//	{
//		return this;
//	}
//protected:
//	int _num;
//};
//
//int main()
//{
//	Student s1("张三", 1);
//	Student S2(s1);
//	Student s3("李四", 2);
//	s1 = s3;
//	return 0;
//}

//友元不能继承
//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//	friend void Display(const Person& p, const Student& s);
//protected:
//	int _stuNum; // 学号
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;
//}
//void main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//}

//class Person
//{
//public:
//	Person(const char* name = "peter")
//		:_name(name)
//	{
//		cout << name << endl;
//	}
//	//string _name;
//protected:
//	string _name;
//};
//
//class Student : public Person
//{
//protected:
//	int _stuNum;
//};
//
//int main()
//{
//	Student s;
//	//s._name = "huang";
//
//	return 0;
//}

//class A final
//{
//private:
//	A()
//	{
//
//	}
//protected:
//	int _a;
//};
//
//class B : public A
//{
//
//protected:
//	int _b;
//};
//// C++98
////父类构造函数私有 —— 子类不可见
//// 子类对象实例化，无法调用构造函数
////C++11   final
//int main()
//{
//	//B b;
//	return 0;
//}

//class Base1 { public:  int _b1; };
//class Base2 { public:  int _b2; };
//class Derive : public Base1, public Base2 { public: int _d; };//现继承的在前面 
//
//int main()
//{
//	Derive d;
//	Base1* p1 = &d;
//	Base2* p2 = &d;
//	Derive* p3 = &d;
//
//	cout << p1 << endl;
//	cout << p2 << endl;
//	cout << p3 << endl;
//	return 0;
//}


//class Person
//{
//public:
//	string _name; 
//};
//class Student : virtual public Person
//{
//protected:
//	int _num;
//};
//class Teacher : virtual public Person
//{
//protected:
//	int _id;
//};
////菱形继承
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; 
//};
//int main()
//{
//	Assistant a;
//	//菱形虚拟继承解决了二义性和数据冗余
//	a._name = "peter";
//	a.Student::_name = "zhang";
//	a.Teacher::_name = "huang";
//	return 0;
//}

//class A
//{
//public:
//	int _a;
//};
//
////class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//
//class C : public A
////class C : virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	D d;
//    d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//
//	B b;
//	B* pb = &d;
//	//虚继承还会存个偏移量会影响B的大小
//	cout << sizeof(b) << endl;
//	return 0;
//}

class A
{

};
class B
{
	//组合
protected:
	A _a;
};

int main()
{
	return 0;
}

