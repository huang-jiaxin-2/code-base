#pragma once
#include <iostream>
#include <string>
#include <memory>
using namespace std;

template<class T>
class SmartPtr
{
public:
	SmartPtr(T* ptr = nullptr)
		:_ptr(ptr)
	{}
	~SmartPtr()
	{
		if (_ptr)
		{
			cout << "Delete:" << _ptr << endl;
			delete _ptr;
		}
	}
	T& operator*()
	{
		return *_ptr;
	}

	T* operator->()
	{
		return _ptr;
	}
private:
	T* _ptr;
};

//auto_ptr拷贝和赋值时本质就是管理权转移
//被拷贝的对象出现悬空问题
namespace hjx
{
	//auto_ptr拷贝和赋值时本质就是管理权转移
	//被拷贝的对象出现悬空问题
	template<class T>
	class auto_ptr
	{
	public:
		auto_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}

		auto_ptr(auto_ptr<T>& ap)
			:_ptr(ap._ptr)
		{
			ap._ptr = nullptr;
		}

		auto_ptr<T>& operator=(auto_ptr<T>& ap)
		{
			if (this != &ap)//不能是自己
			{
				if (_ptr)//判断一下是否为空
				{
					cout << "Delete:" << _ptr << endl;
					delete _ptr;
				}
				_ptr = ap._ptr;
				ap._ptr = nullptr;
			}

			return *this;
		}

		~auto_ptr()
		{
			if (_ptr)
			{
				cout << "Delete:" << _ptr << endl;
				delete _ptr;
			}
		}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};

	//unique_ptr不支持拷贝和赋值
	template<class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}

		//防拷贝—C++11
		unique_ptr(unique_ptr<T>& up) = delete;
		unique_ptr<T>& operator=(unique_ptr<T>& up) = delete;	

		~unique_ptr()
		{
			if (_ptr)
			{
				cout << "Delete:" << _ptr << endl;
				delete _ptr;
			}
		}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};

	template<class T>
	struct Delete
	{
		void operator()(T* ptr)
		{
			delete ptr;
		}
	};

	//shared_ptr采用引用计数的方式
	template<class T, class D = Delete<T>>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			,_pCount(new int(1))
		{}

		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			,_pCount(sp._pCount)
		{
			(*_pCount)++;//共同管理新资源，++计数
		}

		void Release()
		{
			if (--(*_pCount) == 0)
			{
				/*cout << "Delete:" << _ptr << endl;//方便观察现象
				delete _ptr;*/
				D()(_ptr);
				delete _pCount;
			}
		}

		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			//考虑自己给自己赋值的情况
			if (_ptr != sp._ptr)
			{
				//减减被赋值对象的计数，如果是最后一个对象，要释放资源
				Release();

				//共同管理新资源，++计数
				_ptr = sp._ptr;
				_pCount = sp._pCount;
				(*_pCount)++;
			}
			return *this;
		}

		~shared_ptr()
		{
			Release();
		}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		int use_count()
		{
			return *_pCount;
		}

		T* get()const
		{
			return _ptr;
		}
	private:
		T* _ptr;
		int* _pCount;//用一个指针来记录计数值
	};

	//weak_ptr是个辅助型智能指针，用来解决shared_ptr循环引用问题
	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr()
			:_ptr(nullptr)
		{}

		//拷贝和赋值不增加引用计数
		weak_ptr(const weak_ptr<T>& wp)
			:_ptr(wp._ptr)
		{}

		weak_ptr(const shared_ptr<T>& sp)
			:_ptr(sp.get())
		{}

		weak_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			_ptr = sp.get();
			return *this;
		}

		weak_ptr<T>& operator=(const weak_ptr<T>& wp)
		{
			_ptr = wp._ptr;
			return *this;
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};
}



template<class T>
struct DeleteArray
{
	void operator()(T* ptr)
	{
		//cout << "delete[]" << ptr << endl;//可不打印
		delete[] ptr;
	}
};

template<class T>
struct Free
{
	void operator()(T* ptr)
	{
		//cout << "free[]" << ptr << endl;//可不打印
		free(ptr);
	}
};

