#include "SmartPtr.h"


class A
{
public:
	A()
	{}
	~A()
	{
		cout << "~A()" << endl;
	}
	int _a1 = 0;
	int _a2 = 0;
};

void test_auto_ptr()
{
	std::auto_ptr<A> ap1(new A);
	ap1->_a1++;
	ap1->_a2++;

	std::auto_ptr<A> ap2(ap1);
	/*ap1->_a1++;
	ap1->_a2++;*/

	ap2->_a1++;
	ap2->_a2++;

	cout << ap2->_a1 << endl;
	cout << ap2->_a2 << endl;

	std::auto_ptr<A> ap3(new A);
	ap2 = ap3;

	ap2->_a1++;
	ap2->_a2++;
	cout << ap2->_a1 << endl;
	cout << ap2->_a2 << endl;
}

void test_unique_ptr()
{
	//unique_ptr直接不让拷贝
	hjx::unique_ptr<A> up1(new A);
	//unique_ptr<A> up2(up1);
}

class Node
{
public:
	int _val;
	std::shared_ptr<Node> _next;
	std::shared_ptr<Node> _prev;

	~Node()
	{
		cout << "~Node()" << endl;
	}
};

void test_shared_ptr()
{
	//shared_ptr采用引用计数的方式,不过会出现循环引用问题
	/*std::shared_ptr<A> sp1(new A);
	cout << sp1.use_count() << endl;
	std::shared_ptr<A> sp2(sp1);
	cout << sp2.use_count() << endl;
	std::shared_ptr<A> sp3(sp1);
	sp1->_a1++;
	sp2->_a2++;*/

	//cout << sp1->_a1 << ":" << sp1->_a2 << endl;
	//hjx::shared_ptr<int> sp4(new int);
	//hjx::shared_ptr<A> sp5(new A);
	//hjx::shared_ptr<A> sp6(sp5);
	//sp1 = sp6;
	//sp2 = sp6;
	//sp3 = sp6;

	//循环引用问题
	std::shared_ptr<Node> n1(new Node);
	std::shared_ptr<Node> n2(new Node);
	n1->_next = n2;
	n2->_prev = n1;
	cout << n1.use_count() << endl;
	cout << n2.use_count() << endl;
}

//weak_ptr不是常规的智能指针，没有RAII，不支持直接管理资源
//weak_ptr主要用shared_ptr构造，用来解决shared_ptr循环引用问题
void test_weak_ptr()
{
	/*hjx::shared_ptr<Node> n1(new Node);
	hjx::shared_ptr<Node> n2(new Node);

	cout << n1.use_count() << endl;
	cout << n2.use_count() << endl;

	n1->_next = n2;
	n2->_prev = n1;

	cout << n1.use_count() << endl;
	cout << n2.use_count() << endl;*/
}

//定制删除器
void test_shared_ptr1()
{
	hjx::shared_ptr<Node, DeleteArray<Node>> n1(new Node[7]);
	hjx::shared_ptr<int, Free<int>> n2((int*)malloc(sizeof(12)));
}

//int main()
//{
//	char* p = new char[1024 * 1024 * 1024];
//	cout << (void*)p << endl;
//	return 0;
//}

//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	SmartPtr<int> sp1(new int);
//	SmartPtr<int> sp2(new int);
//
//	cout << div() << endl;
//
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

int main()
{
	//SmartPtr<pair<string, int>> sp1(new pair<string, int>("print", 1));
	//cout << sp1->first << ":" << sp1->second << endl;
	//test_auto_ptr();
	//test_unique_ptr();
	//test_shared_ptr();
	//test_shared_ptr1();


	shared_ptr<A> sp2(new A[10]);//程序崩溃
	return 0;
}
