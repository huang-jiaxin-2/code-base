#pragma once

#include <iostream>
using namespace std;
//static int age; //定义
//区分变量是声明还是定义：主要看它有没有开空间，开了空间的就是定义，没开就是声明

//链接属性不一样
//extern int age;//所有文件可见
//static int size;//当前文件可见

//class Person
//{
//public:
//	void printInfo();//声明
//private:
//	char _name[20];//声明
//	char _sex[2];
//	int _age;
//};
