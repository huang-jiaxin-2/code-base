#define _CRT_SECURE_NO_WARNINGS 1

#include "test.h"
//struct QueueNode
//{
//	QueueNode* next;
//	int val;
//};
////封装
//class Queue
//{
//public:
//	void Init()
//	{
//		head = tail = nullptr;
//	}
//	void Push(int x)
//	{
//
//	}
//	void Pop()
//	{
//
//	}
//private:
//	QueueNode* head;
//	QueueNode* tail;
//};
//int main()
//{
//	Queue q;
//
//	q.Init();
//	q.Pop();
//	q.Push(1);
//	q.Push(2);
//	q.Push(3);
//	q.Push(4);
//	q.Push(5);
//
//	return 0;
//}

//void TestAuto()
//{
//	cout << "hello world" << endl;
//}
//int main()
//{
//	for (auto i = 0; i < 10; i++)
//	{
//		cout << "hello Linux" << endl;
//	}
//	return 0;
//}


//int main()
//{
//	Person p;//类的实例化
//	cout << sizeof(Person) << endl;
//	//cout << "age=" << &age << endl;
//	//p.printInfo();
//
//	//cout << "static size=" << &size << endl;
//
//	return 0;
//}

//class A
//{
//public:
//	void PrintA()
//	{
//		cout << _a << endl;
//	}
//	void Func()
//	{
//		cout << "A::Func()" << endl;
//	}
//private:
//	char _a;
//};
//
//class A2
//{
//public:
//	void f2(){}
//};
//
//class A3
//{
//
//};
//int main()
//{
//	/*A aa1;
//	A aa2;
//	A aa3;
//	aa1._a = 1;
//	aa1._a = 2;*/
//	//cout << sizeof(A) << endl;
//	/*A* ptr = nullptr;
//	ptr->Func();*/
//	cout << sizeof(A) << endl;
//
//	//没有成员变量的类对象，给1字节，用来占位不存储实际数据，标识对象存在
//	cout << sizeof(A2) << endl;
//	cout << sizeof(A3) << endl;
//	return 0;
//}

//单词和单词之间首字母大写间隔 —— 驼峰法  GetYear
//单词全部小写，单词之间_(下划线)分割       get_year
//驼峰法
//a、函数名、类名等所有单词首字母大写  GetYear
//b、变量首首字母小写，后面单词首字母大写  getYear
//c、成员变量，首字母前面加_   _getYear

//class Date
//{
//public:
//	//实参和形参位置不能显示传递和接收this指针
//	//但是可以在成员函数内部使用this指针
//	void Init(int year, int month, int day)
//	{
//		cout << this << endl;
//		this->_year = year;
//		this->_month = month;
//		this->_day = day;
//	}
//	void Print()
//	{
//		cout << this->_year << " " << this->_month << " " << this->_day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	/*Date d1;
//	Date d2;
//	d1.Init(2022, 7, 23);
//	d2.Init(2022, 7, 24);
//	d1.Print();
//	d2.Print();*/
//	Date* ptr = nullptr;
//	ptr->Print();
//	return 0;
//}

//this指针存在栈上，因为它是形参
//优化：取决于编译器
//vs下面传递this指针是通过ecx寄存器传递的，这样this指针访问效率提高
//class A
//{
//public:
//	void PrintA()
//	{
//		cout << this->_a << endl;//对this指针进行了使用
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A* p = nullptr;
//	p->PrintA();//结果是运行崩溃
//
//	return 0;
//}

//int main()
//{
//	cout << sizeof(long) << endl;
//	cout << sizeof(short) << endl;
//
//	return 0;
//}

