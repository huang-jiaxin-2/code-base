#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
    Mat img = imread("D:/Users/HP/Desktop/2.png");
    imshow("1", img);

    //转灰度图
//    Mat greyImg;
//    cvtColor(img, greyImg, COLOR_BGR2GRAY);
//    imshow("2", greyImg);

    //转hsv格式
    Mat hsv;
    cvtColor(img, hsv, COLOR_BGR2HSV);
    imshow("2", hsv);

    //去除噪点
//    Mat th;
//    threshold(greyImg, th, 200, 255, THRESH_BINARY);


    //截取颜色范围
    Mat mask;
    inRange(hsv, Scalar(100, 43, 46), Scalar(124, 255, 255), mask);
    imshow("3", mask);

    //取反操作
    bitwise_not(mask, mask);
    imshow("4", mask);

    //红色背景图
    Mat redBack = Mat::zeros(img.size(), img.type());
    redBack = Scalar(40, 40, 200);
    imshow("5", redBack);

    //拷贝操作
    img.copyTo(redBack, mask);
    imshow("6", redBack);

    waitKey();
    return 0;
}


//int main(int argc, char *argv[])
//{

//    Mat img = imread("F:/work/1.jpg");
//    imshow("title", img);

//    //模糊处理
//    Mat blurImg;
//    blur(img,blurImg, Size(50,50));//size(横向模糊尺寸，纵向模糊尺寸)
////    imshow("blur", blurImg);

//    //灰度处理->数据量减少三倍
//    Mat greyImg;
//    cvtColor(img, greyImg, COLOR_BGR2GRAY);
//    imshow("grey", greyImg);

//    //二值化处理
//    Mat threImg;
//    Mat dst;
//    threshold(greyImg, threImg, 120, 255, THRESH_BINARY);
//    GaussianBlur(threImg, dst, Size(3,3), 0, 0);
//    imshow("thre", threImg);
//    imshow("dst", dst);

//    waitKey(0);
//    return 0;
//}
