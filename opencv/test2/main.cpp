#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
//    Mat backImg = imread("D:/Users/HP/Desktop/3.jpg");
//    Mat hideImg = imread("D:/Users/HP/Desktop/4.jpg");
//    imshow("backImg",backImg);
//    imshow("hideImg",hideImg);

//    //转hsv格式
//    Mat hsv;
//    cvtColor(hideImg, hsv, COLOR_BGR2HSV);
//    imshow("hsv",hsv);

//    //识别出红色区域 红色有2个区域
//    Mat mask1,mask2;
//    inRange(hsv,Scalar(0,120,100),Scalar(10,255,255),mask1);//识别出红色区域的第一块模板
//    inRange(hsv,Scalar(170,120,100),Scalar(180,255,255),mask2);
////    imshow("mask1",mask1);
////    imshow("mask2",mask2);
//    mask1 = mask1 + mask2;//一张完整的红色区域内模板
//    imshow("mask",mask1);

//    //取反操作
//    Mat uMask;
//    bitwise_not(mask1,uMask);
//    imshow("uMask",uMask);

//    //背景图片+红布内的模板
//    Mat bkMask;
//    bitwise_and(backImg,backImg,bkMask,mask1);
//    imshow("bkMask",bkMask);
//    //红布的图片+红布以外的图片
//    Mat bkUmask;
//    bitwise_and(hideImg,hideImg,bkUmask,uMask);
//    imshow("bkUmask",bkUmask);

//    Mat img;
//    add(bkMask,bkUmask,img);
//    imshow("img",img);

//    waitKey(0);


    //视频播放
    Mat frame;
    Mat blurImg;
    VideoCapture capture("D:/Users/HP/Desktop/5.mp4");


    while(capture.read(frame))
    {
        imshow("video",frame);
        blur(frame,blurImg,Size(50,50));
        imshow("blur",blurImg);
        imwrite("blur",blurImg);
        waitKey(30);
    }
    return 0;
}
