#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//构造函数不是创建对象，而是初始化对象
//特征：1、函数名与类名相同
//      2、无返回值（也不用加void）
//      3、对象实例化时编译器自动调用对应的构造函数
//      4、构造函数可以重载

//C++类型分类：
//内置类型/基本类型：int/double/char/指针等等
//自定义类型： class/struct

//默认生成构造函数， a:内置类型成员不做处理 b:自定义类型成员回去调用它的默认构造函数

//默认构造函数： (特点：不传参数就可以调用)
//我们不写，编译器自动生成的那个
//我们自己写的，全缺省构造函数
//我们自己写的，无参构造函数
//class Date
//{
//public:
//	/*Date()
//	{
//		_year = 2021;
//		_month = 7;
//		_day = 24;
//	}*/
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//
//	}
//	~Date()
//	{
//		cout << "over" <<_year<< endl;
//	}
//private:
//	int _year = 1;    //这里不是初始化，而是给缺省值，c++11给的一个补丁
//	int _month = 1;
//	int _day = 1;
//};
//int main()
//{
//	Date d1(2022, 7, 24);
//	Date d2(2021);
//	int i = 10;
//	i = i;
//	return 0;
//}

//void Func(int a = 1, int b = 1, int c = 1)
//{
//	cout << a << " " << c << " " << b << " " << endl;
//}
//int main()
//{
//	Func(1, 2, 3);
//	Func();
//
//	return 0;
//}

//析构函数：不是完成对对象本身的销毁，而是对象在销毁时会自动调用析构函数，完成对象中资源的清理工作
//特征;
// 在类名前加上字符~ 按位取反
//无参数，无返回值类型
//一个类只能有一个析构函数(析构函数不能重载)
//对象生命周期结束时，C++编译系统会自动调用析构函数
//默认生成析构函数特点：
//a、内置类型不处理  b、自定义类型成员会去调用它的析构函数

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	~Date()
//	{
//		cout << "over" << endl;
//	}
//private:
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//};
//int main()
//{
//	Date d1(2022,1,2);
//	return 0;
//}

//class Time
//{
//public:
//	~Time()
//	{
//		cout << "~Time() " << endl;
//	}
//private:
//	int _hour = 1;
//	int _minute = 1;
//	int _second = 1;
//};
//
//class Date
//{
//
//private:
//	//内置类型不处理
//	int _year = 2022;
//	int _month = 7;
//	int _day = 24;
//
//	//自定义类型成员会去调用它的析构函数
//	Time _t;
//};
//
//int main()
//{
//	Date d;
//	return 0;
//}

///////////////////////////////////////////////////////////
//拷贝构造
//特点：
//拷贝构造函数是构造函数的一个重载
//参数只有一个，必须是类类型对象的引用，使用传值调用编译器直接报错，会引发无穷递归
//生成的默认拷贝函数中，内置类型按字节方式字节拷贝，自定义类型是调用拷贝构造函数完成拷贝
//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	/*~Date()
//	{
//		cout << "over" << endl;
//	}*/
//	/*Date(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}*/
//private:
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//};
//int main()
//{
//	Date d1(2022,1,2);
//	Date d2(d1);
//	Date d3 = d1;
//	return 0;
//}

//////////////////////////////////////////////////
//运算符重载
class Date
{
public:
	int IsLeapYear(int year)
	{
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}
	int GetMonthDay(int year, int month)
	{
		static int days[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (month == 2 && IsLeapYear(year))
		{
			return 29;
		}
		else
		{
			return days[month];
		}
	}
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	bool operator==(const Date& x)
	{
		return _year == x._year
			&& _month == x._month
			&& _day == x._day;
	}

	bool operator<(const Date& x)
	{
		return _year < x._year
			|| _month < x._month
			|| _day < x._day;
	}

	bool operator>(const Date& x)
	{
		return _year > x._year
			|| _month > x._month
			|| _day > x._day;
	}

	bool operator!=(const Date& x)
	{
		return _year != x._year
			|| _month != x._month
			|| _day != x._day;
	}
	Date operator+=(int day)
	{
		_day += day;
		while (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month == 13)
			{
				_month = 1;
				_year++;
			}
		}
		return *this;
	}

	Date operator+(int day)
	{
		Date ret(*this);
		ret._day += day;
		while (ret._day > GetMonthDay(ret._year, ret._month))
		{
			ret._day -= GetMonthDay(ret._year, ret._month);
			ret._month++;
			if (ret._month == 13)
			{
				ret._month = 1;
				ret._year++;
			}
		}
		return ret;
	}
	
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022, 7, 12);
	Date d2(2022, 7, 11);
	//cout << operator==(d1, d2) << endl;
	//cout << d1.operator==(d2) << endl;//->d1.operator==(&d1,d2)
	//cout << (d1 == d2) << endl;
	//cout << (d1+=100) << endl;
	//d1 += 10;
	//d2 + 12;
	cout << (d1 < d2) << endl;
	cout << (d1 != d2) << endl;

	return 0;
}