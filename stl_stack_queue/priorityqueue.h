#pragma once
#include <vector>
using namespace std;

//Compare进行比较的仿函数  less大堆 greater小堆
template<class T,class Container = vector<T>,class Compare = std::less<T>>
class mypriority_queue
{
public:
	mypriority_queue()
	{}

	template<class InputIterator>
	mypriority_queue(InputIterator first, InputIterator last)
	{
		while (first != last)
		{
			_con.push_back(*first);
			first++;
		}

		for (int i = (_con.size() - 1 - 1) / 2; i >= 0; i--)
		{
			adjust_down(i);
		}
	}

	//logN
	void adjust_up(size_t child)
	{
		Compare com;
		size_t parent = (child - 1) / 2;
		while (child > 0)
		{
			//if (_con[child] > _con[parent])
			//if ( _con[parent]  < _con[child])
			if (com(_con[parent], _con[child]))
			{
				swap(_con[child], _con[parent]);
				child = parent;
				parent = (child - 1) / 2;
			}
			else
			{
				break;
			}
		}
	}

	void push(const T& x)
	{
		_con.push_back(x);
		adjust_up(_con.size() - 1);
	}

	void adjust_down(size_t parent)
	{
		Compare com;
		size_t child = parent * 2 + 1;
		while (child < _con.size())
		{
			//if (child + 1 < _con.size() &&  _con[child] < _con[child + 1])
			if (child + 1 < _con.size() && com(_con[child], _con[child + 1]))
			{
				++child;
			}

			//if (_con[child] > _con[parent])
			//if (_con[parent] < _con[child])
			if (com(_con[parent], _con[child]))
			{
				swap(_con[child], _con[parent]);
				parent = child;
				child = parent * 2 + 1;
			}
			else
			{
				break;
			}
		}
	}

	void pop()
	{
		swap(_con[0], _con[_con.size() - 1]);
		_con.pop_back();

		adjust_down(0);
	}

	const T& top()
	{
		return _con[0];
	}

	bool empty() const
	{
		return _con.empty();
	}

	size_t size() const
	{
		return _con.size();
	}
private:
	Container _con;
};