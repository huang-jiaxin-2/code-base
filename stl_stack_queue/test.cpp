#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <stack>
#include <queue>
#include <functional>
#include <algorithm>
#include <list>
#include "stack.h"
#include "queue.h"
#include "priorityqueue.h"
using namespace std;

void test_mystack()
{
	mystack<int> s;
	s.push(1);
	s.push(2);
	s.push(3);
	s.push(4);
	while (!s.empty())
	{
		cout << s.top() << " ";
		s.pop();
	}
	cout << endl;
}

void test_myqueue()
{
	myqueue<int, list<int>> q;
	//myqueue<int,vector<int>> q; //不支持vector 没有pop_front
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);
	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;

}

void test_priority_queue()
{
	//用堆实现的，默认大的优先级高
	priority_queue<int> pq;
	pq.push(1);
	pq.push(2);
	pq.push(3);
	pq.push(1);
	pq.push(5);
	pq.push(7);
	pq.push(6);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;

	int a[] = { 3,4,2,5,1,6,7 };
	//priority_queue<int> heap(a, a + sizeof(a) / sizeof(int));
	priority_queue<int, vector<int>, greater<int>> heap(a, a + sizeof(a) / sizeof(int));

	while (!heap.empty())
	{
		cout << heap.top() << " ";
		heap.pop();
	}
	cout << endl;
}

void test_mypriority_queue()
{
	mypriority_queue<int> pq;
	pq.push(1);
	pq.push(2);
	pq.push(3);
	pq.push(1);
	pq.push(5);
	pq.push(7);
	pq.push(6);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;

	int a[] = { 3,4,2,5,1,6,7 };
	//mypriority_queue<int> heap(a, a + sizeof(a) / sizeof(int));
	mypriority_queue<int, vector<int>, greater<int>> heap(a, a + sizeof(a) / sizeof(int));

	while (!heap.empty())
	{
		cout << heap.top() << " ";
		heap.pop();
	}
	cout << endl;
}

int main()
{
	/*test_mystack();
	test_myqueue();
	test_priority_queue();*/
	test_mypriority_queue();
	return 0;
}

//仿函数/函数对象 -- 类，重载operator()
//类对象可以像函数一样去使用
//namespace hjx
//{
//	template<class T>
//	class less
//	{
//	public:
//		
//		bool operator()(const T& l, const T& r)const
//		{
//			return l < r;
//		}
//	};
//
//    template<class T>
//	class greater
//	{
//	public:
//		
//		bool operator()(const T& l, const T& r)const
//		{
//			return l > r;
//		}
//	};
//}
//
//int main()
//{
//	hjx::less<int> lsFunc;
//	cout << lsFunc(3, 4) << endl;
//
//	hjx::greater<int> gtFunc;
//	cout << gtFunc(3, 4) << endl;
//	return 0;
//}