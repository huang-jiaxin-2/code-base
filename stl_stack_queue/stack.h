#pragma once
#include <deque>
using namespace std;
template <class T,class Container = deque<T>>
class mystack
{
public:
	void push(const T& x)
	{
		_con.push_back(x);
	}

	void pop()
	{
		_con.pop_back();
	}

	T& top()
	{
		return _con.back();
	}

	bool empty()const
	{
		return _con.empty();
	}

	size_t size()const
	{
		return _con.size();
	}
private:
	Container _con;
};
