#define _CRT_SECURE_NO_WARNINGS 1
#include "Date.h"
//void TestDate1()
//{
//	Date d1(2022, 8, 3);
//	Date d2(d1);//拷贝构造
//
//	Date d3(2022, 8, 6);
//	d1 = d3;//赋值
//	//d2 = d2;
//}
//int main()
//{
//	TestDate1();
//	return 0;
//}

void Test1()
{
	Date d1(2022, 8, 3);
	Date d2(2023, 8, 4);
	/*Date d2(2022, 8, 4);
	Date d3(2022, 8, 3);

	cout << (d1 == d2) << endl;
	cout << (d1 > d2) << endl;
	cout << (d1 > d3) << endl;
	cout << (d1 == d3) << endl;*/

	/*d1 += 23;
	d1.print();


	d1 += 33;
	d1.print();

	d1 += 330;
	d1.print();

	(d1 + 4000).print();*/

	//(++d2).print();//d1.operator(&d1);
	//(d2++).print();//d1.operator(&d1,0);
	//(d1 - 3).print();
	//(d1--).print();
	//(--d1).print();
	int ret = d2 - d1;
	cout << ret << endl;
}
void Test2()
{
	Date d3(2022, 8, 4);
	Date d4(2022, 8, 5);
	cin >> d3;
	cout << d3;//支持连续的流插入要有返回值
	//d3.print();

}
void Test3()
{
	const char* WeekDayToStr[] = { "周一","周二", "周三", "周四", "周五", "周六", "周日" };
	Date d5;
	cin >> d5;
	Date start(1, 1, 1);
	int n = d5 - start;
	int weekday = 0;
	weekday += n;
	cout << WeekDayToStr[weekday % 7 ] << endl;
}

int main()
{
	Test3();
	return 0;
}