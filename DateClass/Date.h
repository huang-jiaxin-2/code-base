#pragma once
#include <iostream>
#include <assert.h>
using namespace std;

class Date
{
	// 友元函数 -- 这个函数内部可以使用Date对象访问私有保护成员 
	 friend ostream& operator<<(ostream& out, const Date& d);
	 friend istream& operator>>(istream& in, Date& d);
public:
	int IsLeapYear(int year)
	{
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}
	int GetMonthDay(int year, int month)
	{
		static int days[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (month == 2 && IsLeapYear(year))
		{
			return 29;
		}
		else
		{
			return days[month];
		}
	}
	bool CheckDate()
	{
		if (_year >= 1
			&& _month > 0 && _month < 13
			&& _day>0 && _day <= GetMonthDay(_year, _month))
		{
			return true;
		}
		else
			return false;
	}
	//构造函数会频繁调用，所以直接放在类里面定义作为inline
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
		
		assert(CheckDate());
	}
	/*Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	Date& operator=(const Date& d)
	{
		if (this != &d)
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}

		return *this;
	}*/

	void print()const;

	bool operator==(const Date& d)const;

	bool operator<(const Date& d)const;
	
	bool operator>(const Date& d)const;

	bool operator!=(const Date& d)const;

	bool operator>=(const Date& d)const;

	bool operator<=(const Date& d)const;

	Date operator+(int day)const;

	Date& operator+=(int day);

	Date& operator++();//前置++

	Date operator++(int);//后置++

	Date operator-(int day)const;

	Date& operator-=(int day);

	Date& operator--();//前置--

	Date operator--(int);//后置--

	int operator-(const Date& d)const;//日期-日期

	//void operator<<(ostream& out);
private:
	int _year;
	int _month;
	int _day;
};

//频繁调用所以改成内联
inline ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	return out;
}

inline istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	assert(d.CheckDate());

	return in;
}