#define _CRT_SECURE_NO_WARNINGS 1
#include "Date.h"

void Date::print()const
{
	cout << _year << "/" << _month << "/" << _day << endl;
}

bool Date::operator==(const Date& d)const
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

bool Date::operator>(const Date& d)const
{
	if ((_year > d._year)
		|| (_year == d._year && _month > d._month)
		|| (_year == d._year && _month == d._month && _day > d._day))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date::operator>=(const Date& d)const
{
	return (*this > d) || (*this == d);
}

bool Date::operator<(const Date& d)const
{
	return !(*this >= d);
}

bool Date::operator!=(const Date& d)const
{
	/*return _year != d._year
		|| _month != d._month
		|| _day != d._day;*/
	return !(*this == d);//复用==
}

bool Date::operator<=(const Date& d)const
{
	return !(*this > d);
}

Date& Date::operator+=(int day)
{
	if (day < 0)
	{
		return *this -= -day;
	}
	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}
	//*this = *this + day;//复用+
	return *this;
}

Date Date::operator+(int day)const
{
	Date ret(*this);
	/*ret._day += day;
	while (ret._day > GetMonthDay(ret._year, ret._month))
	{
		ret._day -= GetMonthDay(ret._year, ret._month);
		ret._month++;
		if (ret._month == 13)
		{
			ret._month = 1;
			ret._year++;
		}
	}*/
	ret += day;//复用+=
	return ret;
}

Date& Date::operator++()//前置++
{
	//*this += 1;
	return *this += 1;
}

Date Date::operator++(int)//后置++
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}

Date Date::operator-(int day)const
{
	Date ret(*this);
	ret -= day;
	return ret;
}

Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		return *this += -day;
	}
	_day -= day;
	while (_day <= 0)
	{
		--_month;
		if (_month < 0)
		{
			--_year;
			_month = 12;
		}
		_day += GetMonthDay(_year, _month);
	}
	return *this;
}

Date& Date::operator--()//前置--
{
	return *this -= 1;
}

Date Date::operator--(int)//后置--
{
	Date ret(*this);
	*this -= 1;
	return ret;

}

int Date::operator-(const Date& d)const//日期-日期
{
	int flag = 1;
	Date max = *this;
	Date min = d;
	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int n = 0;
	while (min != max)
	{
		++min;
		++n;
	}
	return n * flag;
}

//void Date::operator<<(ostream& out)
//{
//	out << _year << "/" << _month << "/" << _day << endl;
//}

