#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <memory>
using namespace std;
#include "SmartPtr.h"

//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//
//void Func()
//{
//	SmartPtr<int> sp1(new int);
//	SmartPtr<int> sp2(new int);
//	cout << div() << endl;
//}

//很多公司明确要求不能使用auto_ptr
void test_auto_ptr()
{
	HJX::auto_ptr<A> ap1(new A);
	ap1->_a1++;
	ap1->_a2++;

	HJX::auto_ptr<A> ap2(ap1);//管理权转移，被拷贝对象出现悬空问题
	//ap1->_a1++;
	//ap1->_a2++;

	ap2->_a1++;
	ap2->_a2++;

	HJX::auto_ptr<A> ap3;
	ap2 = ap3;
}

//unique_ptr只适用于不需要拷贝的一些场景
void test_unique_ptr()
{
	HJX::unique_ptr<A> up1(new A);
	//std::unique_ptr<A> up2(up1);//不让拷贝
	//HJX::unique_ptr<A> up2(up1);

}

//shared_ptr 适用于一些需要拷贝的场景
void test_shared_ptr1()
{
	HJX::shared_ptr<A> sp1(new A);
	HJX::shared_ptr<A> sp2(sp1);
	HJX::shared_ptr<A> sp3(sp1);

	sp1->_a1++;
	sp1->_a2++;
	cout << sp2->_a1 << ":" << sp2->_a2 << endl;

	sp2->_a1++;
	sp2->_a2++;
	cout << sp1->_a1 << ":" << sp1->_a2 << endl;

	sp3->_a1++;
	sp3->_a2++;
	cout << sp1->_a1 << ":" << sp1->_a2 << endl;

	HJX::shared_ptr<int> sp4(new int);
	sp4 = sp4;

	HJX::shared_ptr<A> sp5(new A);
	sp1 = sp5;
	sp1 = sp5;
}

struct Node
{
	int val;
	/*std::shared_ptr<Node> _next;
	std::shared_ptr<Node> _prev;*/

	HJX::weak_ptr<Node> _next;
	HJX::weak_ptr<Node> _prev;

	~Node()
	{
		cout << "~Node()" << endl;
	}
};

//循环引用 -- weak_ptr不是常规智能指针，它没有RAII，不支持直接管理资源
//weak_ptr主要用shared_ptr构造，用来解决shared_ptr循环引用问题
void test_shared_ptr2()
{
	HJX::shared_ptr<Node> n1(new Node);
	HJX::shared_ptr<Node> n2(new Node);

	cout << n1.use_count() << endl;
	cout << n2.use_count() << endl;

	n1->_next = n2;
	n2->_prev = n1;

	cout << n1.use_count() << endl;
	cout << n2.use_count() << endl;
}


//定制删除器
void test_shared_ptr3()
{
	//仿函数
	HJX::shared_ptr<Node> n1(new Node);
	HJX::shared_ptr<Node, DeleteArray<Node>> n2(new Node[3]);
	HJX::shared_ptr<int, DeleteArray<int>> n3(new int[5]);
	HJX::shared_ptr<int, Free<int>> n4((int*)malloc(sizeof(12)));

	//lambda
	/*std::shared_ptr<Node> n1(new Node);
	std::shared_ptr<Node> n2(new Node[3], [](Node* ptr) {delete[] ptr; });
	std::shared_ptr<int> n3(new int[5], [](int* ptr) {delete[] ptr; });
	std::shared_ptr<int> n4((int*)malloc(sizeof(12)), [](int* ptr) {free(ptr); });
	std::shared_ptr<FILE> n5(fopen("test.txt", "w"), [](FILE* ptr) {fclose(ptr); });*/
}

int main()
{
	/*try 
	{
		Func();
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}*/

	//test_auto_ptr();
	//test_unique_ptr();
	test_shared_ptr3();
	return 0;
}