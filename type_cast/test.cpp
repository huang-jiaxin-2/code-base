#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//int main()
//{
//	//double d = 12.34;
//	//int a = static_cast<int>(d);//两个相关的类型进行转换
//	//cout << a << endl;
//
//	//int* p = &a;
//	//int add = reinterpret_cast<int>(p);//两个不相关的类型进行转换
//	//cout << add << endl;
//
//	volatile const int b = 2;//直接从内存中取值
//	//const int b = 2;
//	int* p = (int*)(&b);
//	//int* p = const_cast<int*>(&b);
//	*p = 3;
//	cout << b << endl;//2
//	cout << *p << endl;//3
//	return 0;
//}

class A
{
public:
	int _a = 1;
};

class B :public A
{
public:
	int _b;
};

int main()
{

	return 0;
}