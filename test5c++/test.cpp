#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//#include <stdio.h>
//#include <windows.h>
//#include <unistd.h>
//#define M 100
//int main()
//{
//	int n = 5;
//	while (n >= 0)
//	{
//		printf("\033[31m[倒计时：%d]\033[0m\r", n);
//		n--;
//		fflush(stdout);
//		Sleep(1000);
//
//	}
//	//printf("hello world\n");
//
//	return 0;
//}

//先定义的后析构
//class A
//{
//public:
//	A(int a = 0)
//	{
//		_a = a;
//		cout << "A(int _a = 0)->" << _a << endl;
//	}
//
//	~A()
//	{
//		cout << "~A()->" << _a << endl;
//	}
//private:
//	int _a;
//};
//
//A aa3(3);
//A aa5(5);
//void f()
//{
//	static A aa0(0);//静态区
//	A aa1(1);
//	A aa2(2);//栈区
//	static A aa4(4);
//}
//
//int main()
//{
//	//static A aa0(0);//静态区
//	//A aa1(1);
//	//A aa2(2);//栈区
//	//static A aa4(4);
//	f();
//	f();
//
//	return 0;
//}

//拷贝构造
class A
{
public:
	A(int a)
	{
		_a = a;
		cout << "A(int a)->" << _a << endl;
	}
	A(const A& aa)
	{
		_a = aa._a;
		cout << "A(const A& aa)->" << _a << endl;
	}

	~A()
	{
		cout << "~A()->" << _a << endl;
	}

private:
	int _a;
};

void func1(A aa)
{

}

void func2(A& aa)
{

}

A func3()
{
	static A aa(3);
	return aa;
}

A& func4()
{
	static A aa(4);
	return aa;
}
int main()
{
	//A aa1(1);
	//A aa2(aa1);
	//func1(aa1);
	//func2(aa1);
	func3();
	cout << endl;
	func4();
	return 0;
}