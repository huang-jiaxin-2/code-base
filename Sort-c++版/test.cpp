#include "sort.h"

void Test1()
{
	vector<int> v;
	v.push_back(9);
	v.push_back(1);
	v.push_back(2);
	v.push_back(5);
	v.push_back(7);
	v.push_back(4);
	v.push_back(8);
	v.push_back(6);
	v.push_back(3);
	v.push_back(5);
	InsertSort(v, v.size());
	Print(v);
}

void Test2()
{
	vector<int> v;
	v.push_back(9);
	v.push_back(1);
	v.push_back(2);
	v.push_back(5);
	v.push_back(7);
	v.push_back(4);
	v.push_back(8);
	v.push_back(6);
	v.push_back(3);
	v.push_back(5);
	ShellSort(v, v.size());
	Print(v);
}

void Test3()
{
	vector<int> v;
	v.push_back(9);
	v.push_back(1);
	v.push_back(2);
	v.push_back(5);
	v.push_back(7);
	v.push_back(4);
	v.push_back(8);
	v.push_back(6);
	v.push_back(3);
	v.push_back(5);
	SelectSort(v, v.size());
	Print(v);
}

void Test4()
{
	vector<int> v;
	v.push_back(9);
	v.push_back(1);
	v.push_back(2);
	v.push_back(5);
	v.push_back(7);
	v.push_back(4);
	v.push_back(8);
	v.push_back(6);
	v.push_back(3);
	v.push_back(5);
	BubbleSort(v, v.size());
	Print(v);
}

void Test5()
{
	vector<int> v;
	v.push_back(6);
	v.push_back(1);
	v.push_back(2);
	v.push_back(7);
	v.push_back(9);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(10);
	v.push_back(8);
	QuickSortNonR(v, 0, v.size() - 1);
	Print(v);
}

void Test6()
{
	vector<int> v;
	v.push_back(6);
	v.push_back(1);
	v.push_back(2);
	v.push_back(7);
	v.push_back(9);
	v.push_back(3);
	v.push_back(4);
	v.push_back(11);
	v.push_back(5);
	v.push_back(8);
	v.push_back(10);
	MergeSortNonR(v, v.size());
	Print(v);
}

void Test7()
{
	vector<int> v;
	v.push_back(10);
	v.push_back(15);
	v.push_back(19);
	v.push_back(10);
	v.push_back(15);
	v.push_back(14);
	v.push_back(18);
	v.push_back(10);
	v.push_back(-5);
	v.push_back(-4);

	CountSort(v, v.size());
	Print(v);
}

void Test8()
{
	vector<int> v;
	v.push_back(6);
	v.push_back(1);
	v.push_back(2);
	v.push_back(7);
	v.push_back(9);
	v.push_back(3);
	v.push_back(4);

	HeapSort(v, v.size());
	Print(v);
}

int main()
{
	Test8();
	return 0;
}