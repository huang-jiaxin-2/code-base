#pragma once
#include <iostream>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <stack>

using namespace std;

void Print(vector<int>& v);
void InsertSort(vector<int> &v, int n);
void ShellSort(vector<int>& v, int n);
void SelectSort(vector<int>& v, int n);
void Swap(int& a, int& b);
void BubbleSort(vector<int>& v, int n);
void QuickSort(vector<int>& v, int begin, int end);
int GetMidKeyi(vector<int>& v, int begin, int end);
void QuickSortNonR(vector<int>& v, int begin, int end);
int PartQuickSort(vector<int>& v, int begin, int end);
void MergeSort(vector<int>& v, int n);
void MergeSortNonR(vector<int>& v, int n);
void CountSort(vector<int>& v, int n);
void HeapSort(vector<int>& v, int n);
void AdjustDown(vector<int>& v, int n, int root);

