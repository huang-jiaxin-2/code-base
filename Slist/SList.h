#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLTDataType;
typedef struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
}SLTNode;

//打印
void SListPrint(SLTNode* phead);

//尾插
void SListPushBack(SLTNode** pphead, SLTDataType x);

//头插
void SListPushFront(SLTNode** pphead, SLTDataType x);

//开辟结点
SLTNode* BuySListNode(SLTDataType x);

//尾删
void SListPopBack(SLTNode** pphead);

//头删
void SListPopFront(SLTNode** pphead);

//查找
SLTNode* SListFind(SLTNode* pphead, SLTDataType x);

//在pos位置之前插入
void SListInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x);

//在pos位置删
void SListErase(SLTNode** pphead, SLTNode* pos);

//在pos位置之后插入
void SListInsertAfter(SLTNode* pos, SLTDataType x);

//在pos位置之后删
void SListEraseAfter(SLTNode* pos);

//销毁链表
void SListDestory(SLTNode** pphead);

