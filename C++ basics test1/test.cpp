#define _CRT_SECURE_NO_WARNINGS 1

#include "new.h"

//int main()
//{
//	int a = 1;
//	auto b = a;
//	auto c = 'c';//auto自动推导类型
//	auto d = 1.1;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	
//	return 0;
//}

//int main()
//{
//	int a = 1;
//	auto b = &a;
//	auto* c = &a;//强调一定要传个指针
//	auto& d = a;//强调d是引用
//
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	return 0;
//}
//void Test(auto x)
//{
//int a = 1, b = 2;
//}

//void Test(int a[])
//{
//	for (auto& b : a)
//	{
//		cout << b << " ";
//	}
//	cout << endl;
//}

//int main()
//{
//	int a[] = { 1,2,3,4,5,6,7,8 };
//	for (auto& b : a)
//	{
//		b++;
//		cout << b << " ";
//	}
//	cout << endl;
//	return 0;
//}
void Test(int)
{
	cout << "Test(int)" << endl;
}
void Test(int*)
{
	cout << "Test(int*)" << endl;
}
int main()
{
	int* p = NULL;
	Test(0);
	Test(NULL);
	Test(p);
	return 0;
}
//int main()
//{
//	int* p = NULL;
//	return 0;
//}