#pragma once
#include <iostream>
using namespace std;
//有定义就会有符号表
//static int age;//定义->加上extern或者static 链接冲突 因为你是在.h中定义的，.h文件内容最终会在.cpp文件中展开，age是定义，当有多个.cpp文件同时编译完后
        //在合成一个目标文件时，多个.cpp文件中有同一个age就会造成冲突\
		// static 只在当前文件可见，链接属性不一样
//声明和定义的区别就在于开没开空间
//class Student
//{
//public:
//	void PrintInfo()//成员函数
//	{
//		cout << name << " " << sex << " " << age << endl;
//	}
//
//	char name[10];//成员变量          //声明->类的实例化的时候才会开空间
//	char sex[3];
//	int age;
//};//和结构体一样最后也是要带上分号的

//int age;

//class Student
//{
//public:
//	void PrintInfo();
//
//	char name[10];        
//	char sex[3];
//	int age;
//};

//class Test
//{
//public:
//	int Print()
//	{
//		int b = 3;
//		cout << _a << endl;
//		return b;
//	}
//private:
//	char _a;
//};