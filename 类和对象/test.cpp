#include <iostream>
#include "Student.h"
using namespace std;

//class Student
//{
//public:
//	void PrintInfo();
//private:
//	char name[10];        
//	char sex[3];
//	int age;
//};

//int age;//声明 or 定义？
//
//class Student
//{
//public:
//	void PrintInfo();
//
//	char name[10];
//	char sex[3];
//	int age;//声明 or 定义?
//};

//int main()
//{
//	//Student s;//实例化出一个对象
//	//s.PrintInfo();//用实例化出的对象访问类中的成员
//	return 0;
//}


//int main()
//{
//	//cout << sizeof(Test) << endl;
//	//Test* a = nullptr;
//	//a->Print();//正常运行
//	Student s;
//	s.PrintInfo();
//	return 0;
//}

//// 类中既有成员变量，又有成员函数
//class A1 {
//public:
//	void f1() {}
//private:
//	char c;
//	int a;
//};
////类中仅有成员变量
//class A2
//{
//private:
//	int a;
//};
//// 类中仅有成员函数
//class A3 {
//public:
//	void f2() {}
//};
////空类
//class A4
//{};
//
//int main()
//{
//	cout << sizeof(A1) << endl;
//	cout << sizeof(A2) << endl;
//	cout << sizeof(A3) << endl;
//	cout << sizeof(A4) << endl;
//	return 0;
//}

//int main()
//{
//	//Student s;
//	//s.PrintInfo();
//	return 0;
//}

class Date
{
public:
	void Init(int year, int month, int day)
	{
		this->_year = year;
		this->_month = month;
		this->_day = day;
	}
	void Print()
	{
		cout << &_year << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d2;
	Date* d1 = &d2;
	d1->Print();//运行崩溃
	return 0;
}



