#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <list>
#include <vector>
#include <time.h>
#include <algorithm>
#include <string>
using namespace std;
void test_list1()
{
	list<int> lt;
	lt.push_back(1); 
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	list<int>::iterator it = lt.begin();
	while (it != lt.end())
	{
		cout << *it << " ";
		*it *= 2;
		it++;
	}
	cout << endl;

	lt.push_front(10);
	lt.push_front(20);
	lt.push_front(30);


	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	lt.push_back(60);
	cout << lt.front() << endl;
	cout << lt.back() << endl;
}

void test_list2()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	auto pos = find(lt.begin(), lt.end(), 3);
	if (pos != lt.end())
	{
		lt.insert(pos, 30);
		*pos *= 100;
	}
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	pos = find(lt.begin(), lt.end(), 4);
	if (pos != lt.end())
	{
		lt.erase(pos);
		//*pos *= 100;
	}
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list3()
{
	list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	lt1.push_back(5);

	list<int> lt2;
	lt2.push_back(10);
	lt2.push_back(20);
	lt2.push_back(30);
	lt2.push_back(40);
	lt2.push_back(50);

	list<int>::iterator it = lt2.begin();
	it++;
	lt2.splice(it, lt1);
	for (auto e : lt2)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list4()
{
	list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	lt1.push_back(5);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	lt1.push_back(5);
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;

	lt1.remove(4);
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
	lt1.clear();
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list5()
{
	list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	lt1.push_back(5);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	lt1.push_back(5);

	lt1.sort();
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
	lt1.unique();
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_op()
{
	srand(time(0));
	const int N = 100000;
	vector<int> v;
	v.reserve(N);

	list<int> lt1;
	list<int> lt2;

	for (int i = 0; i < N; ++i)
	{
		auto e = rand();
		v.push_back(e);
		//lt1.push_back(e);
		lt2.push_back(e);
	}

	// 拷贝到vector排序，排完以后再拷贝回来
	int begin1 = clock();
	/*for (auto e : lt1)
	{
		v.push_back(e);
	}*/
	sort(v.begin(), v.end());
	size_t i = 0;
	/*for (auto& e : lt1)
	{
		e = v[i++];
	}*/
	int end1 = clock();

	int begin2 = clock();
	// sort(lt.begin(), lt.end());
	lt2.sort();
	int end2 = clock();

	printf("vector sort:%d\n", end1 - begin1);
	printf("list sort:%d\n", end2 - begin2);
}

void test_list6()
{
	list<string> lt;
	lt.push_back("北京");
	lt.push_back("上海");
	lt.push_back("江西");
	lt.push_back("四川");
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
	lt.sort();
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
	cout << endl;

	vector<string> v;
	v.push_back("北京");
	v.push_back("上海");
	v.push_back("江西");
	v.push_back("四川");
	sort(v.begin(), v.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}
int main()
{
	test_list6();
	//test_op();
	return 0;
}