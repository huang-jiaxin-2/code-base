#include <iostream>
#include <thread>
#include <mutex>
#include <Windows.h>

using namespace std;

int ticket = 1000;
//mutex mtx;

void getTickets(int id, mutex& mtx)
{
	while (true)
	{
		lock_guard<mutex> lck(mtx);
		if (ticket > 0)
		{
			cout << this_thread::get_id() << ":" << ticket << endl;
			ticket--;
		}
		else
		{
			break;
		}
		this_thread::sleep_for(chrono::milliseconds(1));
		/*Sleep(10);*/
	}
}

int main()
{
	mutex mtx;
	thread t1(getTickets, 1, ref(mtx));//直接传的话还是一个拷贝，要加ref修饰
	thread t2(getTickets, 2, ref(mtx));

	t1.join();
	t2.join();
	return 0;
}