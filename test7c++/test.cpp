#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>
using namespace std;

//template <typename T>
//template <class T>
//void Swap(T& left, T& right)
//{
//	T tmp = left;
//	left = right;
//	right = tmp;
//}
//int main()
//{
//	int a = 1;
//	int b = 2;
//	double c = 1.1, d = 2.2;
//	char i = 'm', w = 'n';
//	Swap(a, b);
//	Swap(c, d);
//	Swap(i, w);
//	cout << a << " " << b << endl;
//	cout << c << " " << d << endl;
//	cout << i << " " << w << endl;
//
//	return 0;
//}

//int main()
//{
//	char a[] = "华为";
//	char a1[5] = { -69,-86,-50,-86,0 };
//	cout << a1 << endl;
//
//	/*a1[3]++;
//	cout << a1 << endl;
//
//	a1[3]++;
//	cout << a1 << endl;
//
//	a1[3]++;
//	cout << a1 << endl; 
//	
//	a1[3]++;
//	cout << a1 << endl;*/
//
//	a1[2]--;
//	cout << a1 << endl;
//
//	a1[2]--;
//	cout << a1 << endl;
//
//	a1[2]--;
//	cout << a1 << endl;
//
//	a1[2]--;
//	cout << a1 << endl;
//
//	return 0;
//}
//class A
//{
//public:
//	A(int a = 1)
//		:_a(a)
//	{
//
//	}
//	~A()
//	{
//
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A* p1 = new A;
//	A* p2 = (A*)malloc(sizeof(A));
//	if (p2 == NULL)
//	{
//		perror("malloc fail");
//	}
//	new(p2)A(10);
//	return 0;
//}
//template <class T>
//void Swap(T& left, T& right)
//{
//	T tmp = left;
//	left = right;
//	right = tmp;
//}
//int main()
//{
//	int a = 1;
//	int b = 2;
//	double c = 1.1, d = 2.2;
//	char i = 'm', w = 'n';
//	Swap(a, b);
//	Swap(c, d);
//	Swap(i, w);
//	cout << a << " " << b << endl;
//	cout << c << " " << d << endl;
//	cout << i << " " << w << endl;
//
//	return 0;
//}


//template <class T>
//T Add(const T& left, const T& right)
//{
//	return left + right;
//}
//int main()
//{
//
//	return 0;
//}

//int Add(int left, int right)
//{
//	return left + right;
//}
//
//template <class T>
//T Add(const T& left, const T& right)
//{
//	return left + right;
//}
//
//template <class T1,class T2>
//T1 Add(const T1& left, const T2& right)
//{
//	return left + right;
//}
//int main()
//{
//	cout << Add(1, 2) << endl;
//	cout << Add(1.1, 2.2) << endl;
//	cout << Add(1.1, 2) << endl;
//
//	return 0;
//}

//类模板
//template <class T = char>
//class Stack
//{
//public:
//	Stack(size_t capacity = 0)
//		:_a(nullptr)
//		, _capacity(0)
//		, _top(0)
//	{
//		if (capacity > 0)
//		{
//			_a = new T[capacity];
//			_capacity = capacity;
//			_top = 0;
//		}
//	}
//
//		~Stack()
//		{
//			delete[] _a;
//			_a = nullptr;
//			_capacity = _top = 0;
//		}
//
//		void Push(const T& x)
//		{
//			//1、开新空间
//			//2、拷贝数据
//			//3、释放旧空间
//			if (_capacity == _top)
//			{
//				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
//				T* tmp = new T[newcapacity];
//				if (_a)
//				{
//					memcpy(tmp, _a, sizeof(T)* _top);
//					delete[] _a;
//				}
//				_a = tmp;
//				_capacity = newcapacity;
//			}
//			_a[_top] = x;
//			_top++;
//		}
//
//		void Pop()
//		{
//			assert(_top > 0);
//			_top--;
//		}
//
//		bool Empty()
//		{
//			return _top == 0;
//		}
//		
//		const T& GetTop()
//		{
//			assert(_top > 0);
//			return _a[_top - 1];
//		}
//private:
//	T* _a;
//	size_t _capacity;
//	size_t _top;
//};
//
//int main()
//{
//	try
//	{
//		Stack<int> st1;
//		//Stack<char> st2;
//		st1.Push(1);
//		st1.Push(2);
//		st1.Push(3);
//		st1.Push(4);
//		while (!st1.Empty())
//		{
//			cout << st1.GetTop() << " ";
//			st1.Pop();
//		}
//		cout << endl;
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	
//	return 0;
//}

//int main()
//{
//	vector<int>array;
//
//	array.push_back(100);
//
//	array.push_back(300);
//
//	array.push_back(300);
//
//	array.push_back(300);
//
//	array.push_back(300);
//
//	array.push_back(500);
//
//	vector<int>::iterator itor;
//
//	for (itor = array.begin(); itor != array.end(); itor++)
//	{
//		if (*itor == 300)
//		{
//			itor = array.erase(itor);
//		}
//	}
//	for (itor = array.begin(); itor != array.end(); itor++)
//	{
//		cout << *itor << " ";
//
//	}
//
//	return 0;
//}

int main()
{
	string s1;
	cin >> s1;
	cout << s1;
	return 0;
}