#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
//#include <array>
using namespace std;

//模板特化 —— 针对某些类型进行特殊化处理
//template<class T, size_t N = 20>
//class array
//{
//
//private:
//	T _a[N];
//};

struct Date
{
	Date(int year, int month, int day)
		:_year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator>(const Date& d) const
	{
		if ((_year > d._year)
			|| (_year == d._year && _month > d._month)
			|| (_year == d._year && _month == d._month && _day > d._day))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool operator<(const Date& d) const
	{
		if ((_year < d._year)
			|| (_year == d._year && _month < d._month)
			|| (_year == d._year && _month == d._month && _day < d._day))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	int _year;
	int _month;
	int _day;
};
//
//template<class T>
//bool Greater(T left, T right)
//{
//	return left > right;
//}
//template<>
//bool Greater<Date*>(Date* left, Date* right)
//{
//	return *left > *right;
//}
//
//
//namespace hjx
//{
//	template<class T>
//	struct less
//	{
//		bool operator()(const T& x1, const T& x2) const
//		{
//			return x1 < x2;
//		}
//	};
//
//	template<>
//	struct less<Date*>
//	{
//		bool operator()(Date* x1, Date* x2) const
//		{
//			return *x1 < *x2;
//		}
//	};
//}

template<class T1, class T2>
class Data
{
public:
	Data() { cout << "Data<T1, T2>" << endl; }
private:
	T1 _d1;
	T2 _d2;
};

//全特化
template<>
class Data<int, char>
{
public:
	Data() { cout << "Data<int, char>" << endl; }
private:
	int _d1;
	char _d2;
};

//偏特化
template<class T1>
class Data<T1, char>
{
public:
	Data() { cout << "Data<T1, char>" << endl; }
private:
};

template<class T1,class T2>
class Data<T1*, T2*>
{
public:
	Data() { cout << "Data<T1*, T2*>" << endl; }
private:
};

template<class T1, class T2>
class Data<T1&, T2&>
{
public:
	Data() { cout << "Data<T1&, T2&>" << endl; }
private:
};

template<class T1, class T2>
class Data<T1&, T2*>
{
public:
	Data() { cout << "Data<T1&, T2*>" << endl; }
private:
};
int main()
{
	/*array<int, 10> a1;
	array<double, 100> a2;*/

	//int a[10];
	//a[10];//越界读检查不到，越界写才检查到，设岗抽查
	//a1[10];
	Data<int, int> d0;
	Data<double, char> d1;
	Data<int, char> d2;
	Data<int*, int*> d3;
	Data<char*, char*> d4;
	Data<int*, char> d5;

	Data<int&, double&> d6;
	Data<int&, double&> d7;

	return 0;
}