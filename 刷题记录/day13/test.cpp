#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

//class Solution {
//public:
//    string modifyString(string s) {
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] == '?')
//            {
//                for (char ch = 'a'; ch <= 'z'; ch++)
//                {
//                    if ((i == 0 || s[i - 1] != ch) && (i == n - 1 || s[i + 1] != ch))
//                    {
//                        s[i] = ch;
//                        break;
//                    }
//                }
//            }
//        }
//        return s;
//    }
//};
//
//
//const int N = 100010;
//long long arr[N], dp[N];
//int n, q;
//
//int main() {
//    cin >> n >> q;
//    for (int i = 1; i <= n; i++) cin >> arr[i];
//    for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + arr[i];
//    while (q--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << dp[r] - dp[l - 1] << endl;
//    }
//    return 0;
//}

//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
//        if (timeSeries.size() == 1) return duration;
//        int ret = duration;
//        for (int i = 1; i < timeSeries.size(); i++)
//        {
//            if (timeSeries[i] - timeSeries[i - 1] >= duration) ret += duration;
//            else ret += timeSeries[i] - timeSeries[i - 1];
//        }
//        return ret;
//    }
//};