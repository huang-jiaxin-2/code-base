#include <iostream>
#include <vector>
using namespace std;

//组合总和—解法一
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        t = target;
//        dfs(candidates, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > t || pos == candidates.size())
//        {
//            return;
//        }
//
//        for (int i = pos; i < candidates.size(); i++)
//        {
//            path.push_back(candidates[i]);
//            dfs(candidates, i, sum + candidates[i]);
//            path.pop_back();
//        }
//    }
//};

//解法二
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        t = target;
//        dfs(candidates, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > t || pos == candidates.size())
//        {
//            return;
//        }
//
//        for (int k = 0; k * candidates[pos] + sum <= t; k++)
//        {
//            if (k) path.push_back(candidates[pos]);
//            dfs(candidates, pos + 1, sum + k * candidates[pos]);
//        }
//
//        for (int k = 1; k * candidates[pos] + sum <= t; k++)
//        {
//            path.pop_back();
//        }
//    }
//};

//组合
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int n, k;
//public:
//    vector<vector<int>> combine(int _n, int _k) {
//        n = _n;
//        k = _k;
//        dfs(1);
//        return ret;
//    }
//
//    void dfs(int start)
//    {
//        if (path.size() == k)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = start; i <= n; i++)
//        {
//            path.push_back(i);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//};