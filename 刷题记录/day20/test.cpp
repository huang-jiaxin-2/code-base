#include <iostream>
#include <algorithm>
#include <queue>
#include <string>
#include <vector>

using namespace std;

//class Solution {
//    typedef pair<int, int> PII;
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        int cur = image[sr][sc];
//        if (cur == color) return image;
//
//        int m = image.size(), n = image[0].size();
//
//        queue<PII> q;
//        int a, b;
//        q.push({ sr, sc });
//        while (q.size())
//        {
//            int a = q.front().first;
//            int b = q.front().second;
//            q.pop();
//            image[a][b] = color;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == cur)
//                {
//                    q.push({ x, y });
//                }
//            }
//        }
//        return image;
//    }
//};

//class Solution {
//    typedef pair<int, int> PII;
//    int m, n;
//    int vis[51][51];
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int ret;
//public:
//    void bfs(vector<vector<int>>& grid, int i, int j)
//    {
//        queue<PII> q;
//        q.push({ i, j });
//        vis[i][j] = true;
//        int tmp = 0;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            tmp++;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1 && !vis[x][y])
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                }
//            }
//            ret = max(ret, tmp);
//        }
//    }
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                {
//                    bfs(grid, i, j);
//                }
//            }
//
//        }
//        return ret;
//    }
//};
//
//int main()
//{
//	return 0;
//}