#include <iostream>
#include <algorithm>
#include <queue>
#include <string>
#include <vector>

using namespace std;

//class Solution {
//    typedef pair<int, int> PII;
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        int cur = image[sr][sc];
//        if (cur == color) return image;
//
//        int m = image.size(), n = image[0].size();
//
//        queue<PII> q;
//        int a, b;
//        q.push({ sr, sc });
//        while (q.size())
//        {
//            int a = q.front().first;
//            int b = q.front().second;
//            q.pop();
//            image[a][b] = color;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == cur)
//                {
//                    q.push({ x, y });
//                }
//            }
//        }
//        return image;
//    }
//};

//class Solution {
//    typedef pair<int, int> PII;
//    int m, n;
//    int vis[51][51];
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int ret;
//public:
//    void bfs(vector<vector<int>>& grid, int i, int j)
//    {
//        queue<PII> q;
//        q.push({ i, j });
//        vis[i][j] = true;
//        int tmp = 0;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            tmp++;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1 && !vis[x][y])
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                }
//            }
//            ret = max(ret, tmp);
//        }
//    }
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                {
//                    bfs(grid, i, j);
//                }
//            }
//
//        }
//        return ret;
//    }
//};
//
//int main()
//{
//	return 0;
//}

//class Solution {
//    int m, n;
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    void bfs(vector<vector<char>>& board, int i, int j)
//    {
//        queue<pair<int, int>> q;
//        q.push({ i, j });
//        board[i][j] = '.';
//
//        while (q.size())
//        {
//            int a = q.front().first;
//            int b = q.front().second;
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O')
//                {
//                    q.push({ x, y });
//                    board[x][y] = '.';
//                }
//            }
//        }
//    }
//    void solve(vector<vector<char>>& board) {
//        m = board.size(), n = board[0].size();
//        for (int j = 0; j < n; j++)
//        {
//            if (board[0][j] == 'O')
//                bfs(board, 0, j);
//            if (board[m - 1][j] == 'O')
//                bfs(board, m - 1, j);
//        }
//
//        for (int i = 0; i < m; i++)
//        {
//            if (board[i][0] == 'O')
//                bfs(board, i, 0);
//            if (board[i][n - 1] == 'O')
//                bfs(board, i, n - 1);
//        }
//
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (board[i][j] == 'O')
//                {
//                    board[i][j] = 'X';
//                }
//                else if (board[i][j] = '.')
//                {
//                    board[i][j] = 'O';
//                }
//            }
//        }
//    }
//};
//
//int main()
//{
//    vector<char> b1;
//    b1.push_back('X');
//    b1.push_back('X');
//    b1.push_back('X');
//    b1.push_back('X');
//    vector<char> b2;
//    b2.push_back('X');
//    b2.push_back('O');
//    b2.push_back('O');
//    b2.push_back('X');
//    vector<char> b3;
//    b3.push_back('X');
//    b3.push_back('X');
//    b3.push_back('O');
//    b3.push_back('X');
//    vector<char> b4;
//    b4.push_back('X');
//    b4.push_back('O');
//    b4.push_back('X');
//    b4.push_back('X');
//
//    vector<vector<char>> board;
//    board.push_back(b1);
//    board.push_back(b2);
//    board.push_back(b3);
//    board.push_back(b4);
//
//    Solution().solve(board);
//    return 0;
//}

//class Solution {
//    int m, n;
//    bool vis[101][101];
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    int nearestExit(vector<vector<char>>& maze, vector<int>& entrance) {
//        m = maze.size(), n = maze[0].size();
//        queue<pair<int, int>> q;
//        q.push({ entrance[0], entrance[1] });
//        vis[entrance[0]][entrance[1]] = true;
//        int step = 0;
//
//        while (q.size())
//        {
//            step++;
//            int sz = q.size();
//            for (int i = 0; i < sz; i++)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//
//                for (int j = 0; j < 4; j++)
//                {
//                    int x = a + dx[j], y = b + dy[j];
//                    if (x >= 0 && x < m && y >= 0 && y < n && maze[x][y] == '.' && !vis[x][y])
//                    {
//                        if (x == 0 || x == m - 1 || y == 0 || y == n - 1) return step;
//                        q.push({ x, y });
//                        vis[x][y] = true;
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//};

//class Solution {
//public:
//    int sumNumbers(TreeNode* root) {
//        return dfs(root, 0);
//    }
//
//    int dfs(TreeNode* root, int presum)
//    {
//        presum = 10 * presum + root->val;
//        if (root->left == nullptr && root->right == nullptr)
//            return presum;
//
//        int ret = 0;
//        if (root->left) ret += dfs(root->left, presum);
//        if (root->right) ret += dfs(root->right, presum);
//
//        return ret;
//    }
//};

//class Solution {
//public:
//    bool check(TreeNode* p, TreeNode* q)
//    {
//        if (!p && !q) return true;
//        if (!p || !q) return false;
//
//        return p->val == q->val && check(p->left, q->right) && check(p->right, q->left);
//    }
//    bool checkSymmetricTree(TreeNode* root) {
//        return check(root, root);
//    }
//};

//class Solution {
//public:
//    int count;
//    int ret;
//    void dfs(TreeNode* root)
//    {
//        if (root == nullptr || count == 0)
//        {
//            return;
//        }
//        dfs(root->left);
//        count--;
//        if (count == 0) ret = root->val;
//        dfs(root->right);
//    }
//    int kthSmallest(TreeNode* root, int k) {
//        count = k;
//        dfs(root);
//        return ret;
//    }
//};

//class Solution {
//public:
//    vector<int> tmp;
//    vector<vector<int>> ret;
//
//    void dfs(int cur, vector<int>& nums)
//    {
//        if (cur == nums.size())
//        {
//            ret.push_back(tmp);
//            return;
//        }
//
//        tmp.push_back(nums[cur]);
//        dfs(cur + 1, nums);
//        tmp.pop_back();
//        dfs(cur + 1, nums);
//    }
//    vector<vector<int>> subsets(vector<int>& nums) {
//        dfs(0, nums);
//        return ret;
//    }
//};

//class Solution {
//    long prev = LONG_MIN;
//public:
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true;
//        bool left = isValidBST(root->left);
//        if (left == false) return false;
//
//        bool cur = false;
//        if (root->val > prev)
//            cur = true;
//
//        if (cur == false) return false;
//
//        prev = root->val;
//        bool right = isValidBST(root->right);
//        return right && left && cur;
//    }
//};

//class A
//{
//public:
//	A()
//	{
//		Animal();
//	}
//
//	virtual void Animal()const
//	{
//		cout << "喵~" << endl;
//	}
//};
//
//class B : public A
//{
//public:
//	B()
//	{
//		Animal();
//	}
//	virtual void Animal()const
//	{
//		cout << "旺~" << endl;
//	}
//};
//
//int main()
//{
//	B b;
//	//b.Animal();
//	return 0;
//}

class Solution {
    vector<vector<int>> ret;
    vector<int> path;
    bool check[9];
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        dfs(nums, 0);
        return ret;
    }

    void dfs(vector<int>& nums, int pos)
    {
        if (pos == nums.size())
        {
            ret.push_back(path);
            return;
        }

        for (int i = 0; i < nums.size(); i++)
        {
            //不合法情况：check[i] == true || (i != 0 && nums[i] == nums[i - 1] && check[i - 1] == false)
            if (check[i] == false && (i == 0 || nums[i] != nums[i - 1] || check[i - 1] == true))//合法情况
            {
                path.push_back(nums[i]);
                check[i] = true;
                dfs(nums, pos + 1);
                path.pop_back();
                check[i] = false;
            }
        }
    }
};