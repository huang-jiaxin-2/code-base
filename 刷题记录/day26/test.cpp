#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

//组合总和—解法一
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        t = target;
//        dfs(candidates, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > t || pos == candidates.size())
//        {
//            return;
//        }
//
//        for (int i = pos; i < candidates.size(); i++)
//        {
//            path.push_back(candidates[i]);
//            dfs(candidates, i, sum + candidates[i]);
//            path.pop_back();
//        }
//    }
//};

//解法二
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        t = target;
//        dfs(candidates, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > t || pos == candidates.size())
//        {
//            return;
//        }
//
//        for (int k = 0; k * candidates[pos] + sum <= t; k++)
//        {
//            if (k) path.push_back(candidates[pos]);
//            dfs(candidates, pos + 1, sum + k * candidates[pos]);
//        }
//
//        for (int k = 1; k * candidates[pos] + sum <= t; k++)
//        {
//            path.pop_back();
//        }
//    }
//};

//组合
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int n, k;
//public:
//    vector<vector<int>> combine(int _n, int _k) {
//        n = _n;
//        k = _k;
//        dfs(1);
//        return ret;
//    }
//
//    void dfs(int start)
//    {
//        if (path.size() == k)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = start; i <= n; i++)
//        {
//            path.push_back(i);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//};


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    int combinationSum4(vector<int>& nums, int target) {
//        sort(nums.begin(), nums.end());
//        t = target;
//        dfs(nums, 0, 0);
//        return ret.size();
//    }
//
//    void dfs(vector<int>& nums, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            for (auto v : path)
//            {
//                cout << v;
//            }
//            return;
//        }
//        if (sum > t || pos > nums.size())
//        {
//            return;
//        }
//
//        for (int i = pos; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            dfs(nums, i + 1, sum + nums[i]);
//            path.pop_back();
//        }
//    }
//};
//
//class Solution {
//public:
//    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
//        if (k == 0) return 0;
//        int ret = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (nums[i] < k) ret++;
//            else continue;
//            int mul = nums[i];
//            for (int j = i + 1; j < nums.size(); j++)
//            {
//                mul *= nums[j];
//                if (mul < k) ret++;
//                else break;
//            }
//        }
//        return ret;
//    }
//};
//
// class Solution {
//public:
//    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
//        if (k == 0) return 0;
//        int ret = 0, j = 0;
//        int mul = 1;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            mul *= nums[i];
//            while (j <= i && mul >= k)
//            {
//                mul /= nums[j];
//                j++;
//            }
//            ret += i - j + 1;
//        }
//        return ret;
//    }
//};
// 
//int main()
//{
//    vector<int> nums = { 1,2,3 };
//    Solution().combinationSum4(nums, 4);
//    return 0;
//}