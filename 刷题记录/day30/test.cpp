#include <iostream>
#include <string>
#include <vector>
#include <math.h>
using namespace std;

////单词搜索
//class Solution {
//    bool visit[7][7];
//    int m, n;
//public:
//    bool exist(vector<vector<char>>& board, string word) {
//        m = board.size(), n = board[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (board[i][j] == word[0])
//                {
//                    visit[i][j] = true;
//                    if (dfs(board, i, j, word, 1)) return true;
//                    visit[i][j] = false;
//                }
//            }
//        }
//        return false;
//    }
//
//    int dx[4] = { 0, 0, -1, 1 };
//    int dy[4] = { -1, 1, 0, 0 };
//
//    bool dfs(vector<vector<char>>& board, int x, int y, string& word, int pos)
//    {
//        if (pos == word.size())
//        {
//            return true;
//        }
//
//        for (int k = 0; k < 4; k++)
//        {
//            int a = x + dx[k], b = y + dy[k];
//            if (a >= 0 && a < m && b >= 0 && b < n && !visit[a][b] && board[a][b] == word[pos])
//            {
//                visit[a][b] = true;
//                if (dfs(board, a, b, word, pos + 1)) return true;
//                visit[a][b] = false;
//            }
//        }
//        return false;
//    }
//};
//
////黄金矿工
//class Solution {
//    bool visit[16][16];
//    int ret;
//    int m, n;
//    int dx[4] = { 0, 0, -1, 1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    int getMaximumGold(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] != 0)
//                {
//                    visit[i][j] = true;
//                    dfs(grid, i, j, grid[i][j]);
//                    visit[i][j] = false;
//                }
//            }
//        }
//        return ret;
//    }
//
//    void dfs(vector<vector<int>>& grid, int i, int j, int sum)
//    {
//        ret = max(ret, sum);
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !visit[x][y] && grid[x][y] != 0)
//            {
//                visit[x][y] = true;
//                dfs(grid, x, y, sum + grid[x][y]);
//                visit[x][y] = false;
//            }
//        }
//    }
//};

//找出不大于n的最大质数
//bool Is_prime(int n)
//{
//    for (int i = 2; i <= sqrt(n); i++)
//    {
//        if (n % i == 0) return false;
//    }
//    return true;
//}
//
//int find_max(int n)
//{
//    int i = 0;
//    for (i = n; i > 0; i--)
//    {
//        if (Is_prime(i) == true) break;
//    }
//    return i;
//}

//int main()
//{
//    cout << find_max(80) << endl;
//    return 0;
//}

//int Last(int n, int m)
//{
//	int ret = 0;
//	vector<int> v(n, 0);
//	for (int i = 0; i < n; i++)
//	{
//		v[i] = i + 1;
//	}
//	
//}
//
//int main()
//{
//
//	return 0;
//}

//string splitTest(string str, string split) {
//	string s;
//	int k = 0;
//	for (int i = 0; i < str.size(); i++)
//	{
//		if (str[i] == split[0])
//		{
//
//		}
//	}
//}

//不同路径III
class Solution {
    int step;
    int ret;
    int n, m;
    bool visit[21][21];
public:
    int uniquePathsIII(vector<vector<int>>& grid) {
        int x = 0, y = 0;
        n = grid.size(), m = grid[0].size();
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (grid[i][j] == 0)
                    step++;
                if (grid[i][j] == 1)
                {
                    x = i;
                    y = j;
                }
            }
        }
        step += 2;
        visit[x][y] = true;
        dfs(grid, x, y, 1);
        return ret;
    }

    int dx[4] = { 0, 0, -1, 1 };
    int dy[4] = { -1, 1, 0, 0 };
    void dfs(vector<vector<int>>& grid, int x, int y, int count)
    {
        if (grid[x][y] == 2)
        {
            if (count == step)
                ret++;
            return;
        }

        for (int k = 0; k < 4; k++)
        {
            int a = x + dx[k], b = y + dy[k];
            if (a >= 0 && a < n && b >= 0 && b < m && !visit[a][b] && grid[a][b] != -1)
            {
                visit[a][b] = true;
                dfs(grid, a, b, count + 1);
                visit[a][b] = false;
            }
        }
    }
};

int main()
{
	
	return 0;
}