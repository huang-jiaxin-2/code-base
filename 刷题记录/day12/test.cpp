﻿#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;
//
//class Solution {
//public:
//    vector<int> plusOne(vector<int>& digits) {
//        int n = digits.size();
//        for (int i = n - 1; i >= 0; --i) {
//            if (digits[i] != 9) {
//                ++digits[i];
//                for (int j = i + 1; j < n; ++j) {
//                    digits[j] = 0;
//                }
//                return digits;
//            }
//        }
//
//        // digits 中所有的元素均为 9
//        vector<int> ans(n + 1);
//        ans[0] = 1;
//        return ans;
//    }
//};
//
//int main()
//{
//    vector<int> v;
//    v.push_back(3);
//    v.push_back(2);
//    v.push_back(9);
//
//    vector<int> tmp = Solution().plusOne(v);
//    return 0;
//}

//class Solution {
//public:
//    bool isMonotonic(vector<int>& nums) {
//        bool flag1 = true, flag2 = true;
//        int n = nums.size();
//        for (int i = 0; i < n - 1; i++)
//        {
//            if (nums[i] > nums[i + 1])
//                flag1 = false;
//            if (nums[i] < nums[i + 1])
//                flag2 = false;
//        }
//        return flag1 || flag2;
//    }
//};

//struct TreeNode {
//    int val;
//    TreeNode* left;
//    TreeNode* right;
//    TreeNode() : val(0), left(nullptr), right(nullptr) {}
//    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
//};
//
//
//class Solution {
//public:
//    void inorder(TreeNode* node, vector<int>& res)
//    {
//        if (node == nullptr)
//            return;
//        inorder(node->left, res);
//        res.push_back(node->val);
//        inorder(node->right, res);
//    }
//    TreeNode* increasingBST(TreeNode* root) {
//        vector<int> res;
//        inorder(root, res);
//
//        TreeNode* dummyNode = new TreeNode(-1);
//        TreeNode* cur = dummyNode;
//        for (auto v : res)
//        {
//            cur->right = new TreeNode(v);
//            cur = cur->right;
//        }
//
//        return dummyNode->right;
//    }
//};

//class Solution {
//public:
//    int myAtoi(string s) {
//        int res = 0;
//        int i = 0, n = s.size(), flag = 1, num = 0;
//        while (i < n && s[i] == ' ') i++;
//        if (i < n && s[i] == '-') { flag = -1; i++; }
//
//        else if (i < n && s[i] == '+') i++;
//        for (i; i < n; i++)
//        {
//            if (s[i] >= '0' && s[i] <= '9')
//            {
//                num = s[i] - '0';
//                if (res > (INT_MAX - num) / 10) return INT_MAX;
//                if (res < (INT_MIN + num) / 10) return INT_MIN;
//                res = res * 10 + flag * num;
//            }
//            if (!(s[i] >= '0' && s[i] <= '9'))
//            {
//                break;
//            }
//        }
//        return res;
//    }
//};
//
//int main()
//{
//    int res = Solution().myAtoi("+-42");
//    cout << res << endl;
//    return 0;
//}

//class Solution {
//public:
//    bool isPalindrome(int x) {
//        if (x < 0 || (x % 10 == 0 && x != 0)) return false;
//
//        int res = 0;
//        while (x > res)
//        {
//
//            res = res * 10 + (x % 10);
//            x /= 10;
//        }
//        return res == x || x == (res / 10);
//    }
//};
//
//int main()
//{
//    Solution().isPalindrome(121);
//    return 0;
//}

//将有序数组装换为二叉搜索树
//class Solution {
//public:
//    TreeNode* _sortedArrayToBST(vector<int>& nums, int left, int right)
//    {
//        if (left > right)
//        {
//            return nullptr;
//        }
//        int mid = (left + right) / 2;
//        TreeNode* root = new TreeNode(nums[mid]);
//        root->left = _sortedArrayToBST(nums, left, mid - 1);
//        root->right = _sortedArrayToBST(nums, mid + 1, right);
//        return root;
//    }
//    TreeNode* sortedArrayToBST(vector<int>& nums) {
//        return _sortedArrayToBST(nums, 0, nums.size() - 1);
//    }
//};

//缺失的第一个正数
//class Solution {
//public:
//    int firstMissingPositive(vector<int>& nums) {
//        int n = nums.size();
//        for (int& num : nums)
//        {
//            if (num <= 0)
//                num = n + 1;
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            int num = abs(nums[i]);
//            if (num <= n)
//            {
//                nums[num - 1] = -abs(nums[num - 1]);
//            }
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[i] > 0)
//            {
//                return i + 1;
//            }
//        }
//
//        return n + 1;
//
//    }
//};
//
//int main()
//{
//
//	return 0;
//}

//class Solution {
//public:
//    int divide(int dividend, int divisor) {
//        int res = 0, flag1 = 1, flag2 = 1;
//        if (dividend < 0)
//        {
//            flag1 = -1;
//        }
//        if (divisor < 0)
//        {
//            flag2 = -1;
//        }
//        dividend = abs(dividend);
//        divisor = abs(divisor);
//        while ((dividend - divisor) >= 0)
//        {
//            if (res > INT_MAX / 10)
//            {
//                return INT_MAX;
//            }
//            if (res < INT_MIN / 10)
//            {
//                return INT_MIN;
//            }
//            dividend = dividend - divisor;
//            res++;
//        }
//
//        return res * flag1 * flag2;
//    }
//};
//
//int main()
//{
//    cout << Solution().divide(-2147483648, 1);
//
//    return 0;
//}

//整数翻转
//class Solution {
//public:
//    int reverse(int x) {
//        int res = 0;
//        while (x)
//        {
//            if (res > INT_MAX / 10 || res < INT_MIN / 10)
//            {
//                return 0;
//            }
//            res = res * 10 + x % 10;
//            x /= 10;
//        }
//        return res;
//    }
//};

//最后一个单词的长度
//class Solution {
//public:
//    int lengthOfLastWord(string s) {
//        int n = s.size() - 1, count = 0;
//        while (s[n] == ' ')
//            n--;
//
//        while (n >= 0 && s[n] != ' ')
//        {
//            count++;
//            n--;
//        }
//
//        return count;
//    }
//};

//调整数组顺序使奇数位于偶数前面
//class Solution {
//public:
//    void reOrderArray(vector<int>& nums)
//    {
//        if (nums.empty())
//        {
//            return;
//        }
//
//        int k = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if ((nums[i] & 1) == 1)
//            {
//                int tmp = nums[i];
//                int j = i;
//                while (j > k)
//                {
//                    nums[j] = nums[j - 1];
//                    j--;
//                }
//                nums[k++] = tmp;
//            }
//        }
//    }
//};
//
//int main()
//{
//    vector<int> nums;
//    nums.push_back(1);
//    nums.push_back(2);
//    nums.push_back(3);
//    nums.push_back(4);
//    nums.push_back(5);
//    nums.push_back(6);
//    Solution().reOrderArray(nums);
//    for (auto v : nums)
//    {
//        cout << v << " ";
//    }
//    return 0;
//}

//数组出现超过次数一半的数字
//class Solution {
//public:
//    int MoreThanHalfNum_Solution(vector<int>& numbers) {
//        if (numbers.size() == 0)
//        {
//            return 0;
//        }
//
//        int target = numbers[0];
//        int times = 1;
//        for (int i = 1; i < numbers.size(); i++)
//        {
//            if (times == 0)
//            {
//                target = numbers[i];
//            }
//            if (target == numbers[i])
//            {
//                times++;
//            }
//            else {
//                times--;
//            }
//        }
//
//        times = 0;
//        for (int i = 0; i < numbers.size(); i++)
//        {
//            if (target == numbers[i])
//            {
//                times++;
//            }
//        }
//        return times > numbers.size() / 2 ? target : 0;
//    }
//};

//替换空格
//class Solution {
//public:
//    string replaceSpace(string s) {
//        int count = 0;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i] == ' ')
//            {
//                count++;
//            }
//        }
//
//        int old_len = s.size();
//        int new_len = s.size() + 2 * count;
//        s.resize(new_len);
//        while (old_len >= 0 && new_len >= 0)
//        {
//            if (s[old_len] == ' ')
//            {
//                s[new_len--] = '0';
//                s[new_len--] = '2';
//                s[new_len--] = '%';
//                old_len--;
//            }
//            else
//            {
//                s[new_len--] = s[old_len--];
//            }
//        }
//
//        for (auto str : s)
//        {
//            cout << s;
//        }
//        return s;
//    }
//};
//
//int main()
//{
//    string s = "We Are Happy";
//    Solution().replaceSpace(s);
//    return 0;
//}

//从尾到头打印链表
//class Solution {
//public:
//    vector<int> printListFromTailToHead(ListNode* head) {
//        vector<int> v;
//        while (head != nullptr)
//        {
//            v.push_back(head->val);
//            head = head->next;
//        }
//        reverse(v.begin(), v.end());
//        return v;
//    }
//};

//class Solution {
//public:
//    void printListFromTailToHeadH(ListNode* head, vector<int>& v)
//    {
//        if (head == nullptr)
//        {
//            return;
//        }
//        printListFromTailToHeadH(head->next, v);
//        v.push_back(head->val);
//    }
//    vector<int> printListFromTailToHead(ListNode* head) {
//        vector<int> v;
//        printListFromTailToHeadH(head, v);
//        return v;
//    }
//};

//重建二叉树
//class Solution {
//public:
//    TreeNode* reConstructBinaryTreeCore(vector<int>& preOrder, int pre_start, int pre_end, vector<int>& vinOrder, int vin_start, int vin_end)
//    {
//        if (pre_start > pre_end || vin_start > vin_end)
//            return nullptr;
//        TreeNode* root = new TreeNode(preOrder[pre_start]);
//        for (int i = vin_start; i <= vin_end; i++)
//        {
//            if (root->val == vinOrder[i])
//            {
//                //难点：区间范围的确定[vin_start, i - 1) i [i + 1, vin_end]
//                root->left = reConstructBinaryTreeCore(preOrder, pre_start + 1, pre_start + i - vin_start, vinOrder, vin_start, i - 1);
//                root->right = reConstructBinaryTreeCore(preOrder, pre_start + i - vin_start + 1, pre_end, vinOrder, i + 1, vin_end);
//                break;
//            }
//        }
//        return root;
//    }
//
//    TreeNode* reConstructBinaryTree(vector<int>& preOrder, vector<int>& vinOrder) {
//        // write code here
//        if (preOrder.empty() || vinOrder.empty() || preOrder.size() != vinOrder.size())
//        {
//            return nullptr;
//        }
//        return reConstructBinaryTreeCore(preOrder, 0, preOrder.size() - 1, vinOrder, 0, vinOrder.size() - 1);
//    }
//};

//int main()
//{
//	string s = "abcdefg";
//	int a = 3;
//	reverse(s.begin(), s.begin() + a);
//	for (auto e : s)
//	{
//		cout << e;
//	}
//	return 0;
//}

//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        int n = s.size();
//        for (int i = 0; i < n; i += 2 * k)
//        {
//            reverse(s.begin() + i, s.begin() + min(i + k, n));
//        }
//        return s;
//    }
//};

//class Solution {
//public:
//    string reverseWords(string s) {
//        int n = s.size();
//        int flagi = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] == ' ')
//            {
//                reverse(s.begin() + flagi, s.begin() + i);
//                flagi = i + 1;
//            }
//        }
//        reverse(s.begin() + flagi, s.end());
//        return s;
//    }
//};


//int main()
//{
//	int ar[] = { 1,2,3,4,0,5,6,7,8,9 };
//	int n = sizeof(ar) / sizeof(int);
//	vector<int> v(ar, ar + n);
//	vector<int>::iterator it = v.begin();
//	while (it != v.end())
//	{
//		if (*it != 0)
//			cout << *it;
//		else
//		{
//			it = v.erase(it);
//		}
//		
//		it++;
//	}
//	return 0;
//}

//struct Student
//{
//	char _name[20];
//	int _major = 0;
//	int _english = 0;
//	int _math = 0;
//	int _sum = 0;
//};
//
//
//int main()
//{
//	struct Student s[20];
//	int n = 0;
//	scanf_s("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		cin >> s[i]._name >> s[i]._english >> s[i]._math >> s[i]._major;
//		s[i]._sum = s[i]._english, s[i]._math, s[i]._major;
//	}
//
//	int max = -1, max_e = -1, max_m = -1, tmp = 0;
//	for (int i = 0; i < n; i++)
//	{
//
//		if (s[i]._sum > max)
//		{
//			max = s[i]._sum;
//			max_e = s[i]._english;
//			max_m = s[i]._math;
//			tmp = i;
//		}
//
//		if (s[i]._sum == max)
//		{
//			if (s[i]._english == max_e)
//			{
//				if (s[i]._math > max_m)
//				{
//					tmp = i;
//				}
//			}
//			else if (s[i]._english > max_e)
//			{
//				tmp = i;
//			}
//		}
//	}
//
//	printf("%s\n", s[tmp]._name);
//}
//

//int main()
//{
//	int n = 0;
//	int min_v = 0, max_v = INT_MAX;
//	cin >> n;
//	while (n--)
//	{
//		int o = 0, x = 0;
//		cin >> o >> x;
//		int left = 0, right = o + 1;
//		int mid = 0;
//		 
//
//		while (left + 1 != right)
//		{
//			mid = (left + right) / 2;
//			if ((o / mid) <= x) right = mid;
//			else left = mid;
//		}
//		min_v = max(min_v, right);
//
//		left = 0, right = o + 1;
//		mid = 0;
//		while (left + 1 != right)
//		{
//			mid = (left + right) / 2;
//			if ((o / mid) >= x) left = mid;
//			else right = mid;
//		}
//		max_v = min(max_v, left);
//	}
//	cout << min_v << " " << max_v << endl;
//	return 0;
//}


//const int inf = 0x3f3f3f3f;
//int n;		//n组数据！ 
//int Min_v = 0, Max_v = inf;	//分别存储最小值集合里面最大的最小值，最大值集合里面的最小的最大值！ 
//
//int main()
//{
//	cin >> n;
//
//	while (n--)
//	{
//		int a, b;
//		cin >> a >> b;
//
//		//找能够使得 A个O 恰好转化为 B个X的最小转化值！ 
//		int l = 0, r = a + 1;
//		while (l + 1 != r)
//		{
//			int v = l + r >> 1;
//			if (a / v <= b) r = v;		//找最小值的最大值，则是 <=，即找到合法区域里的左边界！ 
//			else l = v;
//		}
//
//		Min_v = max(Min_v, r);
//
//		//找能够使得 A个O 恰好转化为 B个X的最大转化值！ 
//		l = 0, r = a + 1;
//		while (l + 1 != r)
//		{
//			int v = l + r >> 1;
//			if (a / v >= b) l = v;		//找最大值里的最小值，就是 >= 即找合法区域的右边界！ 
//			else r = v;
//		}
//		Max_v = min(Max_v, l);
//	}
//
//	cout << Min_v << " " << Max_v << endl;
//	return 0;
//}

//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        //1.创建dp表
//        //2.状态转移方程
//        //3.初始化
//        //4.填表 
//        int m = grid.size(), n = grid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        dp[1][0] = dp[0][1] = 0;
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};

//class Solution {
//public:
//    int calculateMinimumHP(vector<vector<int>>& dungeon) {
//        int m = dungeon.size(), n = dungeon[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        dp[m][n - 1] = dp[m - 1][n] = 1;
//        for (int i = m - 1; i >= 0; i--)
//        {
//            for (int j = n - 1; j >= 0; j--)
//            {
//                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
//                dp[i][j] = max(1, dp[i][j]);
//            }
//        }
//        return dp[0][0];
//    }
//};

//class Solution {
//public:
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0) return 0;
//        vector<int> f(n);
//        auto g = f;
//        f[0] = nums[0];
//        for (int i = 1; i < n; i++)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};

//class Solution {
//public:
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n + 1, vector<int>(3));
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i - 1][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i - 1][1];
//            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i - 1][2];
//        }
//        return min(dp[n][0], min(dp[n][1], dp[n][2]));
//    }
//};

//class Solution {
//public:
//    int deleteAndEarn(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> arr(10001);
//        for (auto num : nums)
//        {
//            arr[num] += num;
//        }
//        return deleteAndEarn1(arr);
//    }
//    int deleteAndEarn1(vector<int>& arr)
//    {
//        int n = arr.size();
//        vector<int> f(n);
//        auto g = f;
//        for (int i = 1; i < n; i++)
//        {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};

//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        int f = nums[0] + rob1(nums, 2, n - 2);
//        int g = rob1(nums, 1, n - 1);
//        return max(f, g);
//    }
//    int rob1(vector<int>& nums, int left, int right) {
//        if (left > right) return 0;
//        int n = nums.size();
//        vector<int> f(n);
//        auto g = f;
//        f[left] = nums[left];
//        for (int i = left + 1; i <= right; i++)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[right], g[right]);
//    }
//};

//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);//买入
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
//            dp[i][2] = dp[i - 1][0] + prices[i];
//        }
//        return max(dp[n - 1][1], dp[n - 1][2]);
//    }
//};

//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] < target) return left + 1;
//        return left;
//    }
//};

//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        //出理边界情况
//        if (nums.size() == 0) return { -1, -1 };
//
//        //二分左端点
//        int begin = 0;
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] != target) return { -1, -1 };
//        else begin = left;
//
//        //二分右端点
//        right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] <= target) left = mid;
//            else right = mid - 1;
//        }
//        return { begin, left };
//    }
//};

//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int left = 0, right = records.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (records[mid] == mid) left = mid + 1;
//            else right = mid;
//        }
//        if (records[left] == left) return left + 1;
//        return left;
//    }
//};

//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > nums[right]) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};

//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int left = 0, right = arr.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (arr[mid] > arr[mid - 1]) left = mid;
//            else right = mid - 1;
//        }
//        return left;
//    }
//};

//class Solution {
//public:
//    int findPeakElement(vector<int>& nums) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > nums[mid - 1]) left = mid;
//            else right = mid - 1;
//        }
//        return left;
//    }
//};

//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] < target) return left + 1;
//        return left;
//    }
//};

//class Solution {
//public:
//    int mySqrt(int x) {
//        if (x < 1) return 0;
//        int left = 1, right = x;
//        while (left < right)
//        {
//            long long mid = left + (right - left + 1) / 2;
//            if (mid * mid <= x) left = mid;
//            else right = mid - 1;
//        }
//        return left;
//    }
//};