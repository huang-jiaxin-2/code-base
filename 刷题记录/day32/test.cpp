#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;

//class Solution {
//    int memo[201][201];
//public:
//    int getMoneyAmount(int n) {
//        return dfs(1, n);
//    }
//
//    int dfs(int left, int right)
//    {
//        if (left >= right) return 0;
//        if (memo[left][right] != 0) return memo[left][right];
//
//        int ret = INT_MAX;
//        for (int i = left; i <= right; i++)
//        {
//            int x = dfs(left, i - 1);
//            int y = dfs(i + 1, right);
//            ret = min(ret, i + max(x, y));
//        }
//        memo[left][right] = ret;
//        return ret;
//    }
//};

class Solution {
    int memo[201][201];
    int dx[4] = { -1, 1, 0, 0 };
    int dy[4] = { 0, 0, -1, 1 };
public:
    int longestIncreasingPath(vector<vector<int>>& matrix) {
        memset(memo, -1, sizeof memo);
        int n = matrix.size(), m = matrix[0].size();
        int ret = 0;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                ret = max(ret, dfs(i, j, matrix, n, m));
            }
        }
        return ret;
    }

    int dfs(int i, int j, vector<vector<int>>& matrix, int n, int m)
    {
        int ret = 1;
        if (memo[i][j] != -1) return memo[i][j];
        for (int k = 0; k < 4; k++)
        {
            int x = dx[k] + i, y = dy[k] + j;
            if (x >= 0 && x < n && y >= 0 && y < m && matrix[i][j] < matrix[x][y])
            {
                ret = max(ret, dfs(x, y, matrix, n, m) + 1);
            }
        }
        memo[i][j] = ret;
        return ret;
    }
};