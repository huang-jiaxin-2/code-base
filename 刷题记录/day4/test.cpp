#include <iostream>
#include <vector>

using namespace std;
//
//class Solution {
//public:
//    vector<int> plusOne(vector<int>& digits) {
//        int n = digits.size();
//        for (int i = n - 1; i >= 0; --i) {
//            if (digits[i] != 9) {
//                ++digits[i];
//                for (int j = i + 1; j < n; ++j) {
//                    digits[j] = 0;
//                }
//                return digits;
//            }
//        }
//
//        // digits 中所有的元素均为 9
//        vector<int> ans(n + 1);
//        ans[0] = 1;
//        return ans;
//    }
//};
//
//int main()
//{
//    vector<int> v;
//    v.push_back(3);
//    v.push_back(2);
//    v.push_back(9);
//
//    vector<int> tmp = Solution().plusOne(v);
//    return 0;
//}

//class Solution {
//public:
//    bool isMonotonic(vector<int>& nums) {
//        bool flag1 = true, flag2 = true;
//        int n = nums.size();
//        for (int i = 0; i < n - 1; i++)
//        {
//            if (nums[i] > nums[i + 1])
//                flag1 = false;
//            if (nums[i] < nums[i + 1])
//                flag2 = false;
//        }
//        return flag1 || flag2;
//    }
//};

//struct TreeNode {
//    int val;
//    TreeNode* left;
//    TreeNode* right;
//    TreeNode() : val(0), left(nullptr), right(nullptr) {}
//    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
//};
//
//
//class Solution {
//public:
//    void inorder(TreeNode* node, vector<int>& res)
//    {
//        if (node == nullptr)
//            return;
//        inorder(node->left, res);
//        res.push_back(node->val);
//        inorder(node->right, res);
//    }
//    TreeNode* increasingBST(TreeNode* root) {
//        vector<int> res;
//        inorder(root, res);
//
//        TreeNode* dummyNode = new TreeNode(-1);
//        TreeNode* cur = dummyNode;
//        for (auto v : res)
//        {
//            cur->right = new TreeNode(v);
//            cur = cur->right;
//        }
//
//        return dummyNode->right;
//    }
//};

//class Solution {
//public:
//    int myAtoi(string s) {
//        int res = 0;
//        int i = 0, n = s.size(), flag = 1, num = 0;
//        while (i < n && s[i] == ' ') i++;
//        if (i < n && s[i] == '-') { flag = -1; i++; }
//
//        else if (i < n && s[i] == '+') i++;
//        for (i; i < n; i++)
//        {
//            if (s[i] >= '0' && s[i] <= '9')
//            {
//                num = s[i] - '0';
//                if (res > (INT_MAX - num) / 10) return INT_MAX;
//                if (res < (INT_MIN + num) / 10) return INT_MIN;
//                res = res * 10 + flag * num;
//            }
//            if (!(s[i] >= '0' && s[i] <= '9'))
//            {
//                break;
//            }
//        }
//        return res;
//    }
//};
//
//int main()
//{
//    int res = Solution().myAtoi("+-42");
//    cout << res << endl;
//    return 0;
//}

//class Solution {
//public:
//    bool isPalindrome(int x) {
//        if (x < 0 || (x % 10 == 0 && x != 0)) return false;
//
//        int res = 0;
//        while (x > res)
//        {
//
//            res = res * 10 + (x % 10);
//            x /= 10;
//        }
//        return res == x || x == (res / 10);
//    }
//};
//
//int main()
//{
//    Solution().isPalindrome(121);
//    return 0;
//}

//将有序数组装换为二叉搜索树
//class Solution {
//public:
//    TreeNode* _sortedArrayToBST(vector<int>& nums, int left, int right)
//    {
//        if (left > right)
//        {
//            return nullptr;
//        }
//        int mid = (left + right) / 2;
//        TreeNode* root = new TreeNode(nums[mid]);
//        root->left = _sortedArrayToBST(nums, left, mid - 1);
//        root->right = _sortedArrayToBST(nums, mid + 1, right);
//        return root;
//    }
//    TreeNode* sortedArrayToBST(vector<int>& nums) {
//        return _sortedArrayToBST(nums, 0, nums.size() - 1);
//    }
//};

//缺失的第一个正数
//class Solution {
//public:
//    int firstMissingPositive(vector<int>& nums) {
//        int n = nums.size();
//        for (int& num : nums)
//        {
//            if (num <= 0)
//                num = n + 1;
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            int num = abs(nums[i]);
//            if (num <= n)
//            {
//                nums[num - 1] = -abs(nums[num - 1]);
//            }
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[i] > 0)
//            {
//                return i + 1;
//            }
//        }
//
//        return n + 1;
//
//    }
//};
//
//int main()
//{
//
//	return 0;
//}

//class Solution {
//public:
//    int divide(int dividend, int divisor) {
//        int res = 0, flag1 = 1, flag2 = 1;
//        if (dividend < 0)
//        {
//            flag1 = -1;
//        }
//        if (divisor < 0)
//        {
//            flag2 = -1;
//        }
//        dividend = abs(dividend);
//        divisor = abs(divisor);
//        while ((dividend - divisor) >= 0)
//        {
//            if (res > INT_MAX / 10)
//            {
//                return INT_MAX;
//            }
//            if (res < INT_MIN / 10)
//            {
//                return INT_MIN;
//            }
//            dividend = dividend - divisor;
//            res++;
//        }
//
//        return res * flag1 * flag2;
//    }
//};
//
//int main()
//{
//    cout << Solution().divide(-2147483648, 1);
//
//    return 0;
//}

//整数翻转
//class Solution {
//public:
//    int reverse(int x) {
//        int res = 0;
//        while (x)
//        {
//            if (res > INT_MAX / 10 || res < INT_MIN / 10)
//            {
//                return 0;
//            }
//            res = res * 10 + x % 10;
//            x /= 10;
//        }
//        return res;
//    }
//};

//最后一个单词的长度
//class Solution {
//public:
//    int lengthOfLastWord(string s) {
//        int n = s.size() - 1, count = 0;
//        while (s[n] == ' ')
//            n--;
//
//        while (n >= 0 && s[n] != ' ')
//        {
//            count++;
//            n--;
//        }
//
//        return count;
//    }
//};
