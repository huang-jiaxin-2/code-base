#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

//class Solution {
//public:
//    string modifyString(string s) {
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] == '?')
//            {
//                for (char ch = 'a'; ch <= 'z'; ch++)
//                {
//                    if ((i == 0 || s[i - 1] != ch) && (i == n - 1 || s[i + 1] != ch))
//                    {
//                        s[i] = ch;
//                        break;
//                    }
//                }
//            }
//        }
//        return s;
//    }
//};
//
//
//const int N = 100010;
//long long arr[N], dp[N];
//int n, q;
//
//int main() {
//    cin >> n >> q;
//    for (int i = 1; i <= n; i++) cin >> arr[i];
//    for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + arr[i];
//    while (q--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << dp[r] - dp[l - 1] << endl;
//    }
//    return 0;
//}

//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
//        if (timeSeries.size() == 1) return duration;
//        int ret = duration;
//        for (int i = 1; i < timeSeries.size(); i++)
//        {
//            if (timeSeries[i] - timeSeries[i - 1] >= duration) ret += duration;
//            else ret += timeSeries[i] - timeSeries[i - 1];
//        }
//        return ret;
//    }
//};

//int main()
//{
//	int n;
//	double avg = 0, score = 0, max = 0, min = 100;
//	scanf_s("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		scanf_s("%lf", &score);
//		if (max < score)
//		{
//			max = score;
//		}
//		if (min > score)
//		{
//			min = score;
//		}
//		avg += score;
//	}
//	avg = (avg - max - min) / (n - 2);
//	printf("%.2lf", avg);
//	return 0;
//}
//
//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto num : nums)
//        {
//            ret ^= num;
//        }
//        for (int i = 0; i <= nums.size(); i++) ret ^= i;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //高斯求和
//        int n = nums.size();
//        int ret = n * (n + 1) / 2;
//        int sum = 0;
//        for (auto num : nums)
//        {
//            sum += num;
//        }
//        return ret - sum;
//    }
//};

//class Solution {
//public:
//    bool hasAllCodes(string s, int k) {
//        if (s.size() < (1 << k) + k - 1)
//        {
//            return false;
//        }
//        unordered_set<string> exists;
//        for (int i = 0; i + k <= s.size(); i++)
//        {
//            exists.insert(move(s.substr(i, k)));
//        }
//        return exists.size() == (1 << k);
//    }
//};

//class Solution {
//public:
//    int numWaterBottles(int numBottles, int numExchange) {
//        int bottle = numBottles, ret = numBottles;
//        while (bottle >= numExchange)
//        {
//            bottle -= numExchange;
//            ret++;
//            bottle++;
//        }
//        return ret;
//    }
//};

/**
 * struct ListNode {
 *  int val;
 *  struct ListNode *next;
 *  ListNode(int x) : val(x), next(nullptr) {}
 * };
 */
//class Solution {
//public:
//    ListNode* FindKthToTail(ListNode* pHead, int k) {
//        // write code here
//        ListNode* fast = pHead;
//        ListNode* slow = pHead;
//        while (k-- && fast)
//        {
//            fast = fast->next;
//        }
//        if (k >= 0)
//        {
//            return nullptr;
//        }
//        while (fast != nullptr)
//        {
//            slow = slow->next;
//            fast = fast->next;
//        }
//        return slow;
//    }
//};

//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* newHead = new ListNode(-1);
//        ListNode* cur1 = l1;
//        ListNode* cur2 = l2;
//        ListNode* prev = newHead;
//        int t = 0;//判断进位
//        while (cur1 || cur2 || t)
//        {
//            if (cur1)
//            {
//                t += cur1->val;
//                cur1 = cur1->next;
//            }
//            if (cur2)
//            {
//                t += cur2->val;
//                cur2 = cur2->next;
//            }
//            prev->next = new ListNode(t % 10);
//            prev = prev->next;
//            t /= 10;
//        }
//        prev = newHead->next;
//        delete newHead;
//        return prev;
//    }
//};

//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//
//        ListNode* newHead = new ListNode(-1);
//        newHead->next = head;
//        ListNode* prev = newHead, * cur = prev->next, * next = cur->next, * nnext = next->next;
//        while (cur && next)
//        {
//            //交换数据
//            prev->next = next;
//            next->next = cur;
//            cur->next = nnext;
//            //修改指针
//            prev = cur;
//            cur = nnext;
//            if (cur) next = cur->next;
//            if (next) nnext = next->next;
//        }
//        cur = newHead->next;
//        delete newHead;
//        return cur;
//    }
//};

//class Solution {
//public:
//    ListNode* rotateRight(ListNode* head, int k) {
//        if (head == nullptr || head->next == nullptr || k == 0) return head;
//        ListNode* cur = head;
//        int count = 0;
//        while (cur)
//        {
//            cur = cur->next;
//            count++;
//        }
//        k = k % count;
//        while (k--)
//        {
//            ListNode* pre_tail = head;
//            ListNode* tail = head->next;
//            while (tail->next != nullptr)
//            {
//                tail = tail->next;
//                pre_tail = pre_tail->next;
//            }
//            pre_tail->next = nullptr;
//            tail->next = head;
//            head = tail;
//        }
//        return head;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> generateMatrix(int n) {
//        int maxNum = n * n;
//        int curNum = 1;
//        vector<vector<int>> matrix(n, vector<int>(n));
//        int row = 0, column = 0;
//        vector<vector<int>> directions = { {0, 1}, {1, 0}, {0, -1}, {-1, 0} };  // 右下左上
//        int directionIndex = 0;
//        while (curNum <= maxNum) {
//            matrix[row][column] = curNum;
//            curNum++;
//            int nextRow = row + directions[directionIndex][0], nextColumn = column + directions[directionIndex][1];
//            if (nextRow < 0 || nextRow >= n || nextColumn < 0 || nextColumn >= n || matrix[nextRow][nextColumn] != 0) {
//                directionIndex = (directionIndex + 1) % 4;  // 顺时针旋转至下一个方向
//            }
//            row = row + directions[directionIndex][0];
//            column = column + directions[directionIndex][1];
//        }
//        return matrix;
//    }
//};

