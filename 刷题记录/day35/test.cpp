#include <iostream>
#include <vector>

using namespace std;

//class Solution {
//public:
//    void nextPermutation(vector<int>& nums) {
//        int i = nums.size() - 2;
//        while (i >= 0 && nums[i] >= nums[i + 1])
//        {
//            i--;
//        }
//
//        if (i >= 0)
//        {
//            int j = nums.size() - 1;
//            while (j >= 0 && nums[i] >= nums[j])
//            {
//                j--;
//            }
//            swap(nums[i], nums[j]);
//        }
//        reverse(nums.begin() + i + 1, nums.end());
//    }
//};

class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        vector<int> tmp(m + n);
        int p1 = 0, p2 = 0;
        int cur = 0;
        while (p1 < m || p2 < n)
        {
            if (p1 == m)
            {
                tmp[cur++] = nums2[p2++];
            }
            else if (p2 == n)
            {
                tmp[cur++] = nums1[p1++];
            }
            else if (nums1[p1] > nums2[p2])
            {
                tmp[cur++] = nums2[p2++];
            }
            else
            {
                tmp[cur++] = nums1[p1++];
            }
        }
        for (int i = 0; i != m + n; ++i) {
            nums1[i] = tmp[i];
        }
    }
};