#include <iostream>
#include <vector>

using namespace std;
//
//class Solution {
//public:
//    vector<int> plusOne(vector<int>& digits) {
//        int n = digits.size();
//        for (int i = n - 1; i >= 0; --i) {
//            if (digits[i] != 9) {
//                ++digits[i];
//                for (int j = i + 1; j < n; ++j) {
//                    digits[j] = 0;
//                }
//                return digits;
//            }
//        }
//
//        // digits 中所有的元素均为 9
//        vector<int> ans(n + 1);
//        ans[0] = 1;
//        return ans;
//    }
//};
//
//int main()
//{
//    vector<int> v;
//    v.push_back(3);
//    v.push_back(2);
//    v.push_back(9);
//
//    vector<int> tmp = Solution().plusOne(v);
//    return 0;
//}

//class Solution {
//public:
//    bool isMonotonic(vector<int>& nums) {
//        bool flag1 = true, flag2 = true;
//        int n = nums.size();
//        for (int i = 0; i < n - 1; i++)
//        {
//            if (nums[i] > nums[i + 1])
//                flag1 = false;
//            if (nums[i] < nums[i + 1])
//                flag2 = false;
//        }
//        return flag1 || flag2;
//    }
//};

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    void inorder(TreeNode* node, vector<int>& res)
    {
        if (node == nullptr)
            return;
        inorder(node->left, res);
        res.push_back(node->val);
        inorder(node->right, res);
    }
    TreeNode* increasingBST(TreeNode* root) {
        vector<int> res;
        inorder(root, res);

        TreeNode* dummyNode = new TreeNode(-1);
        TreeNode* cur = dummyNode;
        for (auto v : res)
        {
            cur->right = new TreeNode(v);
            cur = cur->right;
        }

        return dummyNode->right;
    }
};