#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
#include <string>
#include <stack>
#include <cmath>
#include <algorithm>
using namespace std;

// 进制转换
// string Conversion(int x, int y)
//{
//	if (x == 0)
//	{
//		return "0";
//	}
//
//	bool flag = false;
//	if (x < 0)
//	{
//		x = -x;
//		flag = true;
//	}
//
//	string s;
//	char bf[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
//
//	while (x)
//	{
//		s.insert(s.begin(), bf[x % y]);
//		x /= y;
//	}
//
//	if (flag)
//	{
//		s.insert(s.begin(), '-');
//	}
//	return s;
// }
//
// int main()
//{
//	int a, b;
//	cin >> a >> b;
//	cout << Conversion(a, b) << endl;
//	return 0;
// }

// 连续最大和
// int getMax(int x, int y)
//{
//	return x > y ? x : y;
// }
//
// int main()
//{
//	int n;
//	cin >> n;
//	vector<int> v(n);
//	for (int i = 0; i < n; i++)
//	{
//		cin >> v[i];
//	}
//	int sum = v[0];
//	int max = v[0];
//	for (int i = 1; i < n; i++)
//	{
//		sum = getMax(sum + v[i], v[i]);
//		if (sum > max)
//		{
//			max = sum;
//		}
//	}
//	cout << max << endl;
//	return 0;
// }

// 统计回文
// bool IsCircleText(string s)
//{
//	string ss = s;
//	reverse(s.begin(), s.end());
//	if (ss == s)
//	{
//		return true;
//	}
//
//	return false;
// }

// bool IsCircleText(string& s)
//{
//	int begin = 0;
//	int end = s.size() - 1;
//	while (begin < end)
//	{
//		if (s[begin] != s[end])
//		{
//			return false;
//		}
//		begin++;
//		end--;
//	}
//	return true;
// }
//
// int main()
//{
//	string str1, str2;
//	getline(cin, str1);
//	getline(cin, str2);
//	int count = 0;
//	for (int i = 0; i <= str1.size(); ++i)
//	{
//		string str = str1;
//		str.insert(i, str2);
//		if (IsCircleText(str))
//		{
//			count++;
//		}
//	}
//	cout << count << endl;
//	return 0;
// }

//合法括号序列判断
// 1.计数
//bool chkParenthesis(string A, int n)
//{
//    // write code here
//    if ((n & 1) == 1)
//    {
//        return false;
//    }
//    int count1 = 0;
//    int count2 = 0;
//    for (int i = 0; i < n; i++)
//    {
//        if (A[i] == '(')
//        {
//            count1++;
//        }
//        if (A[i] == ')')
//        {
//            count2++;
//        }
//    }
//    if (count1 != count2)
//    {
//        cout << "false" << endl;
//        return false;
//    }
//    else
//    {
//        cout << "true" << endl;
//        return true;
//    }
//}
//2.栈
//bool chkParenthesis(string A, int n) {
//        // write code here
//        stack<char> st;
//        for(auto e : A)
//        {
//            switch(e)
//            {
//                case '(':
//                st.push('(');
//                break;
//                case ')':
//                if(st.empty() || st.top() != '(')
//                {
//                    return false;
//                }
//                else {
//                st.pop();
//                }
//                break;
//                default:
//                return false; 
//            }
//        }
//        return true;
//    }

//int main()
//{
//    cout << chkParenthesis("((())))", 6) << endl;
//    return 0;
//}

//Fibonacci数列
//int main() 
//{
//    int n, f, f1 = 0, f2 = 1;
//    cin >> n;
//    while (true)
//    {
//        f = f1 + f2;
//        f1 = f2;
//        f2 = f;
//        if (f2 >= n && f1 <= n)
//        {
//            cout << ((f2 - n) > (n - f1) ? (n - f1) : (f2 - n)) << endl;
//            break;
//        }
//    }
//    return 0;
//}
//int main() 
//{
//    int n, f, l = 0, r = 0, f0 = 0, f1 = 1;
//    cin >> n;
//    while (true)
//    {
//        f = f0 + f1;
//        f0 = f1;
//        f1 = f;
//
//        if (f < n)
//            l = n - f;
//        else
//        {
//            r = f - n;
//            break;
//        }
//    }
//    cout << min(l, r) << endl;
//    return 0;
//}

//不要二

//
//bool Dictionaries(const vector<string>& v)
//{
//    int i = 1;
//    while (v[i - 1] <= v[i] && i < v.size())
//    {
//        i++;
//    }
//    if (i == v.size() - 1)
//        return true;
//    else
//        return false;
//}
//
//bool Size(const vector<string>& v)
//{
//    int i = 1;
//    while (v[i - 1].size() <= v[i].size() && i < v.size())
//    {
//        i++;
//    }
//    if (i == v.size() - 1)
//        return true;
//    else
//        return false;
//}
//
//int main() {
//    int n;
//    string s;
//    vector<string> v;
//    cin >> n;
//    v.resize(n + 1);
//    for (int i = 0; i < n; i++) {
//        cin >> s;
//        v[i] = s;
//    }
//    if (Size(v) && Dictionaries(v))
//        cout << "both" << endl;
//    else if (Size(v) && (!Dictionaries(v)))
//        cout << "lengths" << endl;
//    else if ((!Size(v)) && Dictionaries(v))
//        cout << "lexicographically" << endl;
//    else
//        cout << "none" << endl;
//
//    return 0;
//}

//动态规划
//int n, m, dp[5][5];
//int main()
//{
//    while (~scanf("%d%d", &n, &m))
//    {
//        dp[0][0] = 0;
//        for (int i = 1; i <= n; i++)
//            dp[i][0] = 1;
//        for (int j = 1; j <= m; j++)
//            dp[0][j] = 1;//初始化
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= m; j++)
//            {
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];//动态规划转移方程
//            }
//        printf("%d\n", dp[n][m]);
//    }
//    return 0;
//}

//二进制插入
//int binInsert(int n, int m, int j, int i) 
//{
//	m <<= j;
//	return n | m;
//}
// 
//int getBin(int x, vector<int>& v)
//{
//    int temp = 0;
//    int count = 0;
//    while (x)
//    {
//        count++;
//        temp = x % 2;
//        v.push_back(temp);
//        x /= 2;
//    }
//    return count;
//}
//int getDec(vector<int>& v)
//{
//    int sum = 0;
//    for (int i = 0; i < v.size(); i++)
//    {
//        sum += (v[i] * pow(2, i));
//    }
//    return sum;
//}
//int binInsert(int n, int m, int j, int i) {
//    vector<int> vn, vm;
//    int sum = 0;
//
//    getBin(n, vn);
//    getBin(m, vm);
//    int p = 0;
//    for (int z = j; z <= i; z++)
//    {
//        vn[z] = vm[p++];
//        if (p > vm.size())
//        {
//            break;
//        }
//    }
//    return getDec(vn);
//}
//
//int main()
//{
//    cout << binInsert(1024, 19, 2, 6) << endl;
//    return 0;
//}

//查找组成一个偶数最接近的两个素数
//bool isPrime(int n)
//{
//	int temp = sqrt(n);
//	for (int i = 2; i <= temp; i++)
//	{
//		if (n % i == 0)
//		{
//			return false;
//		}
//	}
//	return true;
//}
//
//void Analyse(int n)
//{
//	int ret1 = n / 2;
//	int ret2 = n - ret1;
//	while (true)
//	{
//		if (isPrime(ret1) && isPrime(ret2))
//		{
//			break;
//		}
//		ret1--;
//		ret2++;
//	}
//	cout << ret1 << endl;
//	cout << ret2 << endl;
//}
//
//int main()
//{
//	int num;
//	while(cin >> num)
//		Analyse(num);
//	return 0;
//}

//幸运袋子


//手套

//查找两个字符串中的最长子串
//int main()
//{
//    string str1, str2, str3;
//    cin >> str1 >> str2;
//    if (str1.size() > str2.size())
//        swap(str1, str2);
//    for (int i = 0; i < str1.size(); i++)
//    {
//        for (int j = i; j < str1.size(); j++)
//        {
//            string temp = str1.substr(i, j - i + 1);
//            if (int(str2.find(temp)) < 0)
//            {
//                break;
//            }
//            else
//            {
//                if (str3.size() < temp.size())
//                    str3 = temp;
//            }
//        }
//    }
//    cout << str3 << endl;
//    return 0;
//}

//洗牌
//void shuffleK(vector<int>& v, int k, int n)
//{
//    vector<int> temp;
//    temp.resize(2 * n);
//    while (k--)
//    {
//        int left = n - 1;
//        int right = 2 * n - 1;
//        int i = 2 * n - 1;
//        while (i > 0)
//        {
//            temp[i--] = v[right--];
//
//            temp[i--] = v[left--];
//        }
//        swap(temp, v);
//    }
//
//}
//
//int main()
//{
//    int t, n, k, num;
//    cin >> t;
//    while (t--)
//    {
//        cin >> n >> k;
//        vector<int> v;
//        v.resize(2 * n);
//        for (int i = 0; i < 2 * n; i++)
//        {
//            cin >> num;
//            v[i] = num;
//        }
//        shuffleK(v, k, n);
//        for (auto e : v)
//        {
//            cout << e << " ";
//        }
//        cout << endl;
//    }
//    return 0;
//}


//int main()
//{
//    string s;
//    int count = 0;
//    int n = 1, t = 1, d = 0;
//    cin >> count;
//    cin >> s;
//    
//        if (count <= 4)
//        {
//            t = 1;
//            d = n;
//            for (int i = 0; i < s.size(); i++)
//            {
//                if (s[i] == 'U')
//                {
//                    if (n == 1)
//                    {
//                        n = d;
//                    }
//                    else
//                    {
//                        n--;
//                    }
//                }
//                if (s[i] == 'D')
//                {
//                    if (n == count)
//                    {
//                        n = t;
//                    }
//                    else
//                    {
//                        n++;
//                    }
//                }
//            }
//        }
//        else
//        {
//            t = 1;
//            d = 4;
//            for (int i = 0; i < s.size(); i++)
//            {
//                if (s[i] == 'U')
//                {
//                    if (n == 1)
//                    {
//                        n = count;
//                        t = count - 3;
//                        d = count;
//                    }
//                    else if (n == t)
//                    {
//                        t--;
//                        n--;
//                        d--;
//                    }
//                    else
//                    {
//                        n--;
//                    }
//                }
//                if (s[i] == 'D')
//                {
//                    if (n == count)
//                    {
//                        n = 1;
//                        t = 1;
//                        d = 4;
//                    }
//                    else if (n == d)
//                    {
//                        d++;
//                        n++;
//                        t++;
//                    }
//                    else
//                    {
//                        n++;
//                    }
//                }
//            }
//        }
//    for (int i = t; i <= d; i++)
//    {
//        cout << i << " ";
//    }
//    cout << endl;
//    cout << n << endl;
//    return 0;
//}

//int treeroot(int n)
//{
//   int temp = 0;
//    while (n)
//    {
//        temp += (n % 10);
//        n /= 10;
//        if (n == 0 && temp / 10 != 0)
//        {
//            n = temp;
//            temp = 0;
//        }
//    }
//    return temp;
//}
//
//int main()
//{
//    int n = 0;
//    while (cin >> n)
//    {
//        cout << treeroot(n) << endl;
//    }
//    return 0;
//}

#include <queue>
using namespace std;

struct list_node {
    int val;
    struct list_node* next;
};

list_node* input_list(void)
{
    int n, val;
    list_node* phead = new list_node();
    list_node* cur_pnode = phead;
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) {
        scanf("%d", &val);
        if (i == 1) {
            cur_pnode->val = val;
            cur_pnode->next = NULL;
        }
        else {
            list_node* new_pnode = new list_node();
            new_pnode->val = val;
            new_pnode->next = NULL;
            cur_pnode->next = new_pnode;
            cur_pnode = new_pnode;
        }
    }
    return phead;
}


list_node* reverse_list(list_node* head, int L, int R)
{
    //////在下面完成代码
    queue<list_node**> q;
    int count = 1;
    list_node* cur = head;
    while (cur->next != nullptr)
    {
        if (count >= L && count <= R)
        {
            q.push(&cur);
        }
        cur = cur->next;
        count++;
    }
    cur = head;
    count = 1;
    list_node** temp = nullptr;
    while (cur->next != nullptr)
    {
        temp = &cur;
        if (count >= L && count <= R)
        {
            temp = q.front();
            q.pop();
        }
        cur = cur->next;
        count++;
    }

    return head;
}

void print_list(list_node* head)
{
    while (head != NULL) {
        printf("%d ", head->val);
        head = head->next;
    }
    puts("");
}


int main()
{
    int L, R;
    list_node* head = input_list();
    scanf("%d%d", &L, &R);
    list_node* new_head = reverse_list(head, L, R);
    print_list(new_head);
    return 0;
}