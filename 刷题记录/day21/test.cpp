#include <iostream>
#include <algorithm>
#include <queue>
#include <string>
#include <vector>

using namespace std;

//class Solution {
//    typedef pair<int, int> PII;
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        int cur = image[sr][sc];
//        if (cur == color) return image;
//
//        int m = image.size(), n = image[0].size();
//
//        queue<PII> q;
//        int a, b;
//        q.push({ sr, sc });
//        while (q.size())
//        {
//            int a = q.front().first;
//            int b = q.front().second;
//            q.pop();
//            image[a][b] = color;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == cur)
//                {
//                    q.push({ x, y });
//                }
//            }
//        }
//        return image;
//    }
//};

//class Solution {
//    typedef pair<int, int> PII;
//    int m, n;
//    int vis[51][51];
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int ret;
//public:
//    void bfs(vector<vector<int>>& grid, int i, int j)
//    {
//        queue<PII> q;
//        q.push({ i, j });
//        vis[i][j] = true;
//        int tmp = 0;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            tmp++;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1 && !vis[x][y])
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                }
//            }
//            ret = max(ret, tmp);
//        }
//    }
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                {
//                    bfs(grid, i, j);
//                }
//            }
//
//        }
//        return ret;
//    }
//};
//
//int main()
//{
//	return 0;
//}

//class Solution {
//    int m, n;
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    void bfs(vector<vector<char>>& board, int i, int j)
//    {
//        queue<pair<int, int>> q;
//        q.push({ i, j });
//        board[i][j] = '.';
//
//        while (q.size())
//        {
//            int a = q.front().first;
//            int b = q.front().second;
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O')
//                {
//                    q.push({ x, y });
//                    board[x][y] = '.';
//                }
//            }
//        }
//    }
//    void solve(vector<vector<char>>& board) {
//        m = board.size(), n = board[0].size();
//        for (int j = 0; j < n; j++)
//        {
//            if (board[0][j] == 'O')
//                bfs(board, 0, j);
//            if (board[m - 1][j] == 'O')
//                bfs(board, m - 1, j);
//        }
//
//        for (int i = 0; i < m; i++)
//        {
//            if (board[i][0] == 'O')
//                bfs(board, i, 0);
//            if (board[i][n - 1] == 'O')
//                bfs(board, i, n - 1);
//        }
//
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (board[i][j] == 'O')
//                {
//                    board[i][j] = 'X';
//                }
//                else if (board[i][j] = '.')
//                {
//                    board[i][j] = 'O';
//                }
//            }
//        }
//    }
//};
//
//int main()
//{
//    vector<char> b1;
//    b1.push_back('X');
//    b1.push_back('X');
//    b1.push_back('X');
//    b1.push_back('X');
//    vector<char> b2;
//    b2.push_back('X');
//    b2.push_back('O');
//    b2.push_back('O');
//    b2.push_back('X');
//    vector<char> b3;
//    b3.push_back('X');
//    b3.push_back('X');
//    b3.push_back('O');
//    b3.push_back('X');
//    vector<char> b4;
//    b4.push_back('X');
//    b4.push_back('O');
//    b4.push_back('X');
//    b4.push_back('X');
//
//    vector<vector<char>> board;
//    board.push_back(b1);
//    board.push_back(b2);
//    board.push_back(b3);
//    board.push_back(b4);
//
//    Solution().solve(board);
//    return 0;
//}

//class Solution {
//    int m, n;
//    bool vis[101][101];
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//public:
//    int nearestExit(vector<vector<char>>& maze, vector<int>& entrance) {
//        m = maze.size(), n = maze[0].size();
//        queue<pair<int, int>> q;
//        q.push({ entrance[0], entrance[1] });
//        vis[entrance[0]][entrance[1]] = true;
//        int step = 0;
//
//        while (q.size())
//        {
//            step++;
//            int sz = q.size();
//            for (int i = 0; i < sz; i++)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//
//                for (int j = 0; j < 4; j++)
//                {
//                    int x = a + dx[j], y = b + dy[j];
//                    if (x >= 0 && x < m && y >= 0 && y < n && maze[x][y] == '.' && !vis[x][y])
//                    {
//                        if (x == 0 || x == m - 1 || y == 0 || y == n - 1) return step;
//                        q.push({ x, y });
//                        vis[x][y] = true;
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//};