#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>

using namespace std;

//class Solution {
//public:
//    string modifyString(string s) {
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] == '?')
//            {
//                for (char ch = 'a'; ch <= 'z'; ch++)
//                {
//                    if ((i == 0 || s[i - 1] != ch) && (i == n - 1 || s[i + 1] != ch))
//                    {
//                        s[i] = ch;
//                        break;
//                    }
//                }
//            }
//        }
//        return s;
//    }
//};
//
//
//const int N = 100010;
//long long arr[N], dp[N];
//int n, q;
//
//int main() {
//    cin >> n >> q;
//    for (int i = 1; i <= n; i++) cin >> arr[i];
//    for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + arr[i];
//    while (q--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << dp[r] - dp[l - 1] << endl;
//    }
//    return 0;
//}

//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
//        if (timeSeries.size() == 1) return duration;
//        int ret = duration;
//        for (int i = 1; i < timeSeries.size(); i++)
//        {
//            if (timeSeries[i] - timeSeries[i - 1] >= duration) ret += duration;
//            else ret += timeSeries[i] - timeSeries[i - 1];
//        }
//        return ret;
//    }
//};

//int main()
//{
//	int n;
//	double avg = 0, score = 0, max = 0, min = 100;
//	scanf_s("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		scanf_s("%lf", &score);
//		if (max < score)
//		{
//			max = score;
//		}
//		if (min > score)
//		{
//			min = score;
//		}
//		avg += score;
//	}
//	avg = (avg - max - min) / (n - 2);
//	printf("%.2lf", avg);
//	return 0;
//}
//
//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto num : nums)
//        {
//            ret ^= num;
//        }
//        for (int i = 0; i <= nums.size(); i++) ret ^= i;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //高斯求和
//        int n = nums.size();
//        int ret = n * (n + 1) / 2;
//        int sum = 0;
//        for (auto num : nums)
//        {
//            sum += num;
//        }
//        return ret - sum;
//    }
//};

//class Solution {
//public:
//    bool hasAllCodes(string s, int k) {
//        if (s.size() < (1 << k) + k - 1)
//        {
//            return false;
//        }
//        unordered_set<string> exists;
//        for (int i = 0; i + k <= s.size(); i++)
//        {
//            exists.insert(move(s.substr(i, k)));
//        }
//        return exists.size() == (1 << k);
//    }
//};

//class Solution {
//public:
//    int numWaterBottles(int numBottles, int numExchange) {
//        int bottle = numBottles, ret = numBottles;
//        while (bottle >= numExchange)
//        {
//            bottle -= numExchange;
//            ret++;
//            bottle++;
//        }
//        return ret;
//    }
//};

/**
 * struct ListNode {
 *  int val;
 *  struct ListNode *next;
 *  ListNode(int x) : val(x), next(nullptr) {}
 * };
 */
//class Solution {
//public:
//    ListNode* FindKthToTail(ListNode* pHead, int k) {
//        // write code here
//        ListNode* fast = pHead;
//        ListNode* slow = pHead;
//        while (k-- && fast)
//        {
//            fast = fast->next;
//        }
//        if (k >= 0)
//        {
//            return nullptr;
//        }
//        while (fast != nullptr)
//        {
//            slow = slow->next;
//            fast = fast->next;
//        }
//        return slow;
//    }
//};

//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* newHead = new ListNode(-1);
//        ListNode* cur1 = l1;
//        ListNode* cur2 = l2;
//        ListNode* prev = newHead;
//        int t = 0;//判断进位
//        while (cur1 || cur2 || t)
//        {
//            if (cur1)
//            {
//                t += cur1->val;
//                cur1 = cur1->next;
//            }
//            if (cur2)
//            {
//                t += cur2->val;
//                cur2 = cur2->next;
//            }
//            prev->next = new ListNode(t % 10);
//            prev = prev->next;
//            t /= 10;
//        }
//        prev = newHead->next;
//        delete newHead;
//        return prev;
//    }
//};

//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//
//        ListNode* newHead = new ListNode(-1);
//        newHead->next = head;
//        ListNode* prev = newHead, * cur = prev->next, * next = cur->next, * nnext = next->next;
//        while (cur && next)
//        {
//            //交换数据
//            prev->next = next;
//            next->next = cur;
//            cur->next = nnext;
//            //修改指针
//            prev = cur;
//            cur = nnext;
//            if (cur) next = cur->next;
//            if (next) nnext = next->next;
//        }
//        cur = newHead->next;
//        delete newHead;
//        return cur;
//    }
//};

//class Solution {
//public:
//    ListNode* rotateRight(ListNode* head, int k) {
//        if (head == nullptr || head->next == nullptr || k == 0) return head;
//        ListNode* cur = head;
//        int count = 0;
//        while (cur)
//        {
//            cur = cur->next;
//            count++;
//        }
//        k = k % count;
//        while (k--)
//        {
//            ListNode* pre_tail = head;
//            ListNode* tail = head->next;
//            while (tail->next != nullptr)
//            {
//                tail = tail->next;
//                pre_tail = pre_tail->next;
//            }
//            pre_tail->next = nullptr;
//            tail->next = head;
//            head = tail;
//        }
//        return head;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> generateMatrix(int n) {
//        int maxNum = n * n;
//        int curNum = 1;
//        vector<vector<int>> matrix(n, vector<int>(n));
//        int row = 0, column = 0;
//        vector<vector<int>> directions = { {0, 1}, {1, 0}, {0, -1}, {-1, 0} };  // 右下左上
//        int directionIndex = 0;
//        while (curNum <= maxNum) {
//            matrix[row][column] = curNum;
//            curNum++;
//            int nextRow = row + directions[directionIndex][0], nextColumn = column + directions[directionIndex][1];
//            if (nextRow < 0 || nextRow >= n || nextColumn < 0 || nextColumn >= n || matrix[nextRow][nextColumn] != 0) {
//                directionIndex = (directionIndex + 1) % 4;  // 顺时针旋转至下一个方向
//            }
//            row = row + directions[directionIndex][0];
//            column = column + directions[directionIndex][1];
//        }
//        return matrix;
//    }
//};

//class Solution {
//public:
//    void reorderList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr || head->next->next == nullptr) return;
//        ListNode* fast = head, * slow = head;
//
//        //找到中间节点——快慢指针
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        ListNode* head2 = new ListNode(-1);
//        head2->next = nullptr;
//        ListNode* cur = slow->next;
//        slow->next = nullptr;
//
//        //逆序——头插法
//        while (cur)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = head2->next;
//            head2->next = cur;
//            cur = tmp;
//        }
//
//        //合并两个链表——双指针
//        ListNode* head3 = new ListNode(-1);
//        ListNode* cur3 = head3;
//        ListNode* cur1 = head;
//        ListNode* cur2 = head2->next;
//        while (cur1)
//        {
//            cur3->next = cur1;
//            cur1 = cur1->next;
//            cur3 = cur3->next;
//
//            if (cur2)
//            {
//                cur3->next = cur2;
//                cur2 = cur2->next;
//                cur3 = cur3->next;
//            }
//        }
//        head = head3->next;
//        delete head2;
//        delete head3;
//
//    }
//};

//#include <string.h>
//
//int main()
//{
//	for(int number = 0; number <= 150; number++)
//	{
//		cout << number << ":" << strerror(number) << endl;
//	}
//	return 0;
//}

//class Solution {
//public:
//    vector<string> findLongestSubarray(vector<string>& array) {
//        int n = array.size();
//        vector<int> shift(n);
//        int left = 0, right = -1;
//        unordered_map<int, int> hash;
//        for (int i = 0; i < n; i++)
//        {
//            if (array[i] >= "A" && array[i] <= "z") shift[i] = -1;
//            else shift[i] = 1;
//        }
//        for (int i = 1; i < n; i++)
//        {
//            shift[i] += shift[i - 1];
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            auto it = hash.find(shift[i]);
//            if (shift[i] == 0)
//            {
//                if (right - left + 1 < i + 1)
//                {
//                    right = i;
//                    left = 0;
//                }
//                continue;
//            }
//            if (it == hash.end()) hash[shift[i]] = i;
//            else
//            {
//                if (right - left + 1 < i - it->second)
//                {
//                    right = i;
//                    left = it->second + 1;
//                }
//            }
//        }
//        vector<string> ret;
//        for (int i = left; i <= right; i++) ret.push_back(array[i]);
//        return ret;
//    }
//};
//
//int main()
//{
//    vector<string> array;
//    array.push_back("A");
//    array.push_back("1");
//    array.push_back("B");
//    array.push_back("C");
//    array.push_back("D");
//    array.push_back("2");
//    array.push_back("3");
//    array.push_back("4");
//    array.push_back("E");
//    array.push_back("5");
//    array.push_back("F");
//    array.push_back("G");
//
//    Solution().findLongestSubarray(array);
//    return 0;
//}

//#include<iostream>
//#include<cmath>
//using namespace std;
//
//int main()
//{
//    double h = 0;
//    //如果n也定义为整型的话，由于o和n都是整数，这将导致整数除法，结果将丢失小数部分。
//    double n = 23333333;
//    int o = 0;
//    for (o = 0; o < n / 2; o++)
//    {
//        h = -o * (o / n) * log2(o / n) - (n - o) * ((n - o) / n) * log2((n - o) / n);
//        if (h > 11625907.5 && h < 11625907.6)
//        {
//            cout << o << endl;
//            break;
//        }
//    }
//    return 0;
//}

//#include <iostream>
//#include <vector>
//#include <climits>
//#include <algorithm>
//using namespace std;
//int findMin(vector<int>& tmp)
//{
//    int mini = 0, min_num = INT_MAX;
//    for (int i = 0; i < tmp.size(); i++)
//    {
//        if (tmp[i] < min_num)
//        {
//            mini = i;
//        }
//    }
//    return mini;
//}
//int main()
//{
//    // 请在此输入您的代码
//    int n, k;
//    cin >> n >> k;
//    vector<int> ret(n);
//    for (int i = 0; i < n; i++)
//    {
//        cin >> ret[i];
//    }
//    for (int i = 0; i < k; i++)
//    {
//        int deli = findMin(ret);
//        if (deli == 0)
//        {
//            ret[deli + 1] += ret[deli];
//        }
//        else if (deli == ret.size() - 1)
//        {
//            ret[deli - 1] += ret[deli];
//        }
//        else {
//            ret[deli - 1] += ret[deli];
//            ret[deli + 1] += ret[deli];
//        }
//        auto pos = find(ret.begin(), ret.end(), ret[deli]);
//        ret.erase(pos);
//    }
//    for (auto num : ret)
//    {
//        cout << num << " ";
//    }
//    cout << endl;
//    return 0;
//}

//#include <iostream>
//#include <string>
//using namespace std;
//
//int dp[10];
//
//int main()
//{
//    int n;
//    cin >> n;
//    string s;
//    int m = 0;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> s;
//        int x = s[0] - '0', y = s[s.size() - 1] - '0';
//        dp[y] = max(dp[x] + 1, dp[y]);
//        m = max(m, dp[y]);
//    }
//    cout << n - m << endl;
//    return 0;
//}

//class Solution {
//public:
//    int minNumberOfFrogs(string croakOfFrogs) {
//        string t = "crock";
//        int n = t.size();
//        vector<int> hash(n);
//        unordered_map<char, int> index;
//
//        for (int i = 0; i < n; i++)
//        {
//            index[t[i]] = i;
//        }
//
//        for (auto ch : croakOfFrogs)
//        {
//            if (ch == 'c')
//            {
//                if (hash[n - 1] != 0) hash[n - 1]--;
//                hash[0]++;
//            }
//            else
//            {
//                int i = index[ch];
//                if (hash[i - 1] == 0) return -1;
//                hash[i - 1]--;
//                hash[i]++;
//            }
//        }
//        for (int i = 0; i < n - 1; i++)
//        {
//            if (hash[i] != 0)
//            {
//                return -1;
//            }
//        }
//        return hash[n - 1];
//    }
//};
//
//int main()
//{
//    Solution().minNumberOfFrogs("crcoakroak");
//    return 0;
//}

class Solution {
public:
    vector<int> sortArray(vector<int>& nums) {
        srand(time(nullptr));
        qsort(nums, 0, nums.size() - 1);
        return nums;
    }
    void qsort(vector<int>& nums, int l, int r)
    {
        if (l >= r) return;

        int key = getRandom(nums, l, r);
        //分三块区域
        int i = l, left = l - 1, right = r + 1;
        while (i < right)
        {
            if (nums[i] < key) swap(nums[++left], nums[i++]);
            else if (nums[i] == key) i++;
            else swap(nums[--right], nums[i]);
        }
        qsort(nums, l, left);
        qsort(nums, right, r);
    }

    int getRandom(vector<int>& nums, int left, int right)
    {
        int r = rand();
        return nums[r % (right - left + 1) + left];
    }
};