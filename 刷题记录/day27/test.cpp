#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

//组合总和—解法一
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        t = target;
//        dfs(candidates, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > t || pos == candidates.size())
//        {
//            return;
//        }
//
//        for (int i = pos; i < candidates.size(); i++)
//        {
//            path.push_back(candidates[i]);
//            dfs(candidates, i, sum + candidates[i]);
//            path.pop_back();
//        }
//    }
//};

//解法二
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        t = target;
//        dfs(candidates, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (sum > t || pos == candidates.size())
//        {
//            return;
//        }
//
//        for (int k = 0; k * candidates[pos] + sum <= t; k++)
//        {
//            if (k) path.push_back(candidates[pos]);
//            dfs(candidates, pos + 1, sum + k * candidates[pos]);
//        }
//
//        for (int k = 1; k * candidates[pos] + sum <= t; k++)
//        {
//            path.pop_back();
//        }
//    }
//};

//组合
//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int n, k;
//public:
//    vector<vector<int>> combine(int _n, int _k) {
//        n = _n;
//        k = _k;
//        dfs(1);
//        return ret;
//    }
//
//    void dfs(int start)
//    {
//        if (path.size() == k)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = start; i <= n; i++)
//        {
//            path.push_back(i);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//};


//class Solution {
//    vector<vector<int>> ret;
//    vector<int> path;
//    int t;
//public:
//    int combinationSum4(vector<int>& nums, int target) {
//        sort(nums.begin(), nums.end());
//        t = target;
//        dfs(nums, 0, 0);
//        return ret.size();
//    }
//
//    void dfs(vector<int>& nums, int pos, int sum)
//    {
//        if (sum == t)
//        {
//            ret.push_back(path);
//            for (auto v : path)
//            {
//                cout << v;
//            }
//            return;
//        }
//        if (sum > t || pos > nums.size())
//        {
//            return;
//        }
//
//        for (int i = pos; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            dfs(nums, i + 1, sum + nums[i]);
//            path.pop_back();
//        }
//    }
//};
//
//class Solution {
//public:
//    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
//        if (k == 0) return 0;
//        int ret = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (nums[i] < k) ret++;
//            else continue;
//            int mul = nums[i];
//            for (int j = i + 1; j < nums.size(); j++)
//            {
//                mul *= nums[j];
//                if (mul < k) ret++;
//                else break;
//            }
//        }
//        return ret;
//    }
//};
//
// class Solution {
//public:
//    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
//        if (k == 0) return 0;
//        int ret = 0, j = 0;
//        int mul = 1;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            mul *= nums[i];
//            while (j <= i && mul >= k)
//            {
//                mul /= nums[j];
//                j++;
//            }
//            ret += i - j + 1;
//        }
//        return ret;
//    }
//};
// 
//int main()
//{
//    vector<int> nums = { 1,2,3 };
//    Solution().combinationSum4(nums, 4);
//    return 0;
//}

//N皇后问题
//class Solution {
//    bool checkCol[10], checkDig1[20], checkDig2[20];
//    vector<vector<string>> ret;
//    vector<string> path;
//    int _n;
//public:
//    vector<vector<string>> solveNQueens(int n) {
//        _n = n;
//        path.resize(n);
//        for (int i = 0; i < n; i++)
//        {
//            path[i].append(n, '.');
//        }
//
//        dfs(0);
//        return ret;
//    }
//
//    void dfs(int row)
//    {
//        if (row == _n)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int col = 0; col < _n; col++)
//        {
//            //剪枝
//            if (!checkCol[col] && !checkDig1[row - col + _n] && !checkDig2[row + col])
//            {
//                path[row][col] = 'Q';
//                checkCol[col] = checkDig1[row - col + _n] = checkDig2[row + col] = true;
//                dfs(row + 1);
//                cout << path[row][col] << endl;
//                //恢复现场
//                path[row][col] = '.';
//                checkCol[col] = checkDig1[row - col + _n] = checkDig2[row + col] = false;
//            }
//        }
//    }
//};
//
//int main()
//{
//    Solution().solveNQueens(4);
//    return 0;
//}

//滑动窗口
class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        if (nums.size() == 0) return 0;
        int min_len = INT_MAX, left = 0, right = 0, sum = 0;
        while (right < nums.size())
        {
            sum += nums[right];
            while (sum >= target)
            {
                min_len = min(right - left + 1, min_len);
                sum -= nums[left];
                left++;
            }
            right++;
        }
        return min_len == INT_MAX ? 0 : min_len;
    }
};

//动态规划
class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int n = nums.size();
        if (nums.size() == 0) return 0;
        vector<int> dp(n, 0);
        for (int i = 0; i < n; i++)
        {
            dp[i] = 1;
            for (int j = 0; j < i; j++)
            {
                if (nums[i] > nums[j])
                {
                    dp[i] = max(dp[i], dp[j] + 1);
                }
            }
        }
        int max_len = INT_MIN;
        for (auto num : dp)
        {
            max_len = max(num, max_len);
        }
        return max_len;
    }
};