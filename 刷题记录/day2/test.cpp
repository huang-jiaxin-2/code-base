#include <iostream>
#include <vector>

using namespace std;
//class Solution {
//public:
//    int myAtoi(string s) {
//        int res = 0;
//        int i = 0, n = s.size(), flag = 1, num = 0;
//        while (i < n && s[i] == ' ') i++;
//        if (i < n && s[i] == '-') { flag = -1; i++; }
//
//        else if (i < n && s[i] == '+') i++;
//        for (i; i < n; i++)
//        {
//            if (s[i] >= '0' && s[i] <= '9')
//            {
//                num = s[i] - '0';
//                if (res > (INT_MAX - num) / 10) return INT_MAX;
//                if (res < (INT_MIN + num) / 10) return INT_MIN;
//                res = res * 10 + flag * num;
//            }
//            if (!(s[i] >= '0' && s[i] <= '9'))
//            {
//                break;
//            }
//        }
//        return res;
//    }
//};
//
//int main()
//{
//    int res = Solution().myAtoi("+-42");
//    cout << res << endl;
//    return 0;
//}

class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0 || (x % 10 == 0 && x != 0)) return false;

        int res = 0;
        while (x > res)
        {

            res = res * 10 + (x % 10);
            x /= 10;
        }
        return res == x || x == (res / 10);
    }
};

int main()
{
    Solution().isPalindrome(121);
    return 0;
}