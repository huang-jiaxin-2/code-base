#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

class A
{
public:
	virtual void f() {}
public:
	int _a = 1;
};
class B : public A
{
public:
	int _b = 0;
};

//A*指针pa有可能指向父类，有可能指向子类
void fun(A* pa)
{
	//如果pa是指向子类，那么可以转换，转换表达式返回正确的地址
	//如果pa是指向父类，那么不能转换，转换表达式返回nullptr
	B* pb = dynamic_cast<B*>(pa);
	if (pb)
	{
		cout << "转换成功" << endl;
		pb->_a++;
		pb->_b++;
		cout << pb->_a << ":" << pb->_b << endl;
	}
	else
	{
		cout << "转换失败" << endl;
		pa->_a++;
		cout << pa->_a << endl;
	}
}

int main()
{
	//volatile const int a = 2;
	////int* p = const_cast<int*>(&a);
	//int* p = (int*)&a;
	//*p = 3;
	//cout << a << endl;
	//cout << *p << endl;

	A aa;
	fun(&aa);

	return 0;
}