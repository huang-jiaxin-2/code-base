#pragma once

template<size_t N>
class bit_myset
{
public:
	bit_myset()
	{
		//精密一点
		//if (N / 8 == 0)
		//{
		//	_bits.resize(N / 8, 0);
		//}
		//else
		//{
		//	_bits.resize(N / 8 + 1, 0);
		//}

		//粗糙一点
		_bits.resize(N / 8 + 1, 0);
	}

	//将比特位置1
	void set(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;

		_bits[i] |= (1 << j);
	}

	//将比特位置0
	void reset(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;

		_bits[i] &= ~(1 << j);//按位取反
	}

	bool test(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;

		return _bits[i] & (1 << j);
	}

private:
	vector<char> _bits;
};

void test_bit_myset1()
{
	bit_myset<100> bs;
	bs.set(8);
	bs.set(9);
	bs.set(20);

	cout << bs.test(8) << endl;
	cout << bs.test(9) << endl;
	cout << bs.test(20) << endl;

	bs.reset(8);
	bs.reset(9);
	bs.reset(20);

	cout << bs.test(8) << endl;
	cout << bs.test(9) << endl;
	cout << bs.test(20) << endl;
}

void test_bit_myset2()
{
	bit_myset<-1> bs;
}

//两个位图
template<size_t N>
class twobit_myset
{
public:
	void set(size_t x)
	{
		bool inset1 = _bs1.test(x);
		bool inset2 = _bs2.test(x);

		//00 -> 01
		if (inset1 == false && inset2 == false)
		{
			_bs2.set(x);
		}
		//01 -> 10
		else if (inset1 == false && inset2 == true)
		{
			_bs1.set(x);
			_bs2.reset(x);
		}
		//10 -> 11
		else if (inset1 == true && inset2 == false)
		{
			_bs1.set(x);
			_bs2.set(x);
		}
	}

	void print_once_num()
	{
		for (size_t i = 0; i < N; i++)
		{
			if (_bs1.test(i) == false && _bs2.test(i) == true)
			{
				cout << i << endl;
			}
		}
	}

	void print_twice_num()
	{
		
		for (size_t i = 0; i < N; i++)
		{
			if ((_bs1.test(i) == false && _bs2.test(i) == true)
			   ||(_bs1.test(i) == true && _bs2.test(i) == false))
			{
				cout << i << endl;
			}
		}
		
	}
private:
	bit_myset<N> _bs1;
	bit_myset<N> _bs2;
};

void test_bit_myset3()
{
	int a[] = { 3, 5, 2, 3, 4, 4, 4, 4, 12, 77, 65, 4, 44, 99, 33, 33, 33, 6, 5, 34, 12 };

	twobit_myset<100> bs;
	for (auto e : a)
	{
		bs.set(e);
	}

	//bs.print_once_num();
	//cout << endl;
	bs.print_twice_num();
}
