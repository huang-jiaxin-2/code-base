#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <Windows.h>
using namespace std;

mutex mtx;

void func(char ch)
{
	mtx.lock();
	cout << ch << endl;
	mtx.unlock();
	
	Sleep(1);
}

struct ListNode
{
	ListNode(int val)
		:value(val)
		,next(nullptr)
	{}
	int value;
	ListNode* next;
};

ListNode* comm(ListNode* l1, ListNode* l2)
{
	if (l1 == nullptr) return l2;
	if (l2 == nullptr) return l1;

	ListNode* head = new ListNode(0);
	ListNode* cur = head;
	while (l1 != nullptr && l2 != nullptr)
	{
		if (l1->value > l2->value)
		{
			cur->next = l2;
			l2 = l2->next;
		}
		else
		{
			cur->next = l1;
			l1 = l1->next;
		}
		cur = cur->next;
	}
	if (l1 != nullptr)
	{
		cur->next = l1;
	}
	if (l2 != nullptr)
	{
		cur->next = l2;
	}
	return head->next;
}

int main()
{
	ListNode* l1 = new ListNode(1);
	l1->next = new ListNode(2);
	l1->next->next = new ListNode(4);
	ListNode* l2 = new ListNode(1);
	l2->next = new ListNode(3);
	l2->next->next = new ListNode(4);

	ListNode* head = comm(l1, l2);
	ListNode* cur = head;
	while (cur != nullptr)
	{
		cout << cur->value << " ";
		cur = cur->next;
	}
	/*for (int i = 0; i < 10; i++)
	{
		thread t1(func, 'A');
		thread t2(func, 'B');
		thread t3(func, 'C');
	
		t1.join();
		t2.join();
		t3.join();
	}*/
	
	return 0;
}