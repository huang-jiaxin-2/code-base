#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		
//		cout << "Print()" << endl;
//		cout << "year:" << _year << endl;
//		cout << "month:" << _month << endl;
//		cout << "day:" << _day << endl << endl;
//	}
//	void Print() const//取地址及const取地址操作符重载
//	{
//		cout << "Print()const" << endl;
//		cout << "year:" << _year << endl;
//		cout << "month:" << _month << endl;
//		cout << "day:" << _day << endl << endl;
//	}
//private://谁先声明谁先初始化
//	int _year; // 年
//	int _month; // 月
//	int _day; // 日
//};
//void Test()
//{
//	Date d1(2022, 1, 13);
//	d1.Print();
//	const Date d2(2022, 1, 13);
//	d2.Print();//void Date::Print(void)”: 不能将“this”指针从“const Date”转换为"Date&"
//
//}
//class Time
//{
//public:
//	Time(int hour)
//	{
//		_hour = hour;
//	}
//private:
//	int _hour;
//};
//class Date
//{
//public:
//	//如果Time类没有默认构造函数，要初始化_t 对象，必须通过初始化列表
//	Date(int year,int hour)
//		:_t(hour)
//	{
//		_year = year;
//	}
//private:
//	int _year;
//	Time _t;
//};
////自定类型成员，推荐用初始化列表初始化
////初始化列表可以认为是成员变量定义的地方
////必须放在初始化列表：引用成员变量   const成员变量   自定义类型成员(且没有默认构造函数)
////内置类型也推荐使用初始化列表初始化
//int main()
//{
//
//	Date d(2022, 8);
//	return 0;
//}
////////////////////////////////////
//谁先声明谁先初始化
//class A
//{
//public:
//	A(int a) 
//		:_a1(a)
//		, _a2(_a1)
//	{}
//	void Print() {
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2 = 0;
//	int _a1 = 1;
//};
//int main() {
//	A aa(1);
//	aa.Print();//输出1和随机值
//} 

//class Date
//{
//public:
//	 Date(int year)//explicit可以防止隐式类型的装换
//		:_year(year)
//	{
//
//	}
//private:
//	int _year;
//};
//int main()
//{
//	Date d1(2022);//直接调用构造
//	const Date& d2 = 2022;//隐式类型的装换：构造+拷贝构造+优化-》直接调用构造
//
//	//匿名对象--生命周期只有这一行
//	Date(2022);
//	return 0;
//}

//class A
//{
//public:
//	A()
//	{
//		++_scount;
//	}
//	//静态成员函数--没有this指针
//	//只能访问静态成员
//	static int  GetCount()
//	{
//		return _scount;
//	}
//private:
//	//静态成员变量
//	static int _scount;
//};
//
//int A::_scount = 0;
//
//int main()
//{
//	A a1;
//	A a2;
//	/*cout << a1._scount << endl;
//	cout << a2._scount << endl;
//	cout << A::_scount << endl;*///访问公有的
//
//	cout << a1.GetCount() << endl;
//	return 0;
//}
// 

//class StackOnly
//{
//public:
//	static StackOnly CreateObj()
//	{
//		StackOnly so;
//		return so;
//	}
//
//private:
//	StackOnly(int x = 0, int y = 0)
//		:_x(x)
//		, _y(0)
//	{
//	   
//	}
//private:
//	int _x = 0;
//	int _y = 0;
//};
//
//int main()
//{
//	//StackOnly so1; // 栈
//	//static StackOnly so2; // 静态区
//	//StackOnly s;
//	StackOnly so1 = StackOnly::CreateObj();//静态成员函数 被所有类对象所共享，可以通过类名+域操作符进行调用
//
//
//	return 0;
//}

//int main()
//{
//	int* p1 = new int;
//	int* p2 = new int[5]{1,2,4,5,6};//申请5个int的数组
//	int* p3 = new int(5);//申请1个int的对象，初始化为5
//	//const char* p = "wqeer";
//	delete p1;
//	delete[] p2;
//	delete p3;
//	return 0;
//}

class A
{
public:
	A(int a)
		: _a(a)
	{
		cout << "A():" << this << endl;
	}
	~A()
	{
		cout << "~A():" << this << endl;
	}
private:
	int _a;
};

int main()
{
	//1、堆上申请空间  2、调用默认构造函数
	//A* p = new A;
	//如果没有默认构造函数
	//A* p = new A(10);

	//1、调用析构函数清理对象中的资源  2、释放空间
	//delete p;

	cout << endl << endl;
	//new失败不需要检查返回值，它失败是抛异常
	A* p1 = new A[5]{1,2,3,4,6};
	delete[] p1;

	return 0;
}