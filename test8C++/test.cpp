#include <iostream>
#include <string>
using namespace std;

//class People
//{
//public:
//	People()
//	{
//		cout << "People()" << endl;
//	}
//	~People()
//	{
//		cout << "~People()" << endl;
//	}
//};
//
//class student : public People
//{
//public:
//	student()
//	{
//		cout << "student()" << endl;
//	}
//	~student()
//	{
//		cout << "~student()" << endl;
//	}
//};
//
//class Teacher : public People
//{
//public:
//	Teacher()
//	{
//		cout << "Teacher()" << endl;
//	}
//	~Teacher()
//	{
//		cout << "~Teacher()" << endl;
//	}
//};
//
//int main()
//{
//	student s;
//	Teacher t;
//}

//class A
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		A::fun();
//		cout << "func(int i)->" << i << endl;
//	}
//};
//void Test()
//{
//	B b;
//	b.fun(10);
//};

//class Person
//{
//protected:
//	string _name; // 姓名
//	string _sex; // 性别
//	int _age; // 年龄
//};
//class Student : public Person
//{
//public:
//	int _num; // 学号
//};
//void main()
//{
//	Student sobj;
//
//	// 1.子类对象可以赋值给父类对象/指针/引用
//	Person pobj = sobj;
//	Person* pobj1 = &sobj;
//	Person& pobj2 = sobj;
//
//	//2.基类对象不能赋值给派生类对象
//	sobj = pobj;
//
//	// 3.基类的指针可以通过强制类型转换赋值给派生类的指针
//	pobj1 = &sobj;
//	Student* ps1 = (Student*)pobj1; // 这种情况转换是可以的。
//	ps1->_num = 10;
//
//	pobj1 = &pobj;
//	Student* ps2 = (Student*)pobj1; // 这种情况转换时虽然可以，但是会存在越界访问的问题
//	ps2->_num = 10;
//}

//class Person
//{
//public:
//	Person(const char* name)
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//		return *this;
//	}
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//public:
//	Student(const char* name, int num)
//		:Person(name)
//		,_num(num)
//	{
//		cout << "Student()" << endl;
//	}
//
//	Student(const Student& s)
//		:Person(s)
//		,_num(s._num)
//	{
//		cout << "Student(const Student& s)" << endl;
//	}
//
//	Student& operator= (const Student& s)
//	{
//		cout << "Student& operator= (const Student& s)" << endl;
//		if (this != &s)
//		{
//			Person::operator=(s);
//			_num = s._num;
//		}
//		return *this;
//	}
//
//	~Student()
//	{
//		cout << "~Student" << endl;
//	}
//protected:
//	int _num; //学号
//};
//
//void main()
//{
//	Student s1("张三", 18);
//	Student s2(s1);
//	Student s3("李四", 23);
//	s1 = s3;
//}

//class Student;
//class Person
//{
//public:
//
//	friend void Display(const Person& p, const Student& s);
//protected:
//
//	string _name; // 姓名
//};
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;//可以访问
//	cout << s._stuNum << endl;//不可以访问
//}
//
//void main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//}

//class Person
//{
//public:
//	Person() { ++_count; }
//	string _name; // 姓名
//
//public:
//	static int _count; // 统计人的个数。
//};
//int Person::_count = 0;
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//
//void main()
//{
//	Person p;
//	Student s;
//
//	cout << "父类:" << & p._name << endl;
//	cout << "子类:" << & s._name << endl;
//
//	cout << p._count << endl;
//	cout << s._count << endl;
//
//	cout << "父类:" << &p._count << endl;
//	cout << "子类:" << &s._count << endl;
//}

//class Person final
//{
//public:
//	Person()
//	{
//
//	}
//protected:
//	int _a;
//};
//
//class Student : public Person
//{
//
//};
//
//int main()
//{
//	Student s;
//}

//class Person
//{
//public:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//protected:
//	int _num; //学号
//};
//
//class Teacher : public Person
//{
//protected:
//	int _id; // 职工编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//
//void main()
//{
//	Assistant a;
//	//因为Student类里有一份_name，Teacher类里也有一份_name，这样就会有二义性无法确定你要访问的是哪一个
//	//a._name = "张三";
//
//	//需要显示指定访问哪个父类的成员可以解决二义性问题，但是数据冗余问题无法解决
//	a.Student::_name = "李四";
//	a.Teacher::_name = "王五";
//}

//class Person
//{
//public:
//	string _name; // 姓名
//};
//
//class Student : virtual public Person
//{
//protected:
//	int _num; //学号
//};
//
//class Teacher : virtual public Person
//{
//protected:
//	int _id; // 职工编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//
//void main()
//{
//	Assistant a;
//	a._name = "张三";
//}


//class A
//{
//public:
//	int _a;
//};
//
////class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//
////class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	B b;
//	b._a = 1;
//	b._b = 2;
//	cout << sizeof(b) << endl;
//	return 0;
//}

//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	
//	return 0;
//}

//int main()
//{
//	D d;
//	B b = d;
//	C c = d;
//	return 0;
//}

// Person和Student构成is-a的关系
//class Person {
//public:
//	Person() {}
//protected:
//	int _a;
//};
//
//class Student : public Person {
//public:
//	 Student() {}
//};
//
//
//// Tire和Car构成has-a的关系
//class Tire {
//protected:
//	string _brand = "Michelin"; // 品牌
//};
//class Car {
//protected:
//	Tire _t; // 轮胎
//};


//class Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-全价" << endl;
//	}
//};
//
////子类虚函数不加virtual依旧构成重写
//class Student : public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-半价" << endl;
//	}
//};
//
//class Soldier : public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "优先买票" << endl;
//	}
//};
//
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	Person ps;
//	Student stu;
//	Soldier sd;
//
//	Func(ps);
//	Func(stu);
//	Func(sd);
//	return 0;
//}

//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//class B : public A
//{
//public:
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	cout << sizeof(B) << endl;
//	return 0;
//}


//class Base
//{
//public:
//	void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//};
//
//int main()
//{
//	cout << sizeof(Base) << endl;
//	return 0;
//}

//class Person
//{
//public:
//	virtual Person* BuyTicket()
//	{
//		cout << "买票-全价" << endl;
//		return this;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual Student* BuyTicket()
//	{
//		cout << "买票-半价" << endl;
//		return this;
//	}
//};
//
//class Soldier : public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "优先买票" << endl;
//	}
//};
//
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	Person ps;
//	Student stu;
//	Soldier sd;
//
//	Func(ps);
//	Func(stu);
//	Func(sd);
//
//	return 0;
//}

//class A
//{};
//class B : public A
//{};

//class Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-全价" << endl;
//
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-半价" << endl;
//
//	}
//};
//
//int main()
//{
//	Student s;
//	s.BuyTicket();
//}

//class Person
//{
//public:
//	virtual ~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual ~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//};
//
//int main()
//{
//	Person* ptr1 = new Person;
//	delete ptr1;
//
//	Person* ptr2 = new Student;
//	delete ptr2;
//}

//class Person
//{
//public:
//	virtual void BuyTicket() 
//	{
//		cout << "买票-全价" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual void BuyTicket() override
//	{
//		cout << "买票-半价" << endl;
//	}
//};
//
//int main()
//{
//	Student s;
//	Person* p = new Student;
//	return 0;
//}


//class Byd
//{
//public:
//	void Drive()
//	{
//		cout << "Byd-舒适" << endl;
//	}
//};
//int main()
//{
//	Byd* b = new Byd;
//	b->Drive();
//	return 0;
//}

//class B
//{
//public:
//	virtual void Func()
//	{
//		cout << "Func()" << endl;
//	}
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Func3()" << endl;
//	}
//private:
//	int _a;
//};
//
//class C : public B
//{
//public:
//	virtual void Func()
//	{
//		cout << "C::Func()" << endl;
//	}
//	virtual void Func4()
//	{
//		cout << "C::Func4()" << endl;
//	}
//private:
//	int _c;
//};
//
//int main()
//{
//	B b;
//	C c;
//
//	cout << sizeof(b) << endl;
//	cout << sizeof(c) << endl;
//	return 0;
//}

//class B
//{
//public:
//	virtual void Func()
//	{
//		cout << "B::Func()" << endl;
//	}
//	virtual void Func1()
//	{
//		cout << "B::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "B::Func2()" << endl;
//	}
//private:
//	int _a;
//};
//
//class C : public B
//{
//public:
//	virtual void Func()
//	{
//		cout << "C::Func()" << endl;
//	}
//	virtual void Func4()
//	{
//		cout << "C::Func4()" << endl;
//	}
//private:
//	int _c;
//};
//
//typedef void(*VFPTR)();
//
//void PrintVFTable(VFPTR table[], size_t n)
//{
//	for (size_t i = 0; table[i] != nullptr; i++)
//	{
//		printf("vft[%d]:%p->", i, table[i]);
//		/*VFPTR pf = table[i];
//		pf();*/
//		table[i]();
//	}
//	//for (size_t i = 0; i < n; ++i)
//	//{
//	//	
//	//}
//	cout << endl;
//}
////
////
//int main()
//{
//	B b;
//	C c;
//	PrintVFTable((VFPTR*)*(int*)&c, 4);
//	PrintVFTable((VFPTR*)*(int*)&b, 3);
//
//	return 0;
//}

class Base1 {
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int b1;
};

class Base2 {
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2;
};

class Derive : public Base1, public Base2 {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
	void func4() { cout << "Derive::func4" << endl; }
private:
	int d1;
};

typedef void(*VFPTR)();

void PrintVFTable(VFPTR table[])
{
	for (size_t i = 0; table[i] != nullptr; i++)
	{
		printf("vft[%d]:%p->", i, table[i]);
		table[i]();
	}
	cout << endl;
}

int main()
{
	Derive d;
	PrintVFTable((VFPTR*)*(int*)&d);
	PrintVFTable((VFPTR*)(*(int*)((char*)&d + sizeof(Base1))));
	//cout << sizeof(d) << endl;

	d.func1();

	Base2* ptr2 = &d;
	ptr2->func1();

	Base1* ptr1 = &d;
	ptr1->func1();
	return 0;
}