#pragma once
#include <assert.h>
#include "reverse_iterator.h"
using namespace std;
template <class T>
struct list_node
{
	T _data;
	list_node<T>* _next;
	list_node<T>* _prev;
	list_node(const T& x = T())
		:_data(x)
		,_next(nullptr)
		,_prev(nullptr)
	{}
};

template <class T,class Ref,class Ptr>
struct __list_iterator
{
	typedef list_node<T> Node;
	typedef __list_iterator<T, Ref, Ptr> iterator;
	Node* _node;

	__list_iterator(Node* node)
		:_node(node)
	{}

	bool operator!=(const iterator& it)const
	{
		return _node != it._node;
	}

	bool operator==(const iterator& it)const
	{
		return _node == it._node;
	}

	//*it
	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &(operator*());
	}
	//it++
	iterator& operator++()
	{
		_node = _node->_next;
		return *this;
	}

	//++it
	iterator& operator++(int)
	{
		iterator tmp(*this);
		_node = _node->_next;
		return tmp;
	}

	//it--
	iterator& operator--()
	{
		_node = _node->_prev;
		return *this;
	}

	//--it
	iterator& operator--(int)
	{
		iterator tmp(*this);
		_node = _node->_prev;
		return tmp;
	}
};

template <class T>
class mylist
{
	typedef list_node<T> Node;
public:
	typedef __list_iterator<T, T&, T*> iterator;
	typedef __list_iterator<T, const T&, const T*> const_iterator;

	typedef __reverse_iterator<iterator, T&, T*> reverse_iterator;
	typedef __reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;

	const_iterator begin()const
	{
		return const_iterator(_head->_next);
	}

	const_iterator end()const
	{
		return const_iterator(_head);
	}

	iterator begin()
	{
		return iterator(_head->_next);
	}

	iterator end()
	{
		return iterator(_head);
	}

	reverse_iterator rbegin()
	{
		return reverse_iterator(end());
	}

	reverse_iterator rend()
	{
		return reverse_iterator(begin());
	}
	//创建并初始化头结点
	void empty_init()
	{
		_head = new Node;
		_head->_next = _head;
		_head->_prev = _head;
	}

	mylist()
	{
		empty_init();
	}
	template<class Inputiterator>

	mylist(Inputiterator frist, Inputiterator last)
	{
		empty_init();
		while (frist != last)
		{
			push_back(*frist);
			frist++;
		}
	}
	~mylist()
	{
		clear();
		delete _head;
		_head = nullptr;
	}

	//void swap(mylist<T>& x)
	void swap(mylist& x)
	{
		std::swap(_head, x._head);
	}

	mylist(const mylist<T>& lt)
	{
		empty_init();
		mylist<T> tmp(lt.begin(), lt.end());
		swap(tmp);
	}

	mylist<T>& operator=(mylist<T> lt)
	{
		swap(lt);
		return *this;
	}
	void clear()
	{
		iterator it = begin();
		while (it != end())
		{
			it = erase(it);
		}
	}
	void push_back(const T& x)
	{
		//Node* tail = _head->_prev;
		//Node* newnode = new Node(x);
		//// head   tail  newnode
		//tail->_next = newnode;
		//newnode->_prev = tail;
		//newnode->_next = _head;
		//_head->_prev = newnode;

		insert(end(), x);
	}

	void push_front(const T& x)
	{
		insert(begin(), x);
	}
	iterator insert(iterator pos, const T& x)
	{
		Node* cur = pos._node;
		Node* prev = cur->_prev;
		Node* newnode = new Node(x);

		//prev newnode cur
		prev->_next = newnode;
		newnode->_prev = prev;
		newnode->_next = cur;
		cur->_prev = newnode;

		return iterator(newnode);
	}

	void pop_back()
	{
		erase(--end());
	}

	void pop_front()
	{
		erase(begin());
	}

	iterator erase(iterator pos)
	{
		assert(pos != end());
		Node* cur = pos._node;
		Node* prev = cur->_prev;
		Node* next = cur->_next;

		prev->_next = next;
		next->_prev = prev;
		delete cur;

		return iterator(next);
	}
private:
	Node* _head;
};
