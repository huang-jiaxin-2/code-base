#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include "list.h"

void test_list1()
{
	mylist<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	/*mylist<int>::iterator it1 = lt.begin();
	while (it1 != lt.end())
	{
		cout << *it1 << " ";
		++it1;
	}
	cout << endl;

	while (it1 != lt.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;*/
}
void Func(const mylist<int>& l)
{
	mylist<int>::const_iterator it = l.begin();
	while (it != l.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

void test_list2()
{
	mylist<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	Func(lt);
}
void test_list3()
{
	mylist<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	lt.pop_back();

	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list4()
{
	mylist<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	mylist<int> copy(lt);
	for (auto e : copy)
	{
		cout << e << " ";
	}
	cout << endl;

	mylist<int> lt1;
	lt1.push_back(10);
	lt1.push_back(20);
	lt1.push_back(30);
	lt1.push_back(40);
	lt1.push_back(50);

	copy = lt1;
	for (auto e : copy)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list5()
{
	mylist<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	mylist<int>::reverse_iterator rit = lt.rbegin();
	while (rit != lt.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;
}
int main()
{
	test_list5();
	return 0;
}