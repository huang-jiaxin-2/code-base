#include <iostream>

using namespace std;

struct Node {
	int _data;
	Node* _prev;
	Node* _next;

	Node(int data):_data(data),_prev(nullptr),_next(nullptr)
	{}
};


class MyLinkedList {
private:
	Node* head;
	Node* tail;
	int size;
private:
	void copyfrom(const MyLinkedList& other)
	{
		clear();
		if (other.head == nullptr) return;
		Node* cur = other.head;
		head = new Node(cur->_data);
		tail = head;

		while (cur->_next != nullptr)
		{
			Node* temp = new Node(cur->_next->_data);
			tail->_next = temp;
			temp->_prev = tail;
			tail = temp;
			cur = cur->_next;
		}
	}
public:
	MyLinkedList() :head(nullptr), tail(nullptr)
	{}

	MyLinkedList& operator=(const MyLinkedList& other)
	{
		if (this != &other)
		{
			copyfrom(other);
		}
		return *this;
	}

	void push_back(int val)
	{
		Node* newNode = new Node(val);
		if (!head)
		{
			head = tail = newNode;
		}

		tail->_next = newNode;
		newNode->_prev = tail;
		tail = newNode;
		size++;
	}

	void push_front(int val)
	{
		Node* newNode = new Node(val);
		if (!head)
		{
			head = tail = newNode;
		}

		head->_prev = newNode;
		newNode->_next = head;
		head = newNode;
		size++;
	}

	void pop_back()
	{
		if (!tail) return;
		Node* temp = tail;
		tail = tail->_prev;
		if (tail)
		{
			tail->_next = nullptr;
		}
		else
		{
			head = nullptr;
		}
		delete temp;
		size--;
	}

	void pop_front()
	{
		if (!head) return;
		Node* temp = head;
		head = head->_next;
		if (head)
		{
			head->_prev = nullptr;
		}
		else
		{
			tail = nullptr;
		}
		delete temp;
		size--;
	}

	void insert(int val, int index)
	{
		if (index < 0 || index > size)
		{
			return;
		}

		Node* newNode = new Node(val);
		if (index == 0)
		{
			push_front(val);
		}
		else if (index == size)
		{
			push_front(val);
		}
		else
		{
			Node* cur = head;
			for (int i = 0; i < index - 1; i++)
			{
				cur = cur->_next;
			}
			newNode->_next = cur->_next;
			newNode->_prev = cur;
			cur->_next->_prev = newNode;
			cur->_next = newNode;
			size++;
		}
	}

	void erase(int index) {
		if (index < 0 || index >= size) {
			std::cerr << "Index out of bounds" << std::endl;
			return;
		}

		if (index == 0) {
			pop_front();
		}
		else if (index == size - 1) {
			pop_back();
		}
		else {
			Node* cur = head;
			for (int i = 0; i < index; ++i) {
				cur = cur->_next;
			}
			Node* temp = cur;
			cur->_prev->_next = cur->_next;
			cur->_next->_prev = cur->_prev;
			delete temp;
			--size;
		}
	}

	void clear()
	{
		while (head != nullptr)
		{
			Node* temp = head;
			head = head->_next;
			delete temp;
		}
		tail = nullptr;
	}

	void print()
	{
		Node* cur = head;
		while (cur)
		{
			cout << cur->_data << " ";
			cur = cur->_next;
		}
		cout << endl;
	}

	~MyLinkedList()
	{
		while (head != nullptr)
		{
			Node* temp = head;
			head = head->_next;
			delete temp;
		}
	}
};

int main()
{
	MyLinkedList list;
	list.push_back(1);
	list.push_back(2);
	list.push_back(3);
	list.push_back(4);
	list.print();
	list.push_front(3);
	list.print();
	return 0;
}