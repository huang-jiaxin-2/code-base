#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include<memory>
#include <atomic>
using namespace std;

//mutex mtx;
//
//void Print(int n, int& x)
//{
//	for (int i = 0; i < n; i++)
//	{
//		mtx.lock();
//		cout << this_thread::get_id() << ":" << i << endl;
//		this_thread::sleep_for(chrono::milliseconds(100));
//		x++;
//		mtx.unlock();
//
//	}
//}
//
//int main()
//{
//	int count = 0;
//	thread t1(Print, 10, ref(count));
//	thread t2(Print, 10, ref(count));
//
//	t1.join();
//	t2.join();
//	cout << count << endl;
//	return 0;
//}

//int main()
//{
//	mutex mtx;
//	int n = 10;
//	int x = 0;
//	thread t1([&]() {
//		for (int i = 0; i < n; i++)
//		{
//			mtx.lock();
//			cout << this_thread::get_id() << ":" << i << endl;
//			this_thread::sleep_for(chrono::milliseconds(100));
//			x++;
//			mtx.unlock();
//
//		}
//		});
//
//	thread t2([&]() {
//		for (int i = 0; i < n; i++)
//		{
//			mtx.lock();
//			cout << this_thread::get_id() << ":" << i << endl;
//			this_thread::sleep_for(chrono::milliseconds(100));
//			x++;
//			mtx.unlock();
//
//		}
//		});
//	t1.join();
//	t2.join();
//
//	cout << x << endl;
//	return 0;
//}

//int main()
//{
//	mutex mtx;
//	int n = 10;
//	int x = 0;
//	int m = 0;
//	cin >> m;
//	vector<thread> v(m);
//	v.resize(m);
//
//	for (int i = 0; i < m; i++)
//	{
//		移动赋值
//		v[i] = thread([&]() {
//			for (int i = 0; i < n; i++)
//			{
//				mtx.lock();
//				cout << this_thread::get_id() << ":" << i << endl;
//				this_thread::sleep_for(chrono::milliseconds(100));
//				x++;
//				mtx.unlock();
//			}
//			});
//	}
//
//	for (auto& e : v)
//	{
//		e.join();
//	}
//	
//	
//
//	cout << x << endl;
//	return 0;
//}

//int main()
//{
//	mutex mtx;
//	int n = 1000000;
//	int x = 0;
//	int m = 0;
//	cin >> m;
//	vector<thread> v(m);
//	//v.resize(m);
//
//	//for (int i = 0; i < m; i++)
//	//{
//	//	//移动赋值
//	//	v[i] = thread([&]() {
//	//		for (int i = 0; i < n; i++)
//	//		{
//	//			//并行
//	//			mtx.lock();
//	//			x++;
//	//			mtx.unlock();
//	//		}
//	//		});
//	//}
//
//	for (int i = 0; i < m; i++)
//	{
//		//移动赋值
//		v[i] = thread([&]() {
//			//串行
//			mtx.lock();
//			for (int i = 0; i < n; i++)
//			{
//				x++;
//			}
//			mtx.unlock();
//			});
//	}
//
//	//加锁串行比并行快，并行时线程切换消耗了太多时间
//
//	for (auto& e : v)
//	{
//		e.join();
//	}
//
//
//
//	cout << x << endl;
//	return 0;
//}

//int main()
//{
//	mutex mtx;
//	int n = 1000000;
//	atomic<int> x = 0;
//	int m = 0;
//	cin >> m;
//	vector<thread> v(m);
//	//v.resize(m);
//
//	for (int i = 0; i < m; i++)
//	{
//		//移动赋值
//		v[i] = thread([&]() {
//			for (int i = 0; i < n; i++)
//			{
//				x++;
//			}
//			});
//	}
//
//	for (auto& e : v)
//	{
//		e.join();
//	}
//
//	cout << x << endl;
//	return 0;
//}

//RAII
//template<class Lock>
//class LockGuard
//{
//public:
//	LockGuard(Lock& lk)
//		:_lock(lk)
//	{
//		_lock.lock();
//		cout << "thread:" << this_thread::get_id() << "加锁" << endl;
//
//
//	}
//	~LockGuard()
//	{
//		cout << "thread:" << this_thread::get_id() << "解锁" << endl << endl;
//		_lock.unlock();
//	}
//private:
//	Lock& _lock;
//};

//int main()
//{
//	mutex mtx;
//	int n = 100;
//	atomic<int> x = 0;
//	int m = 0;
//	cin >> m;
//	vector<thread> v(m);
//	//v.resize(m);
//
//	for (int i = 0; i < m; i++)
//	{
//		//移动赋值
//		v[i] = thread([&]() {
//			for (int i = 0; i < n; i++)
//			{
//				/*mtx.lock();
//				try {
//					cout << this_thread::get_id() << ":" << i << endl;
//					this_thread::sleep_for(chrono::milliseconds(100));
//				}
//				catch(...)
//				{
//					mtx.unlock();
//					throw;
//				}
//				mtx.unlock();*/
//				//LockGuard<mutex> lock(mtx);
//				{
//					lock_guard<mutex> lock(mtx);
//					cout << this_thread::get_id() << ":" << i << endl;
//				}
//				this_thread::sleep_for(chrono::milliseconds(100));
//			}
//			});
//	}
//
//	for (auto& e : v)
//	{
//		e.join();
//	}
//
//	cout << x << endl;
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int n = 100;
//	thread t1([&]() {
//		while (i < n)
//		{
//			while (i % 2 == 0)
//			{
//				this_thread::yield();
//			}
//			cout << this_thread::get_id() << ":" << i << endl;
//			i += 1;
//		}
//		});
//
//	thread t2([&]() {
//		while (i < n)
//		{
//			while (i % 2 != 0)
//			{
//				this_thread::yield();
//			}
//			cout << this_thread::get_id() << ":" << i << endl;
//			i += 1;
//		}
//		});
//
//	t1.join();
//	t2.join();
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int n = 100;
//	mutex mtx;
//	condition_variable cv;
//	bool ready = true;
//
//	//打印偶数
//	thread t1([&]() {
//		while (i < n)
//		{
//			unique_lock<mutex> lock(mtx);
//			cv.wait(lock, [&ready]() {return ready; });
//			cout << this_thread::get_id() << ":" << i << endl;
//			i += 1;
//			ready = false;
//			cv.notify_one();
//		}
//		});
//
//	//打印奇数
//	thread t2([&]() {
//		while (i < n)
//		{
//			unique_lock<mutex> lock(mtx);
//			cv.wait(lock, [&ready]() {return !ready; });
//			cout << this_thread::get_id() << ":" << i << endl;
//			i += 1;
//			ready = true;
//			cv.notify_one();
//		}
//		});
//
//	this_thread::sleep_for(chrono::seconds(2));
//	cout << "t1:" << t1.get_id() << endl;
//	cout << "t2:" << t2.get_id() << endl;
//
//	t1.join();
//	t2.join();
//	return 0;
//}

//智能指针的多线程问题
//namespace HJX
//{
//	template<class T>
//	class shared_ptr
//	{
//	public:
//		shared_ptr(T* ptr = nullptr)
//			:_ptr(ptr)
//			, _pRefCount(new int(1))
//			, _pMutex(new mutex)
//		{}
//
//		shared_ptr(const shared_ptr<T>& sp)
//			:_ptr(sp._ptr)
//			, _pRefCount(sp._pRefCount)
//			, _pMutex(sp._pMutex)
//		{
//			AddRef();
//		}
//
//		void Release()
//		{
//			bool flag = false;
//
//			_pMutex->lock();
//			if (--(*_pRefCount) == 0 && _ptr)
//			{
//				cout << "delete:" << _ptr << endl;
//				delete _ptr;
//				delete _pRefCount;
//
//				flag = true;
//			}
//			_pMutex->unlock();
//
//			if (flag)
//				delete _pMutex;
//		}
//
//		void AddRef()
//		{
//			_pMutex->lock();
//
//			++(*_pRefCount);
//
//			_pMutex->unlock();
//		}
//
//		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
//		{
//			if (_ptr != sp._ptr)
//			{
//				Release();
//
//				_ptr = sp._ptr;
//				_pRefCount = sp._pRefCount;
//				_pMutex = sp._pMutex;
//				AddRef();
//			}
//
//			return *this;
//		}
//
//		int use_count()
//		{
//			return *_pRefCount;
//		}
//
//		~shared_ptr()
//		{
//			Release();
//		}
//
//		// 像指针一样使用
//		T& operator*()
//		{
//			return *_ptr;
//		}
//
//		T* operator->()
//		{
//			return _ptr;
//		}
//
//		T* get() const
//		{
//			return _ptr;
//		}
//	private:
//		T* _ptr;
//		int* _pRefCount;
//		mutex* _pMutex;
//	};
//}
//
//int main()
//{
//	std::shared_ptr<double> sp1(new double(1.11));
//	std::shared_ptr<double> sp2(sp1);
//
//	int n = 100000;
//
//	mutex mtx;
//	vector<thread> v(2);
//	for (auto& t : v)
//	{
//		t = thread([&]() {
//			for (size_t i = 0; i < n; i++)
//			{
//				std::shared_ptr<double> sp(sp1);
//
//				mtx.lock();
//				(*sp)++;
//				mtx.unlock();
//			}
//			});
//	}
//
//	for (auto& t : v)
//	{
//		t.join();
//	}
//
//	cout << sp1.use_count() << endl;
//	cout << (*sp1) << endl;
//	return 0;
//}

class Singleton
{
public:
	static Singleton* GetInstance()
	{
		if (_pInstance == nullptr)
		{
			unique_lock<mutex> lock(_mtx);
			if (_pInstance == nullptr)
			{
				_pInstance = new Singleton;
			}
		}

		return _pInstance;
	}

private:
	// 构造函数私有
	Singleton(){};

	// C++11
	Singleton(Singleton const&) = delete;
	Singleton& operator=(Singleton const&) = delete;

	static Singleton* _pInstance;
	static mutex _mtx;
};

Singleton* Singleton::_pInstance = nullptr;
mutex Singleton::_mtx;

int main()
{

	return 0;
}