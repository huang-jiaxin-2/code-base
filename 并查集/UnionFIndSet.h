#pragma once
#include <iostream>
#include <vector>
#include <map>

using namespace std;

//template<class T>
//class UnionFindSet
//{
//public:
//	UnionFindSet(const T* a, size_t n)
//	{
//		for (size_t i = 0; i < n; i++)
//		{
//			_a.push_back(a[i]);
//			_indexMap[a[i]] = i;
//		}
//	}
//private:
//	vector<T> _a;
//	map<T, int> _indexMap;
//};


class UnionFindSet
{
public:
	UnionFindSet(size_t n) :_ufs(n, -1)
	{}

	void Union(int x1, int x2)
	{
		int root1 = FindRoot(x1);
		int root2 = FindRoot(x2);

		//本省就在一个集合里就没必要合并
		if (root1 == root2)
			return;

		_ufs[root1] += _ufs[root2];
		_ufs[root2] = root1;
	}

	int FindRoot(int index)
	{
		while (_ufs[index] >= 0)
		{
			index = _ufs[index];
		}

		return index;
	}

	bool InSet(int x1, int x2)
	{
		return FindRoot(x1) == FindRoot(x2);
	}

	size_t Count()const
	{
		size_t size = 0;
		for (size_t i = 0; i < _ufs.size(); i++)
		{
			if (_ufs[i] < 0)
				size++;
		}
		return size;
	}
private:
	vector<int> _ufs;
};