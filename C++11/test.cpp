#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <array>
#include <functional>
using namespace std;

//struct point
//{
//	int x;
//	int y;
//};
//int main()
//{
//	int k1 = { 1 };
//	int k2{ 2 };
//	int a1[] = { 1,2,3,4 };
//	int a2[]{ 1,2,3,4 };
//	point p1 = { 1,2 };
//	point p2{ 1,2 };
//	int* p = new int[5]{ 0 };
//	return 0;
//}
//
class Date
{
public:
	Date(int year, int month, int day)
		:_year(year)
		, _month(month)
		, _day(day)
	{
		cout << "Date(int year, int month, int day)" << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

//int main()
//{
//	
//	Date d1(2022, 1, 1);
//	//C++11
//	Date d2 = { 2022,1,1 };
//	Date d3{ 2002,1,1 };
//	return 0;
//}

//int main()
//{
//	// the type of il is an initializer_list
//	auto il = { 10, 20, 30 };
//	cout << typeid(il).name() << endl;
//	return 0;
//}

//int main()
//{
//	//C++11支持，不建议使用
//	//int a1 = { 1 };
//	//int a2{ 2 };
//
//	//都是在调用构造函数
//	//Date d1(2022, 1, 1);
//	////C++11
//	//Date d2 = { 2022,1,1 };
//	//Date d3{ 2002,1,1 };
//
//	vector<int> v1 = { 1,2,3,4,5,6 };
//	vector<int> v2 { 1,2,3,4,5,6,7 };
//	list<int> lt1 = { 1,2,3,4,5 };
//	list<int> lt2{ 1,32,4,5,6 };
//
//	auto x = { 1,2,3,4,5 };
//	cout << typeid(x).name() << endl;
//
//	vector<Date> v4 = { {2022,1,1},{2023,1,1} };
//
//	map<string, string> dict = { {"apple","苹果"},{"banana","香蕉"} };
//	dict = { {"sort","排序"} };
//	return 0;
//}

// decltype的一些使用使用场景
//template<class T1, class T2>
//void F(T1 t1, T2 t2)
//{
//	decltype(t1 * t2) ret;
//	cout << typeid(ret).name() << endl;
//}
//int main()
//{
//	const int x = 1;
//	double y = 2.2;
//	decltype(x * y) ret; // ret的类型是double
//	decltype(&x) p; // p的类型是int*
//	cout << typeid(ret).name() << endl;
//	cout << typeid(p).name() << endl;
//	F(1, 'a');
//	return 0;
//}


//#ifndef NULL
//#ifdef __cplusplus
//#define NULL 0
//#else
//#define NULL ((void *)0)
//#endif
//#endif
//
//int main()
//{
//	NULL;
//	nullptr;
//}
//
//int main()
//{
//	/*int x = 1;
//	decltype(x) y1 = 2.22;
//	auto y2 = 2.22;
//
//	cout << y1 << endl;
//	cout << y2 << endl;*/
//	//const size_t N = 100;
//	////c语言越界检查，越界读基本检查不出来，越界写是抽查
//	//int a1[N];
//	//a1[N];//越界读
//	//a1[N + 100] = 1;//越界写
//
//	////越界读写都可以检查出来
//	//array<int, N> a2;
//	//a2[N];
//	//a2[N] = 1;
//	//a2[N + 5] = 1;
//	return 0;
//}

//x既能接收左值，也能接收右值
//void Func(const int& x)
//{
//
//}
//
//int main()
//{
//	//以下的a、b、*p、p都是左值
//	int a = 1;
//	const int b = 2;
//	int* p = new int(1);
//
//	//以下是对上面左值的左值引用
//	int& ra = a;
//	const int& rb = b;
//	int*& rp1 = p;
//	int& rp2 = *p;
//
//	double x = 1.1, y = 2.2;
//	//右值
//	10;
//	x + y;
//
//	//右值引用：给右值取别名
//	int&& r1 = 10;
//	double&& r2 = x + y;
//	return 0;
//}

////引用价值：减少拷贝 
//int main()
//{
//	//左值：可以取出它的地址
//	int a = 10;
//	int b = 1;
//	int* p = &a;
//	//左值引用
//	int& ra = a;
//	int& rb = b;
//	int*& rp = p;
//
//	//右值：不能取出它的地址
//	double x = 1.1, y = 2.2;
//	10;
//	x + y;
//	//右值引用：给右值取别名
//	int&& r1 = 10;
//	double&& r2 = x + y;
//
//	//左值引用不可以引用右值，但是加const修饰可以
//	//double& rr1 = x + y;
//	const double& rr1 = x + y;
//
//	//右值引用不可以引用左值，但是可以引用move以后的左值
//	//int&& r4 = b;
//	int&& r4 = move(b);
//	cout << &r1 << endl;
//	cout << &r2 << endl;
//	return 0;
//}

//void Func(int& x) 
//{ 
//	cout << "左值引用" << endl; 
//}
//void Func(const int& x) 
//{ 
//	cout << "const 左值引用" << endl; 
//}
//void Func(int&& x) 
//{ 
//	cout << "右值引用" << endl; 
//}
//void Func(const int&& x) 
//{ 
//	cout << "const 右值引用" << endl; 
//}
//
//
//// 万能引用：t既能引用左值，也能引用右值
//// 也可以叫引用折叠
////要用模板
//template<typename T>
//void PerfectForward(T&& t)
//{
//	//Func(t);//t都是左值
//	
//	// 完美转发：保持t引用对象属性
//	Func(forward<T>(t));//t是左值就是左值，右值就是右值
//}
//
//int main()
//{
//	PerfectForward(10);           // 右值
//
//	int a;
//	PerfectForward(a);            // 左值
//	PerfectForward(move(a));     // 右值
//
//	const int b = 8;
//	PerfectForward(b);		     // const 左值
//	PerfectForward(move(b));     // const 右值
//	return 0;
//}

//delete——禁止生成默认函数
//使用delete关键字，实现一个类，要求只能在堆上创建空间
//class HeapOnly
//{
//	~HeapOnly() = delete;
//};
//
//int main()
//{
//	HeapOnly* ptr = new HeapOnly;
//	return 0;
//}

//////////////////////////////////////////////////////////////////////
//可变参数模板

//递归终止函数
//void ShowList()
//{
//	cout << endl;
//}
//
//template <class T, class ...Args>
////Args... args代表N个参数包(N >= 0)
//void ShowList(const T& val, Args... args)
//{
//	//cout << sizeof...(args) << endl;//可以帮助我们算出参数的个数
//	//cout << val << " ";
//	cout << "ShowList(" << val << ", " << sizeof...(args) << "参数包)" << endl;
//	ShowList(args...);
//}

//template<class T>
//int PrintArg(const T& x)
//{
//	cout << x << " ";
//	return 0;
//}
//
//template <class ...Args>
////Args... args代表N个参数包(N >= 0)
//void ShowList(Args... args)
//{
//	int a[] = { PrintArg(args)... };
//	cout << endl;
//}
//int main()
//{
//	string str("hello");
//	ShowList(1, "ssss");
//	ShowList(1, "dsfd", str);
//	ShowList(1, "fdfd", 's', str);
//	return 0;
//}


//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year = 1, int month = 1, int day = 1)" << endl;
//	}
//
//	Date(const Date& d)
//		:_year(d._year)
//		, _month(d._month)
//		, _day(d._day)
//	{
//		cout << "Date(const Date& d)" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	//没有区别
//	/*vector<int> v1;
//	v1.push_back(1);
//	v1.emplace_back(2);
//
//	vector<pair<string, int>> v2;
//	v2.push_back(make_pair("sort", 1));
//	v2.emplace_back(make_pair("sort", 1));
//
//	v2.emplace_back("sort", 1);*/
//
//	/*vector<Date> v1;
//	v1.push_back(Date(2022, 12, 29));
//	cout << endl;
//
//	v1.emplace_back(2022, 12, 30);*/
//
//	list<Date> lt;
//	lt.push_back(Date(2022, 12, 29));
//	cout << endl;
//
//	lt.emplace_back(2022, 12, 30);
//	return 0;
//}

//////////////////////////////////////////////////////////////////
//lambda表达式
//int main()
//{
//	auto add1 = [](int a, int b)->int {return a + b; };//lambda表达式
//	cout << add1(1, 2) << endl;
//
//	//省略返回值
//	auto add2 = [](int a, int b){return a + b; };
//	cout << add2(1, 2) << endl;
//
//	int x = 10, y = 20;
//	auto swap1 = [](int& x1, int& x2)->void {int tmp = x1; x1 = x2; x2 = tmp; };
//	swap1(x, y);
//	cout << x << "<->" << y << endl;
//
//	auto swap2 = [](int& x1, int& x2)
//	{
//		int tmp = x1; 
//		x1 = x2; 
//		x2 = tmp; 
//	};
//	swap2(x, y);
//	cout << x << "<->" << y << endl;
//
//
//	//捕捉列表
//	//默认捕捉的对象不能修改，要加mutable
//	auto swap3 = [&x, &y]()mutable  //传引用捕捉
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	swap3();
//	cout << x << "<->" << y << endl;
//	return 0;
//}
//
//int main()
//{
//	int a, b, c, d, e;
//	a = b = c = d = e = 1;
//	//全部传值捕捉
//	auto f1 = [=]() {
//		cout << a << b << c << d << e << endl;
//	};
//	f1();
//	return 0;
//}


//////////////////////////////////////////////////////////
//包装器

//int f(int a, int b)
//{
//	return a + b;
//}
//
//struct Functor
//{
//public:
//	int operator() (int a, int b)
//	{
//		return a + b;
//	}
//};
//
//class Plus
//{
//public:
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//
//	double plusd(double a, double b)
//	{
//		return a + b;
//	}
//};
//
//int main()
//{
//	function<int(int, int)> f1 = f;
//	f1(1, 2);
//
//	function<int(int, int)> f2 = Functor();
//	f1(1, 2);
//
//	function<int(int, int)> f3 = Plus::plusi;
//	f3(1, 2);
//
//	function<double(Plus, double, double)> f4 = &Plus::plusd;
//	f4(Plus(), 1.1, 2.2);
//	return 0;
//}

//template<class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//
//	return f(x);
//}
//
//double f(double i)
//{
//	return i / 2;
//}
//
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};

//int main()
//{
//	// 函数指针
//	function<double(double)> f1 = f;
//	cout << useF(f1, 11.11) << endl;
//
//	// 函数对象
//	function<double(double)> f2 = Functor();
//	cout << useF(f2, 11.11) << endl;
//
//	// lambda表达式对象
//	function<double(double)> f3 = [](double d)->double{ return d / 4; };
//	cout << useF(f3, 11.11) << endl;
//
//	return 0;
//}

//class mystring
//{
//
//};
//
//mystring to_string(int value)
//{
//	mystring str;
//	//...
//
//	return str;
//}
//
//int main()
//{
//	mystring ret = to_string(123);
//
//	return 0;
//}

int main()
{
	int s[10] = {0};
	for (auto i : s)
	{
		cout << i << " ";
	}
	cout << endl;
	return 0;
}