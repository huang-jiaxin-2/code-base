#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

//前置声明
struct BinaryTreeNode;
typedef struct BinaryTreeNode* QDataType;

typedef struct QueueNode
{
	struct QueueNode* next;
	QDataType data;
}QNode;

typedef struct Queue
{
	int size;
	QNode* head;
	QNode* tail;
}Queue;

//初始化队列
void QueueInit(Queue* pq);

//销毁队列
void QueueDestroy(Queue* pq);

//对尾入队列
void QueuePush(Queue* pq, QDataType x);

//对头出队列
void QueuePop(Queue* pq);

//获取对列头部元素
QDataType QueueFront(Queue* pq);

//获取队列队尾元素
QDataType QueueBack(Queue* pq);

//判空
bool QueueEmpty(Queue* pq);

//获取队列中有效元素的个数
int QueueSize(Queue* pq);
