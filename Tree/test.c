#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "Queue.h"

typedef int BTDataType;
typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
	BTDataType data;
}BTNode;

BTNode* BuyNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	assert(node);

	node->data = x;
	node->left = NULL;
	node->right = NULL;

	return node;
}
BTNode* CreatBinaryTree()
{
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode * node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);
	BTNode* node7 = BuyNode(7);


	node1->left = node2;
	node1->right = node4;
	node2->left = node3;
	node4->left = node5;
	node4->right = node6;
	node3->left = node7;

	return node1;
}

void ProOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("# ");
		return;
	}

	printf("%d ", root->data);
	ProOrder(root->left);
	ProOrder(root->right);
}
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("# ");
		return;
	}

	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("# ");
		return;
	}

	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//1
//int count = 0;
//void TreeSize(BTNode* root)
//{
//	if (root == NULL)
//	{
//		return;
//	}
//
//	count++;
//	TreeSize(root->left);
//	TreeSize(root->right);
//}

//2
int TreeSize(BTNode* root)
{
	return root == NULL ? 0 : TreeSize(root->left) + TreeSize(root->right) + 1;
}

//1
//int count = 0;
//void TreeLeafSize(BTNode* root)
//{
//	if (root == NULL)
//	{
//		return;
//	}
//
//	if (root->left==NULL&&root->right==NULL)
//	{
//		count++;
//	}
//
//	TreeLeafSize(root->left);
//	TreeLeafSize(root->right);
//}

//2树的叶子结点的个数
int TreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}

	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
}

//树一层的结点数
int TreeKLevel(BTNode* root, int k)
{
	assert(k >= 1);
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}

	return TreeKLevel(root->left, k - 1) + TreeKLevel(root->right, k - 1);
}

//树的深度
//写法1
//int TreeDepth(BTNode* root)
//{
//	if (root == NULL)
//		return 0;
//	int leftDepth=TreeDepth(root->left) + 1;
//	int rightDepth = TreeDepth(root->right) + 1;
//	return leftDepth > rightDepth ? leftDepth : rightDepth;
//}

//写法2
int TreeDepth(BTNode* root)
{
	if (root == NULL)
		return 0;
	int leftDepth = TreeDepth(root->left);
	int rightDepth = TreeDepth(root->right);
	return leftDepth > rightDepth ? leftDepth+1 : rightDepth+1;
}

//查找树的结点——可读性更好
//写法1：
//BTNode* TreeFind(BTNode* root, BTDataType x)
//{
//	if (root == NULL)
//	{
//		return NULL;
//	}
//	if (root->data == x)
//	{
//		return root;
//	}
//	 
//	BTNode* ret1 = TreeFind(root->left, x);
//	if (ret1)
//		return ret1;
//
//	BTNode* ret2 = TreeFind(root->right, x);
//	if (ret2)
//		return ret2;
//
//	return NULL;
//}
//写法2
BTNode* TreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == x)
	{
		return root;
	}

	BTNode* ret = TreeFind(root->left, x);

	if (ret != NULL)
		return ret;
	return TreeFind(root->right, x);
}

void TreeDestroy(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}

	TreeDestroy(root->left);
	TreeDestroy(root->right);
	//printf("%p:%d\n", root, root->data);
	free(root);
}

void LevelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
	{
		QueuePush(&q, root);
	}
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		printf("%d ", front->data);
		QueuePop(&q);
		if (front->left)
		{
			QueuePush(&q, front->left);
		}
		if (front->right)
		{
			QueuePush(&q, front->right);
		}
	}
	printf("\n");
	QueueDestroy(&q);
}
// 判断二叉树是否是完全二叉树
int TreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
	{
		QueuePush(&q, root);
	}
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front)
		{
			QueuePush(&q, front->left);
			QueuePush(&q, front->right);
		}
		else
		{
			//遇到空以后，则跳出层序遍历
			break;
		}
		//1.如果后面全是空，则是完全二叉树
		//2.如果空后面还有非空，则不是完全二叉树
		while (!QueueEmpty(&q))
		{
			BTNode* front = QueueFront(&q);
			QueuePop(&q);
			if (front)
			{
				QueueDestroy(&q);
				return false;
			}
		}
	}
	
	QueueDestroy(&q);
	return true;
}

int main()
{
	BTNode* root = CreatBinaryTree();
	//前序
	ProOrder(root);//分治算法
	printf("\n");
	//中序
	InOrder(root);
	printf("\n");
	//后序
	PostOrder(root);
	printf("\n");

	//count = 0;
	//TreeSize(root);
	//printf("TreeSize:%d\n", count);

	//count = 0;
	//TreeSize(root);
	//printf("TreeSize:%d\n", count);//全局写法，多线程编不过去

	printf("TreeSize:%d\n", TreeSize(root));

	/*count = 0;
	TreeLeafSize(root);
	printf("TreeLeafSize:%d\n", count);*/
	printf("TreeLeafSize:%d\n", TreeLeafSize(root));

	printf("TreeKLevel:%d\n", TreeKLevel(root, 2));
	printf("TreeKLevel:%d\n", TreeKLevel(root, 3));
	BTNode* ret = TreeFind(root, 2);
	if (ret != NULL)
		printf("找到了\n");
	else
		printf("找不到\n");

	printf("TreeDepth:%d\n", TreeDepth(root));
	LevelOrder(root);
	printf("TreeComplete:%d\n", TreeComplete(root));

	TreeDestroy(root);
	root = NULL;

	return 0;

}