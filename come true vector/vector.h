#include <iostream>
#include <assert.h>
#include <initializer_list>
#include <algorithm>
#include "reverse_iterator.h"
using namespace std;

template <class T>
class myvector
{
public:
	typedef  T* iterator;
	typedef const T* const_iterator;

	typedef __reverse_iterator<iterator, T&, T*> reverse_iterator;
	typedef __reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;

	iterator begin()const 
	{
		return _start;
	}
	iterator end()const 
	{
		return _finish;
	}

	reverse_iterator rbegin()const
	{
		return reverse_iterator(end());
	}
	reverse_iterator rend()const
	{
		return reverse_iterator(begin());
	}

	myvector()
		:_start(nullptr)
		,_finish(nullptr)
		,_end_of_storage(nullptr)
	{
	}

	//传统写法
	//myvector(const myvector<T>& v)
	//{
	//	_start = new T[v.size()];
	//	memcpy(_start, v._start, sizeof(T) * v.size());//memcpy是浅拷贝不好
	//	for (size_t i = 0; i < size(); i++)
	//	{
	//		_start[i] = v._start[i];
	//	}
	//	_finish = _start + v.size();
	//	_end_of_storage = _start + v.size();
	//}
	
	//现代写法
	/*myvector(const myvector<T>& v)
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{
		reserve(v.size());
		for (const auto& e : v)
		{
			push_back(e);
		}
	}*/

	myvector(initializer_list<T> il)
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{
		reserve(il.size());
		for (const auto& e : il)
		{
			push_back(e);
		}
	}

	template <class InputIterator>
	myvector(InputIterator first, InputIterator last)
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{
		while (first != last)
		{
			push_back(*first);
			first++;
		}
	}

	void swap(myvector<T>& v)
	{
		std::swap(_start, v._start);
		std::swap(_finish, v._finish);
		std::swap(_end_of_storage, v._end_of_storage);
	}

	myvector(const myvector<T>& v)
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{
		myvector<T> tmp(v.begin(), v.end());
		swap(tmp);
	}

	

	myvector<T>& operator=(myvector<T> v)
	{
		swap(v);
		return *this;
	}

	~myvector()
	{
		delete[] _start;
		_start = _finish = _end_of_storage = nullptr;
	}

	size_t capacity()const
	{
		return _end_of_storage - _start;
	}

	size_t size()const
	{
		return _finish - _start;
	}

	void reserve(size_t n)
	{
		if (n > capacity())
		{
			size_t sz = size();
			T* tmp = new T[n];
			if (_start)
			{
				//memcpy(tmp, _start, sizeof(T) * sz);
				for (size_t i = 0; i < sz; i++)
				{
					tmp[i] = _start[i];
				}
				delete[] _start;
			}

			_start = tmp;
			_finish = _start + sz;
			_end_of_storage = _start + n;
		}	
	}

	void resize(size_t n, const T& val = T())
	{
		if (n > capacity())
		{
			reserve(n);
		}
		if (n > size())
		{
			//初始化填值
			while (_finish < _start + n)
			{
				*_finish = val;
				_finish++;
			}
		}
		else
		{
			_finish = _start + n;
		}
	}

	 T& operator[](size_t pos)
	{
		assert(pos < size());

		return _start[pos];
	}

	const T& operator[](size_t pos)const
	{
		assert(pos < size());

		return _start[pos];
	}

	void push_back(const T& x)
	{
		/*if (_finish == _end_of_storage)
		{
			reserve(capacity() == 0 ? 4 : capacity() * 2);
		}
		*_finish = x;
		_finish++;*/
		insert(end(), x);
	}

	void pop_back()
	{
		assert(_finish > _start);
		_finish--;
	}

	iterator insert(iterator pos, const T& x)
	{
		assert(pos >= _start && pos <= _finish);
		if (_finish == _end_of_storage)
		{
			size_t len = pos - _start;
			reserve(capacity() == 0 ? 4 : capacity() * 2);
			pos = _start + len;
		}

		iterator end = _finish - 1;
		while (end >= pos)
		{
			*(end + 1) = *end;
			end--;
		}

		*pos = x;
		_finish++;

		return pos;
	}

	iterator erase(iterator pos)
	{
		assert(pos >= _start && pos < _finish);

		iterator begin = pos + 1;
		while (begin < _finish)
		{
			*(begin - 1) = *begin;
			begin++;
		}
		_finish--;

		return pos;
	}

	T& front()
	{
		assert(size() > 0);
		return *_start;
	}

	T& back()
	{
		assert(size() > 0);
		return *(_finish - 1);
	}
private:
	iterator _start;
	iterator _finish;
	iterator _end_of_storage;
};