#pragma once

//���ã�������������
template<class Iterator, class Ref, class Ptr>
struct __reverse_iterator
{
	Iterator _cur;
	typedef __reverse_iterator<Iterator, Ref, Ptr> RIterator;

	__reverse_iterator(Iterator it)
		:_cur(it)
	{}

	RIterator operator++()
	{
		--_cur;
		return *this;
	}

	RIterator operator++(int)
	{
		auto tmp = _cur;
		--_cur;
		return tmp;
	}

	RIterator& operator--()
	{
		++_cur;
		return *this;
	}

	RIterator& operator--(int)
	{
		auto tmp = _cur;
		++_cur;
		return tmp;
	}

	Ref operator*()
	{
		//return *_cur;
		auto tmp = _cur;
		--tmp;
		return *tmp;
	}

	Ptr operator->()
	{
		return &(operator*());
	}

	bool operator!=(const RIterator& it)
	{
		return _cur != it._cur;
	}
};