#define _CRT_SECURE_NO_WARNINGS 1

#include "vector.h"
void test_vector1()
{
	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	for (size_t i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;

	v.pop_back();
	myvector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

}
void test_vector2()
{
	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	auto pos = find(v.begin(), v.end(), 5);
	if (pos != v.end())
	{
		v.insert(pos, 30);//用了一次pos，最好不要在用了，会发生迭代器失效
		cout << *pos << endl;
	}
	v.insert(v.end(), 50);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector3()
{
	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
	auto pos = find(v.begin(), v.end(), 3);
	if (pos != v.end())
	{
		v.erase(pos);
	}

	v.erase(v.begin());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector4()
{
	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	//在所有偶数前面插入一个偶数的2倍
	auto it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			it = v.insert(it, *it * 2);
			it++;
			it++;
		}
		else
		{
			it++;
		}
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_vector5()
{
	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(2);

	v.push_back(3);
	v.push_back(4);
	v.push_back(5);


	auto it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			it = v.erase(it);
		}
		else
		{
			it++;
		}
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_vector6()
{
	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(2);

	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	myvector<int> v1(v);
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector7()
{
	string s("hello world");
	myvector<int> vs(s.begin(), s.end());
	for (auto e : vs)
	{
		cout << e << " ";
	}
	cout << endl;

	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(4);

	vs = v;  

	vs[0] *= 10;
	for (auto e : vs)
	{
		cout << e << " ";
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector8()
{
	myvector<int> v1;
	v1.resize(10, 0);
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	myvector<int> v2;
	v2.reserve(10);
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(3);
	v2.push_back(4);
	v2.push_back(5);

	v2.resize(8, 8);
	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;
	v2.resize(20, 10);
	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector9()
{
	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(4);

	myvector<int>::reverse_iterator rit = v.rbegin();
	while (rit != v.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;
}

void test_vector10()
{
	myvector<int> v = { 1,2,3,4 };
	myvector<int> v1{ 1,2,3,4,5 };
}
int main()
{
	test_vector10();
	return 0;
}