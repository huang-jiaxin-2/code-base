#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <functional>
#include "UnionFIndSet.h"

using namespace std;

//邻接矩阵
namespace matrix
{
	template<class V, class W, W MAX_W = INT_MAX, bool Direction = false>
	class Graph
	{
		typedef Graph<V, W, MAX_W, Direction> Self;
	public:
		Graph() = default;
		Graph(const V* a, size_t n)
		{
			_vertexs.reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				_vertexs.push_back(a[i]);
				_indexMap[a[i]] = i;
			}

			_matrix.resize(n);
			for (auto& e : _matrix)
			{
				e.resize(n, MAX_W);
			}
		}

		//获取顶点下标
		size_t GetVertexIndex(const V& v)
		{
			auto it = _indexMap.find(v);
			if (it != _indexMap.end())
			{
				return it->second;
			}
			else
			{
				throw invalid_argument("顶点不存在");
				return -1;
			}
		}

		//添加边
		void _AddEdge(size_t srci, size_t dsti, const W& w)
		{
			_matrix[srci][dsti] = w;
			//无向图
			if (Direction == false)
			{
				_matrix[dsti][srci] = w;
			}
		}

		void AddEdge(const V& src, const V& dst, const W& w)
		{
			size_t srci = GetVertexIndex(src);
			size_t dsti = GetVertexIndex(dst);
			_AddEdge(srci, dsti, w);
		}

		void Print()
		{
			//打印顶点
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				cout << "[" << i << "]" << "->" << _vertexs[i] << endl;
			}
			cout << endl;

			//打印矩阵
			//打印横下标
			cout << "  ";
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				cout << i << " ";
			}
			cout << endl;

			for (size_t i = 0; i < _matrix.size(); i++)
			{
				cout << i << " ";//打印竖下标
				for (size_t j = 0; j < _matrix[i].size(); j++)
				{
					
					if (_matrix[i][j] != MAX_W)
					{
						cout << _matrix[i][j] << " ";
					}
					else
					{
						//if (i == j)//对脚线
						//	cout << 0 << " ";
						//else
						cout << "#" << " ";
					}
				}
				cout << endl;
			}
			cout << endl << endl;
		}

		//void BFS(const V& src)
		//{
		//	size_t srci = GetVertexIndex(src);

		//	//队列和标记数组
		//	queue<int> q;
		//	vector<bool> visited(_vertexs.size(), false);

		//	q.push(srci);
		//	visited[srci] = true;

		//	size_t n = _vertexs.size();
		//	while (!q.empty())
		//	{
		//		int front = q.front();
		//		q.pop();
		//		cout << front << ":" << _vertexs[front] << endl;

		//		//把front顶点的邻接顶点入队列
		//		for (size_t i = 0; i < n; i++)
		//		{
		//			if (_matrix[front][i] != MAX_W)
		//			{
		//				if (visited[i] == false)
		//				{
		//					q.push(i);
		//					visited[i] = true;
		//				}
		//			}
		//		}
		//	}
		//	cout << endl;
		//}

		void BFS(const V& src)
		{
			size_t srci = GetVertexIndex(src);

			//队列和标记数组
			queue<int> q;
			vector<bool> visited(_vertexs.size(), false);

			q.push(srci);
			visited[srci] = true;
			int levelSize = 1;

			size_t n = _vertexs.size();
			while (!q.empty())
			{
				for (int i = 0; i < levelSize; i++)
				{
					int front = q.front();
					q.pop();
					cout << front << ":" << _vertexs[front] << " ";

					//把front顶点的邻接顶点入队列
					for (size_t i = 0; i < n; i++)
					{
						if (_matrix[front][i] != MAX_W)
						{
							if (visited[i] == false)
							{
								q.push(i);
								visited[i] = true;
							}
						}
					}
				}
				cout << endl;
				levelSize = q.size();
			}
		}

		void _DFS(size_t srci, vector<bool>& visited)
		{
			cout << srci << ":" << _vertexs[srci] << endl;
			visited[srci] = true;

			//找一个srci相邻的没有访问过的点，深度遍历
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				if (_matrix[srci][i] != MAX_W && visited[i] == false)
				{
					_DFS(i, visited);
				}
			}
		}

		void DFS(const V& src)
		{
			size_t srci = GetVertexIndex(src);
			vector<bool> visited(_vertexs.size(), false);

			_DFS(srci, visited);

			//图是不连通的情况
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				if (visited[i] == false)
				{
					srci = GetVertexIndex(_vertexs[i]);
					_DFS(srci, visited);
				}
			}
		}

		//最小生成树
		struct Edge
		{
			size_t _srci;
			size_t _dsti;
			W _w;

			Edge(size_t srci, size_t dsti, const W& w)
				:_srci(srci)
				,_dsti(dsti)
				,_w(w)
			{}

			bool operator > (const Edge& e)const
			{
				return _w > e._w;
			}
		};

		//最小生成树—Kruskal算法
		W Kruskal(Self& minTree)
		{
			size_t n = _vertexs.size();
			//初始化
			minTree._vertexs = _vertexs;
			minTree._indexMap = _indexMap;
			minTree._matrix.resize(n);
			for (size_t i = 0; i < n; i++)
			{
				minTree._matrix[i].resize(n, MAX_W);
			}

			//优先队列
			priority_queue<Edge, vector<Edge>, greater<Edge>> minque;
			for (size_t i = 0; i < n; i++)
			{
				for (size_t j = 0; j < n; j++)
				{
					if (i < j && _matrix[i][j] != MAX_W)
					{
						minque.push(Edge(i, j, _matrix[i][j]));
					}
				}
			}

			//选出n-1条边
			int size = 0;
			W totalW = W();
			UnionFindSet ufs(n);
			while (!minque.empty())
			{
				Edge min = minque.top();
				minque.pop();

				//判断顶点是否在一个集合里，避免成环
				if (!ufs.InSet(min._srci, min._dsti))
				{
					cout << _vertexs[min._srci] << "->" << _vertexs[min._dsti] << ":" << min._w << endl;
					minTree._AddEdge(min._srci, min._dsti, min._w);
					ufs.Union(min._srci, min._dsti);
					size++;
					totalW += min._w;
				}

			}

			if (size == n - 1)
			{
				return totalW;
			}
			else
			{
				return W();
			}
		}

		//最小生成树—Prim算法
		W Prim(Self& minTree, const W& src)
		{
			size_t srci = GetVertexIndex(src);
			size_t n = _vertexs.size();
			//初始化
			minTree._vertexs = _vertexs;
			minTree._indexMap = _indexMap;
			minTree._matrix.resize(n);
			for (size_t i = 0; i < n; i++)
			{
				minTree._matrix[i].resize(n, MAX_W);
			}

			//两个集合X，Y
			vector<bool> X(n, false);
			vector<bool> Y(n, true);
			X[srci] = true;
			Y[srci] = false;

			//从X->Y集合中连接的边里面选出权值最小的边
			priority_queue<Edge, vector<Edge>, greater<Edge>> minq;
			for (size_t i = 0; i < n; i++)
			{
				if (_matrix[srci][i] != MAX_W)
				{
					minq.push(Edge(srci, i, _matrix[srci][i]));
				}
			}

			size_t count = 0;
			W totalW = W();//权值之和
			while (!minq.empty())
			{
				Edge min = minq.top();
				minq.pop();

				//如果目标顶点也在集合X中则构成环
				if (!X[min._dsti])
				{
					minTree._AddEdge(min._srci, min._dsti, min._w);
					//cout << _vertexs[min._srci] << "->" << _vertexs[min._dsti] << ":" << min._w << endl;//方便观察数据用

					X[min._dsti] = true;
					Y[min._dsti] = false;

					count++;
					totalW += min._w;
					if (count == n - 1)
					{
						break;
					}

					for (size_t i = 0; i < n; i++)
					{
						if (_matrix[min._dsti][i] != MAX_W && Y[i])
						{
							minq.push(Edge(min._dsti, i, _matrix[min._dsti][i]));
						}
					}
				}
			}

			if (count == n - 1)
			{
				return totalW;
			}
			else
			{
				return W();
			}
		}

		//单源最短路径—Dijkstra算法
		void Dijkstra(const V& src, vector<W>& dist, vector<int>& pPath)
		{
			size_t srci = GetVertexIndex(src);
			size_t n = _vertexs.size();
			dist.resize(n, MAX_W);
			pPath.resize(n, -1);

			dist[srci] = 0;
			pPath[srci] = srci;

			//已经确定最短路径的顶点集合
			vector<bool> S(n, false);

			for (size_t i = 0; i < n; i++)
			{
				int u = 0;
				W min = MAX_W;
				//选出不在S中且路径最短的顶点
				for (size_t j = 0; j < n; j++)
				{
					if (S[j] == false && dist[j] < min)
					{
						min = dist[j];
						u = i;
					}
				}

				S[u] = true;//将已经确定最短的顶点放入S中
				//松弛更新
				for (size_t k = 0; k < n; k++)
				{
					if (S[k] == false && _matrix[u][k] != MAX_W && dist[u] + _matrix[u][k] < dist[k])
					{
						dist[k] = dist[u] + _matrix[u][k];
						pPath[k] = u;
					}
				}

			}
		}

		void PrintShortPath(const V& src, const vector<W>& dist, const vector<int>& pPath)
		{
			size_t srci = GetVertexIndex(src);
			size_t n = _vertexs.size();
			for (size_t i = 0; i < n; i++)
			{
				if (i != srci)
				{
					//找出i顶点的路径
					vector<int> path;
					size_t parenti = i;
					while (parenti != srci)
					{
						path.push_back(parenti);
						parenti = pPath[parenti];
					}
					path.push_back(srci);
					reverse(path.begin(), path.end());

					for (auto pos : path)
					{
						cout << _vertexs[pos] << "->";
					}
					cout << dist[i] << endl;
				}
			}
		}

		//单源最短路径—BellmanFord算法
		bool BellmanFord(const V& src, vector<W>& dist, vector<int>& pPath)
		{
			size_t srci = GetVertexIndex(src);
			size_t n = _vertexs.size();
			dist.resize(n, MAX_W);
			pPath.resize(n, -1);

			//先更新srci->srci
			dist[srci] = W();

			for (size_t k = 0; k < n; k++)
			{
				bool update = false;
				//printf("更新第%d轮\n", k);
				for (size_t i = 0; i < n; i++)
				{
					for (size_t j = 0; j < n; j++)
					{
						if (_matrix[i][j] != MAX_W && dist[i] + _matrix[i][j] < dist[j])
						{
							update = true;
							dist[j] = dist[i] + _matrix[i][j];
							pPath[j] = i;
						}
					}
				}

				//如果没有发生更新直接退出循环
				if (update == false)
				{
					break;
				}
			}

			//如果还能更新证明有负权回路
			for (size_t i = 0; i < n; i++)
			{
				for (size_t j = 0; j < n; j++)
				{
					if (_matrix[i][j] != MAX_W && dist[i] + _matrix[i][j] < dist[j])
					{
						return false;
					}
				}
			}

			return true;
		}

		//多源最短路径—FloydWarshall
		void FloydWarshall(vector<vector<W>>& vvDist, vector<vector<int>>& vvpPath)
		{
			size_t n = _vertexs.size();
			vvDist.resize(n);
			vvpPath.resize(n);
			for (size_t i = 0; i < n; i++)
			{
				vvDist[i].resize(n, MAX_W);
				vvpPath[i].resize(n, -1);
			}

			//直接相连的更新
			for (size_t i = 0; i < n; i++)
			{
				for (size_t j = 0; j < n; j++)
				{
					if (_matrix[i][j] != MAX_W)
					{
						vvDist[i][j] = _matrix[i][j];
						vvpPath[i][j] = i;
					}

					if (i == j)
					{
						vvDist[i][j] = 0;
					}
				}
			}

			//最短路径更新
			for (size_t k = 0; k < n; k++)
			{
				for (size_t i = 0; i < n; i++)
				{
					for (size_t j = 0; j < n; j++)
					{
						if (vvDist[i][k] != MAX_W && vvDist[k][j] != MAX_W && vvDist[i][k] + vvDist[k][j] < vvDist[i][j])
						{
							vvDist[i][j] = vvDist[i][k] + vvDist[k][j];
							vvpPath[i][j] = vvpPath[k][j];
						}
					}
				}
				// 打印权值和路径矩阵观察数据
				//for (size_t i = 0; i < n; ++i)
				//{
				//	for (size_t j = 0; j < n; ++j)
				//	{
				//		if (vvDist[i][j] == MAX_W)
				//		{
				//			//cout << "*" << " ";
				//			printf("%3c", '*');
				//		}
				//		else
				//		{
				//			//cout << vvDist[i][j] << " ";
				//			printf("%3d", vvDist[i][j]);
				//		}
				//	}
				//	cout << endl;
				//}
				//cout << endl;
				//for (size_t i = 0; i < n; ++i)
				//{
				//	for (size_t j = 0; j < n; ++j)
				//	{
				//		//cout << vvParentPath[i][j] << " ";
				//		printf("%3d", vvpPath[i][j]);
				//	}
				//	cout << endl;
				//}
				//cout << "=================================" << endl;
			}
		}

	private:
		vector<V> _vertexs;         //顶点集合
		map<V, int> _indexMap;      //顶点映射下标
		vector<vector<W>> _matrix; //领接矩阵
	};
}

namespace link_table
{
	template<class W>
	struct Edge
	{
		int _dsti; //目标点的下标
		W _w;     //权值
		Edge<W>* _next;

		Edge(int dsti, const W& w)
			:_dsti(dsti)
			, _w(w)
			,_next(nullptr)
		{}
	};

	template<class V, class W, bool Direction = false>
	class Graph
	{
		typedef Edge<W> Edge;
	public:
		Graph() = default;
		Graph(const V* a, size_t n)
		{
			_vertexs.reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				_vertexs.push_back(a[i]);
				_indexMap[a[i]] = i;
			}
			_tables.resize(n, nullptr);
		}

		//获取顶点下标
		size_t GetVertexIndex(const V& v)
		{
			auto it = _indexMap.find(v);
			if (it != _indexMap.end())
			{
				return it->second;
			}
			else
			{
				throw invalid_argument("顶点不存在");
				return -1;
			}
		}

		//添加边
		void AddEdge(const V& src, const V& dst, const W& w)
		{
			size_t srci = GetVertexIndex(src);
			size_t dsti = GetVertexIndex(dst);

			Edge* eg = new Edge(dsti, w);
			eg->_next = _tables[srci];
			_tables[srci] = eg;

			//无向图
			if (Direction == false)
			{
				Edge* eg = new Edge(srci, w);
				eg->_next = _tables[dsti];
				_tables[dsti] = eg;
			}
		}

		void Print()
		{
			//打印顶点
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				cout << "[" << i << "]" << "->" << _vertexs[i] << endl;
			}
			cout << endl;

			for (size_t i = 0; i < _tables.size(); i++)
			{
				cout << _vertexs[i] << "[" << i << "]->";
				Edge* cur = _tables[i];
				while (cur)
				{
					cout << _vertexs[cur->_dsti] << "[" << cur->_dsti << "]:" << cur->_w << "->";
					cur = cur->_next;
				}
				cout << "nullptr" << endl;
			}
		}

	private:
		vector<V> _vertexs;         //顶点集合
		map<V, int> _indexMap;      //顶点映射下标
		vector<Edge*> _tables;		//领接表
	};
}

