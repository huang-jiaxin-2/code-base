#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//值和引用作为返回值返回值类型的性能比较
//#include <time.h>
//struct A { int a[10000]; };
//A a;
//// 值返回
//A TestFunc1() { return a; }
//// 引用返回
//A& TestFunc2() { return a; }
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1 time:" << end1 - begin1 << endl;
//	cout << "TestFunc2 time:" << end2 - begin2 << endl;
//}

//int main()
//{
//	TestReturnByRefOrValue();
//	return 0;
//}

//引用
//int main()
//{
//	//权限平移
//	int a = 10;
//	int& b = a;
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//
//	//权限放大
//	//权限不能放大，只能权限平移
//	const int c = 20;
//	//int& d = c;
//	const int& d = c;
//
//	//权限缩小
//	int e = 30;
//	const int& f = e;
//	
//	//类型转化并不会改变原变量类型，中间会产生临时变量
//	//临时变量具有常性
//	int k = 1;
//	double g = k;
//	//double& gg = k;
//	const double& gg = k;
//
//	const int& w = 10;
//	return 0;
//}

//void Fun1(int n)
//{
//
//}
//
////如果使用引用传参，函数内如果不改变n,那么建议尽量用const引用传参
////void Fun2(int& n)
//void Fun2(const int& n)
//{
//
//}
//int main()
//{
//	int a = 10;
//	const int b = 20;
//	Fun1(a);
//	Fun1(b);
//	Fun1(30);
//
//	Fun2(a);
//	Fun2(b);
//	Fun2(30);
//	Fun2(1.11);
//	double d = 2.22;
//	Fun2(d);
//	return 0;
//}

//语法角度：引用没有开空间，指针开了空间
//底层角度：引用底层是用指针实现的
//int main()
//{
//	int a = 10;
//	int& b = a;
//	b = 20;
//
//	int* pa = &a;
//	*pa = 20;
//	return 0;
//}

//为什么c++支持函数重载，而C语言不支持
//因为c++有函数名修饰规则——参数不同，修饰出来的名字就不同
//

//宏的缺点：1、可读性差  2、没有类型安全检查  3、不放调试
//宏的优点：1、复用性强  2、宏函数提高效率，减少栈帧建立

//#define Add(x,y) ((x)+(y))

//内联函数 inline 符合条件的情况下，在调用的地方展开
//inline对于编译器而言只是一个建议，编译器会自动优化，如果定义为inline的函数体内有函数内部实现代码中指令比较长或者递归等，编译器有优化时会忽略掉内联
//inline void Add(int x, int y)
//{
//	int c = x + y;
//}
//int main()
//{
//	int x = 10+1;
//	int y = 20;
//	/*int c = Add(x, y);
//	printf("%d\n", c);
//	if (Add(1, 2))
//	{
//
//	}
//	Add(1, 2);
//	Add(1, 2)*3;
//	Add(x | y, x & y);*/
//	Add(1, 2);
//	return 0;
//}


//auto自动推导类型
//int Testauto()
//{
//	return 10;
//}
//int main()
//{
//	int a = 10;
//	auto b = a;
//	auto c = 'c';
//	auto d = Testauto();
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	for (auto i = 10; i < 20; i++)
//	{
//		cout << "hello world" << endl;
//	}
//	return 0;
//}

//int main()
//{
//	//int a[] = { 1,2,3,4,5,6 };
//	////范围for 
//	////自动依次取a的数据赋值给i
//	////自动迭代，自动判断结束
//	//for (auto i : a)
//	//{
//	//	cout << i << " ";
//	//}
//	//cout << endl;
//
//	int x = 10;
//	auto a = &x;
//	auto* b = &x;//强调一定要传指针
//	auto& c = x; //强调c是一个引用
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	return 0;
//}

