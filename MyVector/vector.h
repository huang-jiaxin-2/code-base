#pragma once
#include <iostream>
#include <algorithm>
#include <assert.h>

using namespace std;

template <class T>
class MyVector
{
public:
	typedef T* iterator;
	typedef const T* const_iterator;


	iterator begin()
	{
		return _start;
	}

	iterator end()
	{
		return _finish;
	}

	const_iterator begin()
	{
		return _start;
	}

	const_iterator end()
	{
		return _finish;
	}

	MyVector()
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{}

	~MyVector()
	{
		delete[] _start;
		_start = _finish = _end_of_storage = nullptr;
	}

	template <class InputIterator>
	MyVector(InputIterator first, InputIterator last)
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{
		while (first != last)
		{
			push_back(*first);
			first++;
		}
	}

	//传统写法
	/*MyVector(const MyVector<T>& v)
	{
		_start = new T[v.size()];
		memcpy(_start, v._start, sizeof(T) * v.size());
		_finish = _start + v.size();
		_end_of_storage = _start + v.size();
	}

	MyVector(const MyVector<T>& v)
		:_start(nullptr)
		,_finish(nullptr)
		,_end_of_storage(nullptr)
	{
		reserve(v.size());
		for (const auto& d : v)
		{
			push_back(d);
		}
	}*/
	void swap(MyVector<T>& v)
	{
		::swap(_start, v._start);
		::swap(_finish, v._finish);
		::swap(_end_of_storage, v._end_of_storage);
	}

	//现代写法
	MyVector(const MyVector<T>& v)
		:_start(nullptr)
		,_finish(nullptr)
		,_end_of_storage(nullptr)
	{
		MyVector tmp(v.begin(), v.end());
		swap(tmp);
	}

	size_t capacity()const
	{
		return _end_of_storage - _start;
	}

	size_t size()const
	{
		return _finish - _start;
	}

	T& operator[](size_t pos)
	{
		assert(pos < size());
		return _start[pos];
	}

	const T& operator[](size_t pos)const 
	{
		assert(pos < size());
		return _start[pos];
	}

	void reserve(size_t n)
	{
		if (n > capacity())
		{
			size_t sz = size();
			T* tmp = new T[n];
			if (_start)
			{
				memcpy(tmp, _start, sizeof(T) * sz);
				delete[] _start;
			}

			_start = tmp;
			_finish = _start + sz;
			_end_of_storage = _start + n;
		}
	}

	void push_back(const T& x)
	{
		if (_finish == _end_of_storage)
		{
			reserve(capacity() == 0 ? 4 : capacity() * 2);
		}
		*_finish = x;
		_finish++;
	}

	void pop_back()
	{
		assert(_finish > _start);
		_finish--;
	}

	iterator insert(iterator pos, const T& x)
	{
		assert(pos >= _start);
		assert(pos <= _finish);

		if (_finish == _end_of_storage)
		{
			size_t len = pos - _start;
			reserve(capacity() == 0 ? 4 : capacity() * 2);
			pos = _start + len;
		}

		iterator end = _finish - 1;
		while (end >= pos)
		{
			*(end - 1) = *end;
			end--;
		}

		*pos = x;
		_finish++;
		return pos;
	}

	iterator erase(iterator pos)
	{
		assert(pos >= _start && pos < _finish);
		iterator begin = pos + 1;
		while (begin < _finish)
		{
			*(begin - 1) = *(begin);
			begin++;
		}

		_finish--;
		return pos;
	}
private:
	iterator _start;
	iterator _finish;
	iterator _end_of_storage;
};


