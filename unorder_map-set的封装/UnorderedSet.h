#pragma once
#include "HashTable.h"

template<class K, class Hash = HashFunc<K>>
class unordered_myset
{
	struct SetKeyOfT
	{
		const K& operator()(const K& key)
		{
			return key;
		}
	};

public:
	typedef typename HashBucket::HashTable<K, K, Hash, SetKeyOfT>::iterator iterator;

	iterator begin()
	{
		return _ht.begin();
	}

	iterator end()
	{
		return _ht.end();
	}


	pair<iterator, bool> insert(const K& key)
	{
		return _ht.Insert(key);
	}
private:
	HashBucket::HashTable<K, K, Hash, SetKeyOfT> _ht;
};

void test_myset()
{
	unordered_myset<int> s;
	s.insert(1);
	s.insert(2);
	s.insert(3);
	s.insert(4);
	s.insert(5);

	unordered_myset<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}
