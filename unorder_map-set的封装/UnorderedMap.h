﻿#pragma once
#include "HashTable.h"

template<class K, class V, class Hash = HashFunc<K>>
class unordered_mymap
{
	struct MapKeyOfT
	{
		const K& operator()(const pair<K, V>& kv)
		{
			return kv.first;
		}
	};

public:
	typedef typename HashBucket::HashTable<K, pair<K, V>, Hash, MapKeyOfT>::iterator iterator;

	iterator begin()
	{
		return _ht.begin();
	}
	iterator end()
	{
		return _ht.end();
	}

	pair<iterator, bool> insert(const pair<K, V>& kv)
	{
		return _ht.Insert(kv);
	}

	V& operator[](const K& key)
	{
		pair<iterator, bool> ret = _ht.Insert(make_pair(key, V()));
		return ret.first->second;
	}

private:
	HashBucket::HashTable<K, pair<K, V>, Hash, MapKeyOfT> _ht;
};

void test_mymap()
{
	unordered_mymap<string, string> dict;
	dict.insert(make_pair("sort", "排序"));
	dict.insert(make_pair("string", "字符串"));
	dict.insert(make_pair("left", "左边"));
	unordered_mymap<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;

	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	unordered_mymap<string, int> count;
	for (auto e : arr)
	{
		count[e]++;
	}

	for (auto& kv : count)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}