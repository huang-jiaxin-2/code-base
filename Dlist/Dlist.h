#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

typedef int LTDataType;
typedef struct ListNode
{
	struct ListNode* next;
	struct ListNode* prev;
	LTDataType data;
}LTNode;



//初始化
//void ListInit(LTNode** pphead);
LTNode* ListInit();

//尾插
void ListPushBack(LTNode* phead, LTDataType x);

//打印
void ListPrint(LTNode* phead);

//头插
void ListPushFront(LTNode* phead, LTDataType x);

//尾删
void ListPopBack(LTNode* phead);

//头删
void ListPopFront(LTNode* phead);

//判空
bool ListEmpty(LTNode* phead);

//在pos位置之前插入x
void ListInsert(LTNode* pos, LTDataType x);

//删除pos位置的节点
void ListErase(LTNode* pos);

//链表长度
int ListSize(LTNode* phead);

//销毁链表
void ListDestory(LTNode* phead);