#define _CRT_SECURE_NO_WARNINGS 1
#include "Red_BlackTree.h"
#include "set.h"
#include "map.h"
void test_set()
{
	myset<int> s;
	s.insert(3);
	s.insert(2);
	s.insert(1);
	s.insert(5);
	s.insert(6);
	s.insert(7);
	s.insert(5);

	myset<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}
void test_map()
{
	/*mymap<int, int> m;
	m.insert(make_pair(1, 2));
	m.insert(make_pair(5, 4));
	m.insert(make_pair(7, 2));
	m.insert(make_pair(7, 8));*/
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };

	mymap<string, int> countMap;
	for (auto& str : arr)
	{
		countMap[str]++;
	}

	mymap<string, int>::iterator it = countMap.begin();
	while (it != countMap.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
}
int main()
{
	test_set();
	test_map();
	return 0;
}