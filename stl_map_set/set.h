#pragma once
#include "Red_BlackTree.h"

template<class K>
class myset
{
	struct SetKeyOfT
	{
		const K& operator()(const K& key)
		{
			return key;
		}
	};
public:
	typedef typename RBTree<K, K, SetKeyOfT>::iterator iterator;

	iterator begin()
	{
		return _t.begin();
	}

	iterator end()
	{
		return _t.end();
	}
	pair<iterator, bool> insert(const K& key)
	{
		return _t.Insert(key);
	}
private:
	RBTree<K, K, SetKeyOfT> _t;
};