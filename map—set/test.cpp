#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <functional>
#include <set>
#include <map>
#include <string>
using namespace std;

void test_set1()
{
	//set<int> s = { 1,2,1,6,3,2 };

	/*set<int> s;
	s.insert(1);
	s.insert(2);
	s.insert(3);
	s.insert(4);*/

	int a[] = { 1,2,3,4,5,6,7 };

	// 排序 + 去重
	set<int,greater<int>> s(a, a + sizeof(a) / sizeof(int));

	/*set<int>::reverse_iterator rit = s.rbegin();
	while (rit != s.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;*/

	set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	/*s.erase(20);
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	set<int>::iterator pos = s.find(30);
	s.erase(pos);
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;*/

	cout << s.count(1) << endl;
	cout << s.count(3) << endl;
}
void test_set2()
{
	//std::set<int> myset;
	//std::set<int>::iterator itlow, itup;

	//for (int i = 1; i < 10; i++) myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90

	//itlow = myset.lower_bound(35); // 返回的是>=val的值
	//itup = myset.upper_bound(60);  //  返回的是 > val的值                 

	//cout << *itlow << " " << * itup << endl;

	//myset.erase(itlow, itup); // 10 20 70 80 90

	//std::cout << "myset contains:";
	//for (auto e : myset)
	//{
	//	cout << e << " ";
	//}
	//cout << endl;
	//set<int> myset;

	//for (int i = 1; i <= 5; i++) myset.insert(i * 10);   // myset: 10 20 30 40 50

	//pair<set<int>::const_iterator, set<int>::const_iterator> ret;
	//ret = myset.equal_range(30); // x <= val < y

	//cout << "the lower bound points to: " << *ret.first << '\n';
	//cout << "the upper bound points to: " << *ret.second << '\n';

}
void test_set3()
{
	int a[] = { 1,2,3,4,5,6,7,1,2,3 };
	multiset<int> s(a, a + sizeof(a) / sizeof(int));//允许值冗余 排序
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << s.count(1) << endl;//不仅能知道是否有这个数还能返回它的个数

	auto pos = s.find(2);//如果有多个值，返回中序遍历的第一个
	while (pos != s.end())
	{
		cout << *pos << " ";
		pos++;
	}
	cout << endl;

	s.erase(3);//删除所有的3
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	pos = s.find(2);
	if (pos != s.end())
	{
		s.erase(pos);
	}
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_map1()
{
	map<string, string> dict;
	dict.insert(pair<string, string>("sort", "排序"));
	dict.insert(pair<string, string>("test", "测试"));
	dict.insert(pair<string, string>("string", "字符串"));
	dict.insert(pair<string, string>("sleep", "睡觉"));
	dict.insert(pair<string, string>("string", "xxxx"));
	dict.insert(make_pair("left", "左边"));
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		//cout << (*it).first << (*it).second << endl;
		cout << it->first << it->second << endl;//省略了一个箭头
		++it;
	}
	cout << endl;

	for (const auto& kval : dict)
	{
		cout << kval.first << kval.second << endl;
	}
	cout << endl;
}
void test_map2()
{
	string arr[] = { "苹果", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	//map<string, int> countMap;
	//for (auto& str : arr)
	//{
	//	map<string, int>::iterator it = countMap.find(str);
	//	if (it != countMap.end())
	//	{
	//		//(*it).second++;
	//		it->second++;
	//	}
	//	else
	//	{
	//		countMap.insert(make_pair(str, 1));
	//	}
	//}
	//map<string, int>::iterator it = countMap.begin();
	//while (it != countMap.end())
	//{
	//	cout << it->first << ":" << it->second << endl;
	//	++it;
	//}
	//cout << endl;

	/*map<string, int> countMap;
	for (auto& str : arr)
	{
		countMap[str]++;
	}
	map<string, int>::iterator it = countMap.begin();
	while (it != countMap.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;*/

	map<string, string> dict;
	dict.insert(make_pair("sort", "排序"));
	dict["sleep"];
	dict["sleep"] = "睡觉";//查找+修改
	dict["right"] = "右边";//插入+修改

	multimap<string, string> mdict;
	mdict.insert(make_pair("sort", "排序"));
	mdict.insert(make_pair("left", "左边"));
	mdict.insert(make_pair("left", "左边"));
	mdict.insert(make_pair("left", "剩余"));


}
int main()
{
	//test_set3();
	test_map2();
	return 0;
}