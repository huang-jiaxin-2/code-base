#define _CRT_SECURE_NO_WARNINGS 1
#include "string.h"

void test_string1()
{
	mystring s1("hello world");
	mystring s2;

	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;

	for (size_t i = 0; i < s1.size(); i++)
	{
		cout << s1[i] << " ";
	}
	cout << endl;

	for (size_t i = 0; i < s1.size(); i++)
	{
		s1[i]++;
	}
	for (size_t i = 0; i < s1.size(); i++)
	{
		cout << s1[i] << " ";
	}
	cout << endl;

	cout << s1.begin() << endl;

}
void test_string2()
{
	mystring s1("hello world");
	mystring::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto ch : s1)
	{
		cout << ch << " ";
	}
	cout << endl;
}
void test_string3()
{
	mystring s1("hello world");
	mystring s2(s1);
	mystring s3("1111111111111111111");
	//s1 = s3;
	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;
	//cout << s3.c_str() << endl;

}
void test_string4()
{
	mystring s1("hello");
	cout << s1.c_str() << endl;
	s1.push_back('x');
	cout << s1.c_str() << endl;
	cout << s1.capacity() << endl;

	s1 += 'h';
	s1 += 'u';
	s1 += 'a';
	s1 += 'n';
	s1 += 'g';

	cout << s1.c_str() << endl;
	cout << s1.capacity() << endl;

}
void test_string5()
{
	mystring s1("hello");
	s1 += ' ';
	s1.append("world");
	cout << s1.c_str() << endl;
	s1 += "hahaha";
	cout << s1.c_str() << endl;

}

void test_string6()
{
	mystring s1("hello");
	s1 += ' ';
	s1.append("world");
	cout << s1.c_str() << endl;
	s1 += "hahaha";
	cout << s1.c_str() << endl;
	s1.insert(5, 'e');
	cout << s1.c_str() << endl;
	s1.insert(0, '#');
	cout << s1.c_str() << endl;
	s1.insert(0, "hdhhd");
	cout << s1.c_str() << endl;

}

void test_string7()
{
	mystring s1("hello");
	s1 += ' ';
	s1.append("world");
	cout << s1.c_str() << endl;
	s1.erase(2, 3);
	cout << s1.c_str() << endl;

}
void test_string8()
{
	mystring s1("hello");
	s1 += ' ';
	s1.append("world");
	cout << s1.c_str() << endl;
	cout << s1 << endl;

	mystring s2("hello");
	cin >> s2;
	cout << s2 << endl;
}
void test_string9()
{
	mystring s1("hello");
	mystring s2("hellb");
	cout << (s1 == s2) << endl;
	cout << (s1 > s2) << endl;
	cout << (s1 < s2) << endl;
	cout << (s1 != s2) << endl;
	cout << (s1 >= s2) << endl;

}

void test_string10()
{
	mystring s1("hello");
	s1.resize(20, 'w');
	s1.resize(10);
	mystring s2;
	s2.resize(20);
}

void test_string11()
{
	mystring str1("hello");
	mystring str2(str1);//拷贝构造
	mystring str3(move(str1));//移动构造
}

void test_string12()
{
	mystring ret("111111");
	mystring str1("111111");
	//ret = to_mystring(-1333);//移动赋值
	ret = str1;//拷贝赋值
}
//void test_string11()
//{
//	string s0;
//	cout << sizeof(s0) << endl;
//
//	string s1("111111111111111");
//	string s2("111111111111111111111111111111111111111111111111111");
//	cout << sizeof(s1) << endl;
//	cout << sizeof(s2) << endl;
//
//	char ch[] = "dfdfgf";
//	cout << sizeof(ch) << endl;
//}
int main()
{
	try
	{
		test_string12();
		
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}
	return 0;
}