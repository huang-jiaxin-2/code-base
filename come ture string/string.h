#pragma once
#include <iostream>
#include <assert.h>
#include <algorithm>
//#include <string>
using namespace std;


class mystring
{
public:
	typedef char* iterator;
	typedef char* const_iterator;

	iterator begin()
	{
		return _str;
	}

	iterator end()
	{
		return _str + _size;
	}


	const_iterator begin()const
	{
		return _str;
	}

	const_iterator end()const
	{
		return _str + _size;
	}

	/*mystring()
		:_str(new char[1])
		, _size(0)
		, _capacity(0)
	{
		_str[0] = '\0';
	}*/
	//mystring(const char* str = "")//隐含了\0
	//	:_str(new char[strlen(str) + 1])
	//	, _size(strlen(str))
	//	, _capacity(strlen(str))//strlen太冗余了，减少strlen使用，用初始化列表就不太好了
	//{
	//	strcpy(_str, str);
	//}

	mystring(const char* str = "")//隐含了\0
	{
		_size = strlen(str);
		_capacity = _size;
		_str = new char[_capacity + 1];
		strcpy(_str, str);
	}

	//传统写法
	//s2(s1)
	/*mystring(const mystring& s)
		:_str(new char[s._capacity+1])
		,_size(s._size)
		,_capacity(s._capacity)
	{
		strcpy(_str, s._str);
	}

	mystring& operator=(const mystring& s)
	{
		if (this != &s)
		{
			char* tmp = new char[s._capacity + 1];
			strcpy(tmp, s._str);
			delete[] _str;
			_str = tmp;
			_size = s._size;
			_capacity = s._capacity;
		}

		return *this;
	}*/

	//现代写法
	void swap(mystring& tmp)
	{
		::swap(_str, tmp._str);
		::swap(_size, tmp._size);
		::swap(_capacity, tmp._capacity);
	}
	//拷贝构造
	mystring(const mystring& s)
		:_str(nullptr)
		,_size(0)
		,_capacity(0)
	{
		cout << "拷贝构造" << endl;
		mystring tmp(s._str);
		swap(tmp);
	}

	//移动构造
	mystring(mystring&& s)
		:_str(nullptr)
		,_size(0)
		,_capacity(0)
	{
		cout << "资源转移" << endl;
		swap(s);
	}
	/*mystring operator=(const mystring& s)
	{
		if (this != &s)
		{
			mystring tmp(s);
			swap(tmp);
		}
		return *this;
	}*/

	/*mystring& operator=(mystring s)
	{
		swap(s);
		return *this;
	}*/

	//拷贝赋值
	mystring& operator=(mystring& s)
	{
		cout << "拷贝赋值" << endl;
		mystring tmp(s);
		swap(tmp);
		return *this;
	}

	//移动赋值
	mystring& operator=(mystring&& s)
	{
		cout << "移动赋值" << endl;
		swap(s);
		return *this;
	}

	~mystring()
	{
		delete[] _str;
		_str = nullptr;
		_size = _capacity = 0;
	}
	const char* c_str()const
	{
		return _str;
	}

	size_t size()const
	{
		return _size;
	}

	size_t capacity()const
	{
		return _capacity;
	}

	const char& operator[](size_t pos) const
	{
		assert(pos < _size);

		return _str[pos];
	}

	char& operator[](size_t pos)
	{
		assert(pos < _size);
		return _str[pos];
	}

	void reserve(size_t n)
	{
		if (n > _capacity)
		{
			char* tmp = new char[n + 1];
			strcpy(tmp, _str);
			delete[] _str;

			_str = tmp;
			_capacity = n;
		}
	}
	void resize(size_t n, char ch = '\0')
	{
		if (n > _size)
		{
			//插入数据
			reserve(n);
			for (size_t i = _size; i < n; i++)
			{
				_str[i] = ch;
			}
			_str[n] = '\0';
			_size = n;
		}
		else
		{
			//删除数据
			_str[n] = '\0';
			_size = n;
		}
	}

	void push_back(char ch)
	{
		/*if (_size == _capacity)
		{
			reserve(_capacity == 0 ? 4 : _capacity * 2);
		}

		_str[_size++] = ch;
		_str[_size] = '\0';*/
		insert(_size, ch);
	}
	mystring& operator+=(char ch)
	{
		push_back(ch);
		return *this;
	}

	void append(const char* str)
	{
		//size_t len = strlen(str);
		//if (_size + len > _capacity)
		//{
		//	reserve(_size + len);
		//}
		//strcpy(_str + _size, str);
		////strcat(_str, str);//不推荐使用，需要找\0，效率低
		//_size += len;
		insert(_size, str);
	}

	void append(const mystring& s)
	{
		append(s._str);
	}

	mystring& operator+=(const char* ch)
	{
		append(ch);
		return *this;
	}

	mystring& insert(size_t pos, char ch)
	{
		assert(pos <= _size);
		if (_size == _capacity)
		{
			reserve(_capacity == 0 ? 4 : _capacity * 2);
		}

		size_t end = _size + 1;
		while (end > pos)
		{
			_str[end] = _str[end - 1];
			end--;
		}

		_str[pos] = ch;
		_size++;

		return *this;
	}

	mystring& insert(size_t pos, const char* str)
	{
		assert(pos <= _size);
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}

		size_t end = _size + len;
		while (end >= pos+len)
		{
			_str[end] = _str[end - len];
			end--;
		}

		strncpy(_str + pos, str, len);
		_size += len;

		return *this;
	}

	void erase(size_t pos, size_t len = npos)
	{
		assert(pos < _size);
		if (len == npos || pos + len >= _size)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			strcpy(_str + pos, _str + pos + len);
			_size -= len;
		}
	}
	void clear()
	{
		_str[0] = '\0';
		_size = 0;
	}

	size_t find(char ch, size_t pos = 0)const
	{
		assert(pos < _size);
		for (int i = pos; i < _size; i++)
		{
			if (ch == _str[i])
			{
				return i;
			}
		}
		return npos;
	}
	size_t find(const char* sub, size_t pos = 0)const
	{
		assert(sub);
		assert(pos < _size);

		const char* ptr = strstr(_str + pos, sub);
		if (ptr == nullptr)
		{
			return npos;
		}
		else
		{
			return ptr - _str;
		}
	}

	mystring substr(size_t pos, size_t len = npos)const
	{
		assert(pos < _size);
		size_t reallen = len;
		if (len == npos || pos + len > _size)
		{
			reallen = _size - pos;
		}
		mystring sub;
		for (size_t i = 0; i < reallen; i++)
		{
			sub += _str[pos + i];
		}

		return sub;
	}

	bool operator > (const mystring& s)const
	{
		return strcmp(_str, s._str) > 0;
	}

	bool operator == (const mystring& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool operator >= (const mystring& s)const
	{
		return *this > s || *this == s;
	}

	bool operator <= (const mystring& s)const
	{
		return !(*this > s);
	}

	bool operator < (const mystring& s)const
	{
		return !(*this >= s);
	}

	bool operator != (const mystring& s)const
	{
		return !(*this == s);
	}
private:
	char* _str;
	size_t _size;
	size_t _capacity;
public:
	//直接可以当成定义初始化
	const static size_t npos = -1; 
};

//size_t mystring::npos = -1;
ostream& operator<<(ostream& out, const mystring& s)
{
	for (size_t i = 0; i < s.size(); i++)
	{
		out << s[i];
	}
	return out;
}

istream& operator>>(istream& in, mystring& s)
{
	s.clear();
	char ch;
	ch = in.get();
	const size_t N = 64;
	char buff[N];
	size_t i = 0;
	while (ch != ' ' && ch != '\n')
	{
		buff[i++] = ch;
		if (i == N - 1)
		{
			buff[i] = '\0';
			s += buff;
			i = 0;
		}
		ch = in.get();
	}

	buff[i] = '\0';
	s += buff;
	return in;
}

mystring to_mystring(int value)
{
	bool flag = true;
	if (value < 0)
	{
		flag = false;
		value = 0 - value;
	}

	mystring str;
	while (value > 0)
	{
		int x = value % 10;
		value /= 10;

		str += ('0' + x);
	}

	if (flag == false)
	{
		str += '-';
	}

	std::reverse(str.begin(), str.end());
	return str;
}
