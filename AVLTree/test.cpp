#define _CRT_SECURE_NO_WARNINGS 1
#include "AVLTree.h"
#include <time.h>
using namespace std;
void TestAVLTree1()
{
	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	//int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };//����
	AVLTree<int, int> t1;
	for (auto e : a)
	{
		t1.Insert(make_pair(e, e));
	}
	t1.InOrder();
	cout << "IsBalance:" << t1.IsBalance() << endl;
}

void TestAVLTree2()
{
	size_t N = 100;
	srand(time(0));
	AVLTree<int, int> t1;

	for (size_t i = 0; i < N; i++)
	{
		int x = rand();
		t1.Insert(make_pair(x, i));
	}
	cout << "IsBalance:" << t1.IsBalance() << endl;

}
int main()
{
	TestAVLTree2();
	return 0;
}