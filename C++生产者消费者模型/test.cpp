#include <iostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <queue>
using namespace std;

queue<int> data_queue;
mutex mtx;
condition_variable cv;
bool done = false;

void producer(int id)
{
	for (int i = 1; i <= 10; i++)
	{
		unique_lock<mutex> lck(mtx);
		data_queue.push(i);

		cv.notify_one();
		lck.unlock();

		this_thread::sleep_for(chrono::seconds(1));
	}

	{
		lock_guard<mutex> lck(mtx);
		done = true;
		cv.notify_all();
	}
}

void consumer()
{
	unique_lock<mutex> lck(mtx);
	while (!done || !data_queue.empty())
	{
		cv.wait(lck, [] {return done || !data_queue.empty(); });

		if (!data_queue.empty())
		{
			int value = data_queue.front();
			data_queue.pop();

			cout << value * 10 << endl;
		}
	}
}

int main()
{
	thread th1[2];//生产者
	thread th2[2];//消费者

	for (int i = 0; i < 2; i++)
	{
		th1[i] = thread(producer, i);
	}

	for (int i = 0; i < 2; i++)
	{
		th2[i] = thread(consumer);
	}

	for (auto& th : th1)
	{
		th.join();
	}

	cv.notify_all();

	for (auto& th : th2)
	{
		th.join();
	}

	return 0;
}