#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <assert.h>
using namespace std;

void test_string1()
{
	string s1;
	string s2 = "hello world!!!";
	cout << s1 << endl;
	cout << s2 << endl;

	string s3(s2);
	cout << s3 << endl;

	string s4(s2, 6, 3);
	cout << s4 << endl;

	string s5(s2, 6, 15);
	cout << s5 << endl;

	string s6(s2, 6);
	cout << s6 << endl;

	string s7("hello world", 6);
	cout << s7 << endl;

	string s8(10, 'n');
	cout << s8 << endl;
}

void test_string2()
{
	string s1;
	string s2 = "hello world";//隐式类型的转换
    
	//赋值
	s1 = s2;
	s1 = 'r';
	s1 = "fdfdg";
}

void test_string3()
{
	string s1 = "hello world";
	cout << s1[3] << endl;
	s1[3] = 's';
	cout << s1[3] << endl;
	cout << s1 << endl;

	for (size_t i = 0; i < s1.size(); i++)
	//for (size_t i = 0; i < s1.length(); i++)
	{
		s1[i]++;
	}
	cout << s1 << endl;
	cout<<s1.at(2)<<endl;
}

void test_string4()
{
	string s1("hello");
	string::iterator it = s1.begin();//迭代器

	//依次取s中的每个字符，赋值给ch
	for (auto& ch : s1)//自动++，自动判断结束
	{
		ch++;
		cout << ch << " ";
	}
	cout << endl;
	
	cout << s1 << endl;

	/*while (it != s1.end())
	{
		(*it)++;
		cout << *it << " ";
		it++;
	}
	cout << endl;*/
}
void Print(const string& s)
{
	string::const_iterator cit = s.begin();
	while (cit != s.end())
	{
		cout << *cit << " ";
		cit++;
	}
	cout << endl;
}
void test_string5()
{
	string s("hello");
	string::reverse_iterator rit = s.rbegin();
	while (rit != s.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;
	Print(s);
}

void test_string6()
{
	string s("hello");
	s.push_back('-');
	s.push_back('-');
	s.append("world");
	cout << s << endl;

	string str("没错就是我");
	s += '!';
	s += str;
	s += "nb";
	cout << s << endl;
	s.append(str.begin(), str.end());
	cout << s << endl;

}

void test_string7()
{
	/*string s1("hello");
	string s2("2112e323");
	cout << s1.capacity() << endl;
	cout << s2.capacity() << endl;*/
	string s;
	s.reserve(1000);//开空间
	s.resize(1000);//开空间+初始化
	size_t sz = s.capacity();

	cout << "capacity changed: " << sz << '\n';

	s.reserve(2000);//开空间

	sz = s.capacity();
	cout << "capacity changed: " << sz << '\n';
	cout << "making s grow:\n";

	/*for (int i = 0; i < 1000; ++i)
	{
		s.push_back('c');
		if (sz != s.capacity())
		{
			sz = s.capacity();
			cout << "capacity changed: " << sz << '\n';
		}
	}*/
}
void test_string8()
{
		/*string str("Hello.");
		str.reserve(111);
		str.resize(5);
		str.reserve(50);
		cout << str.size() << ":" << str.capacity() << endl;*/
	string s("lai le lao di");
	/*for (size_t i = 0; i < s.size(); i++)
	{
		if (s[i] == ' ')
		{
			s.insert(i, "20%");
			i += 3;
		}
	}
	for (size_t i = 0; i < s.size(); i++)
	{
		if (s[i] == ' ')
		{
			s.erase(i, 1);
		}
	}
	cout << s << endl;*/

	string newstr;
	for (size_t i = 0; i < s.size(); i++)
	{
		if (s[i] != ' ')
		{
			newstr += s[i];
		}
		else
		{
			newstr += "20%";
		}
	}
	cout << newstr << endl;
}

void test_string9()
{
	string file("test.cpp");
	FILE* fout = fopen(file.c_str(), "r");
	assert(fout);
	char ch = fgetc(fout);
	while (ch != EOF)
	{
		cout << ch;
		ch = fgetc(fout);
	}
}
void test_string10()
{
	//string file("test.cpp");
	//cout << file << endl;
	//cout << file.c_str() << endl;

	//file += '\0';
	//file += "string.cpp";
	//cout << file << endl;//string以size为准
	//cout << file.c_str() << endl;//以常量字符串\0为准，停止打印

	//string copy = file;
	//cout << copy << endl;//string以size为准
	//cout << file.c_str() << endl; //以常量字符串\0为准，停止打印

	for (unsigned char ch = 0; ch < 128; ch++)
	{
		cout << ch;
	}
}
void DealUrl(const string& url)
{
	size_t pos1 = url.find("://");
	if (pos1 == string::npos)
	{
		cout << "非法url" << endl;
		return;
	}
	string protocol = url.substr(0, pos1);
	cout << protocol << endl;

	size_t pos2 = url.find('/', pos1 + 3);
	if (pos2 == string::npos)
	{
		cout << "非法url" << endl;
		return;
	}
	string domain = url.substr(pos1 + 3, pos2 - pos1 - 3);
	cout << domain << endl;

	string uri = url.substr(pos2 + 1);
	cout << uri << endl << endl;
}


void test_string11()
{
	string file("test.cpp.zip.hhh");
	//size_t pos = file.find('.');
	size_t pos = file.rfind('.');//找最后一个出现的

	if (pos != string::npos)//npos是静态成员变量
	{
		//string sub = file.substr(pos, file.size() - pos);
		string sub = file.substr(pos);

		cout << sub << endl;
	}

	string url = "https://gitee.com/";
	DealUrl(url);
}
void test_string12()
{
	int ival;
	double dval;
	cin >> ival >> dval;
	string istr = to_string(ival);
	string dstr = to_string(dval);
	cout << istr << endl;
	cout << dstr << endl;

	istr = "9999";
	dstr = "99.99";
	ival = stoi(istr);
	dval = stod(dstr);
	cout << ival << endl;
	cout << dval << endl;
}

void test_string13()
{
	string s("hello");
	reverse(s.begin(), s.end());
	cout << s << endl;
}
int main()
{
	test_string13();
	return 0;
}

//int main()
//{
//	string str;
//	//cin >> str;
//	getline(cin, str);
//	size_t pos = str.rfind(' ');
//	if (pos != string::npos)
//	{
//		cout << str.size() - pos - 1 << endl;
//
//	}
//	else
//	{
//		cout << str.size() << endl;
//
//	}
//}