#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include "BinarySearchTree.h"
using namespace std;
void TestBSTree1()
{
	BSTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13, 4, 3, 4 };
	for (auto e : a)
	{
		t.Insert(e);
	}
	//����+ȥ��
	t.InOrder();

	t.Erase(3);
	t.InOrder();

	t.Erase(8);
	t.InOrder();

	for (auto e : a)
	{
		t.Erase(e);
		t.InOrder();
	}
}
void TestBSTree2()
{
	BSTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13, 4, 3, 4 };
	for (auto e : a)
	{
		t.Insert(e);
	};
	
	BSTree<int> copy = t;
	copy.InOrder();
	t.InOrder();

	BSTree<int> t1;
	t1.Insert(2);
	t1.Insert(1);
	t1.Insert(3);

	copy = t1;
	copy.InOrder();
	t1.InOrder();
}

void TestBSTree3()
{
	string arr[] = { "�㽶", "ƻ��", "�㽶", "�㽶", "ƻ��", "ƻ��", "ƻ��" };

	KeyValue::BSTree<string, int> countTree;
	for (auto& str : arr)
	{
		auto ret = countTree.Find(str);
		if (ret)
		{
			ret->_value++;
		}
		else
		{
			countTree.Insert(str, 1);
		}
	}

	countTree.InOrder();
}
int main()
{
	TestBSTree3();
	return 0;
}