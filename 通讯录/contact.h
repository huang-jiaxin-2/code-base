#pragma once

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>

//类型声明
#define NAME_MAX 20
#define SEX_MAX 5
#define TELE_MAX 12
#define ADDR_MAX 30
#define MAX 1000
//通讯录初始状态的容量大小
#define DEFAULT_SZ 5
//枚举
enum Option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SORT,
	PRINT
};

typedef struct peoinfo
{
	char name[NAME_MAX];
	char sex[SEX_MAX];
	int age;
	char tele[TELE_MAX];
	char addr[ADDR_MAX];
} peoinfo;

//静态的版本
//typedef struct contact
//{
//	peoinfo data[MAX];//通讯录容量
//	int sz;//记录通讯录中已经存放的信息个数
//} contact;

//动态的版本
typedef struct contact
{
	peoinfo* data;//可以存放1000个人的信息
	int sz;//记录通讯中已经保存的信息个数
	int capacity;//记录通讯录当前的最大容量
} contact;

//函数声明

//初始化通讯录
void Initcontact(contact* pa);

//增加联系人的信息
void Addcontact(contact* pa);

//删除指定联系人的信息
void Delcontact(contact* pa);

//打印通讯录中的信息
void Printcontact(contact* pa);

//查找指定联系人信息
void Searchcontact(const contact* pa);

//按字典顺序排序
void Sortcontact(contact* pa);

//修改指定联系人的信息
void Modifycontact(contact* pa);

//销毁通讯录
void Destroycontact(contact* pa);

//文件版本
void Savecontact(const contact* pa);
