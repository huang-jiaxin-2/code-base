#define _CRT_SECURE_NO_WARNINGS 1

#include "contact.h"

//静态的版本
//void Initcontact(contact* pa)
//{
//	pa->sz = 0;
//	memset(pa->data, 0, sizeof(pa->data));//初始化数组
//}

//动态的版本
//void Initcontact(contact* pa)
//{
//	assert(pa);
//	pa->sz = 0;
//	pa->capacity = DEFAULT_SZ;
//	pa->data = (peoinfo*)malloc(pa->capacity * sizeof(peoinfo));
//	if (pa->data == NULL)
//	{
//		perror("Initcontact::malloc");
//		return;
//	}
//	memset(pa->data, 0, pa->capacity * sizeof(peoinfo));//初始化数组
//}

void checkcapacity(contact* pa)//检查容量
{
	if (pa->sz == pa->capacity)
	{
		peoinfo* tmp = (peoinfo*)realloc(pa->data, (pa->capacity + 3) * sizeof(peoinfo));
		if (tmp != NULL)
		{
			pa->data = tmp;
		}
		else
		{
			perror("checkcapacity::realloc");
			return;
		}
		pa->capacity += 3;
		printf("增容成功\n");
	}
}

void Loadcontact(contact* pa)
{
	//打开文件
	FILE* pf = fopen("contact.txt", "rb");
	if (pf == NULL)
	{
		perror("Loadconatct::fopen");
		return;
	}
	//读文件
	peoinfo tmp = { 0 };
	while (fread(&tmp, sizeof(peoinfo), 1, pf))
	{
		checkcapacity(pa);
		pa->data[pa->sz] = tmp;
		pa->sz++;
	}
	//关闭文件
	fclose(pf);
	pf = NULL;
}


//文件版本
void Initcontact(contact* pa)
{
	assert(pa);
	pa->sz = 0;
	pa->capacity = DEFAULT_SZ;
	pa->data = (peoinfo*)malloc(pa->capacity * sizeof(peoinfo));
	if (pa->data == NULL)
	{
		perror("Initcontact::malloc");
		return;
	}
	memset(pa->data, 0, pa->capacity * sizeof(peoinfo));//初始化数组

	//加载文件信息到通讯录中
	Loadcontact(pa);
}


void Addcontact(contact* pa)
{
	assert(pa);
	//静态的版本
	/*if (pa->sz == MAX)
	{
		printf("通讯录已满，无法添加\n");
	}*/
	//录入信息

	//动态的版本
	checkcapacity(pa);

	printf("请输入名字:>");
	scanf("%s", pa->data[pa->sz].name);
	printf("请输入年龄:>");
	scanf("%d", &pa->data[pa->sz].age);
	printf("请输入性别:>");
	scanf("%s", pa->data[pa->sz].sex);
	printf("请输入电话:>");
	scanf("%s", pa->data[pa->sz].tele);
	printf("请输入地址:>");
	scanf("%s", pa->data[pa->sz].addr);
	pa->sz++;
	printf("添加成功\n");
}

//找到了返回下标
//找不到返回-1
int FindName(const contact* pa, char name[])
{
	assert(pa);
	int i = 0;
	for (i = 0; i < pa->sz; i++)
	{
		if (0 == strcmp(pa->data[i].name, name))
		{
			return i;
		}
	}
	return -1;
}
void Delcontact(contact* pa)
{
	assert(pa);
	if (pa->sz==0)
	{
		printf("通讯录已空\n");
	}
	//1、找到
	char name[NAME_MAX] = { 0 };
	printf("请输入要删除的名字:>");
	scanf("%s", name);
	int pos = FindName(pa, name);
	if (pos == -1)
	{
		printf("要删除的人不存在\n");
		return;
	}
	//2、删除
	int j = 0;
	for (j = pos; j < pa->sz - 1; j++)
	{
		pa->data[j] = pa->data[j + 1];
	}
	pa->sz--;
	printf("删除成功\n");
}

void Printcontact(contact* pa)
{
	assert(pa);
	int i = 0;
	printf("%-10s %-5s %-5s %-12s %-20s\n", "姓名", "年龄", "性别", "电话", "地址");
	for (i = 0; i < pa->sz; i++)
	{
		printf("%-10s %-5d %-5s %-12s %-20s\n", pa->data[i].name,pa->data[i].age,pa->data[i].sex,pa->data[i].tele,pa->data[i].addr);
	}
}

void Searchcontact(const contact* pa)
{
	char name[NAME_MAX] = { 0 };
	printf("请输入要查找的名字:>");
	scanf("%s", name);
	int pos = FindName(pa, name);
	if (pos == -1)
	{
		printf("要查找的人不存在\n");
		return;
	}
	printf("%-10s %-5s %-5s %-12s %-20s\n", "姓名", "年龄", "性别", "电话", "地址");
	printf("%-10s %-5d %-5s %-12s %-20s\n", pa->data[pos].name, pa->data[pos].age, pa->data[pos].sex, pa->data[pos].tele, pa->data[pos].addr);
}
int Cmpname(const void* e1, const void* e2)
{
	return strcmp(((peoinfo*)e1)->name,((peoinfo*)e2)->name);
}
void Sortcontact(contact* pa)
{
	assert(pa);
	qsort(pa->data, pa->sz, sizeof(pa->data[0]), Cmpname);
	printf("排序成功\n");
}

void Modifycontact(contact* pa)
{
	char name[NAME_MAX] = { 0 };
	printf("请输入要修改人的名字:>");
	scanf("%s", name);
	int pos = FindName(pa, name);
	if (pos == -1)
	{
		printf("要修改的人不存在\n");
		return;
	}
	else
	{
		printf("请输入修改后的名字:>");
		scanf("%s", pa->data[pos].name);
		printf("请输入修改后的年龄:>");
		scanf("%d", &(pa->data[pos].age));
		printf("请输入修改后的性别:>");
		scanf("%s", pa->data[pos].sex);
		printf("请输入修改后的电话:>");
		scanf("%s", pa->data[pos].tele);
		printf("请输入修改后的地址:>");
		scanf("%s", pa->data[pos].addr);
		printf("修改成功！！！\n");
	}
	printf("%-10s %-5s %-5s %-12s %-20s\n", "姓名", "年龄", "性别", "电话", "地址");
	printf("%-10s %-5d %-5s %-12s %-20s\n", pa->data[pos].name, pa->data[pos].age, pa->data[pos].sex, pa->data[pos].tele, pa->data[pos].addr);
}

void Destroycontact(contact* pa)
{
	free(pa->data);
	pa->data = NULL;
	pa->capacity = 0;
	pa->sz = 0;
	printf("销毁成功\n");
}

//文件版本
void Savecontact(const contact* pa)
{
	//打开文件
	FILE* pf = fopen("contact.txt", "wb");
	if (pf == NULL)
	{
		perror("Savecontact::fopen");
		return;
	}
	//写文件
	int i = 0;
	for (i = 0; i < pa->sz; i++)
	{
		fwrite(pa->data + i, sizeof(peoinfo), 1, pf);
	}

	//关闭文件
	fclose(pf);
	pf = NULL;
}

