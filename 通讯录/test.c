#define _CRT_SECURE_NO_WARNINGS 1

#include "contact.h"

void menu()
{
	printf("****************************\n");
	printf("**** 1.增加    2.删除   ****\n");
	printf("**** 3.查找    4.修改   ****\n");
	printf("**** 5.排序    6.打印   ****\n");
	printf("**** 0.退出             ****\n");
	printf("****************************\n");
}

void test()
{
	int input = 0;
	//创建通讯录
	contact con;
	//初始化通讯录
	Initcontact(&con);
	do
	{
		menu();
		printf("请选择功能:>");
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			Addcontact(&con);
			break;
		case DEL:
			Delcontact(&con);
			break;
		case SEARCH:
			Searchcontact(&con);
			break;
		case MODIFY:
			Modifycontact(&con);
			break;
		case SORT:
			Sortcontact(&con);
			break;
		case PRINT:
			Printcontact(&con);
			break;
		case EXIT:
			Savecontact(&con);
			Destroycontact(&con);
			printf("退出通讯录\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);
}

int main()
{
	test();
	return 0;
}