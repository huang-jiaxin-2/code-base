#define _CRT_SECURE_NO_WARNINGS 1

#include "sort.h"
#include "stack.h"
#include "Queue.h"

int callCount = 0;
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

//时间复杂度：O(N^2)
void InsertSort(int* a, int n)
{
	//[0,end]有序，把end+1的值插入，保持有序
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (tmp < a[end])
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}
void ShellSort(int* a, int n)//希尔排序
{
	int gap = n;
	//gap > 1时是预排序
	// gap 最后一次等于1，是直接插入排序
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		//gap = gap / 2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}
void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}
void AdjustDown(int* a, int size, int parent)
{
	int child = parent * 2 + 1;
	while (child < size)
	{
		//选出左右孩子中小的那个
		if (child + 1 < size && a[child + 1] > a[child])//防止数组越界
		{
			child++;
		}
		//孩子和父亲比较
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
void HeapSort(int* a, int n)
{
	
	assert(a);
	//建堆方式2：o(N) 
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);//向下调整前提：左右子树必须是大/小堆
	}
	// o(N*logN)
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}

void SelectSort(int* a, int n)
{
	assert(a);
	int begin = 0, end = n - 1;
	while (begin < end)
	{
		int mini = begin, maxi = begin;
		for (int i = begin + 1; i <= end; i++)
		{
			if (a[i] < a[mini])
				mini = i;

			if (a[i] > a[maxi])
				maxi = i;
		}
		Swap(&a[begin], &a[mini]);

		//如果begin和maxi重叠，那么要修正一下maxi的位置
		if (begin == maxi)
		{
			maxi = mini;
		}

		Swap(&a[end], &a[maxi]);
		begin++;
		end--;
	}
}

void BubbleSort(int* a, int n)
{
	assert(a);

	for (int j = 0; j < n-1; j++)
	{
		int exchange = 0;
		for (int i = 1; i < n - j; i++)
		{
			if (a[i - 1] > a[i])
			{
				Swap(&a[i - 1], &a[i]);
				exchange = 1;
			}
		}
		if (exchange == 0)
		{
			break;
		}
	}
	
}

int GetMidIndex(int* a, int begin, int end)
{
	int mid = (begin + end) / 2;
	if (a[begin] < a[mid])
	{
		if (a[mid] < a[end])
		{
			return mid;
		}
		else if (a[begin] < a[end])
		{
			return end;
		}
		else
		{
			return begin;
		}
	}
	else  //a[begin] >= a[mid]
	{
		if (a[mid] > a[end])
		{
			return mid;
		}
		else if (a[begin] < a[end])
		{
			return begin;
		}
		else
		{
			return end;
		}
	}
}
//Hoare
int PartSort1(int* a, int begin, int end)
{
	int left = begin, right = end;
	int keyi = left;
	while (left < right)
	{
		//右边先走，找比key小
		while (left < right && a[right] >= a[keyi])
		{
			right--;
		}

		//左边在走，找比key大
		while (left < right && a[left] <= a[keyi])
		{
			left++;
		}

		Swap(&a[right], &a[left]);
	}
	Swap(&a[keyi], &a[left]);
	keyi = left;
	return keyi;
}

//挖坑法
int PartSort2(int* a, int begin, int end)
{
	int key = a[begin];
	int piti = begin;
	while (begin < end)
	{
		//右边找小，填到左边的坑里面去。这个位置形成新的坑
		while (begin < end && a[end] >= key)
		{
			end--;
		}

		a[piti] = a[end];
		piti = end;

		// 左边找大，填到右边的坑里面去。这个位置形成新的坑
		while (begin < end && a[begin] <= key)
		{
			begin++;
		}

		a[piti] = a[begin];
		piti = begin;
	}
	a[piti] = key;
	return piti;

}

// 前后指针法
int PartSort3(int* a, int begin, int end)
{
	int prev = begin, cur = begin + 1;
	int keyi = begin;

	// 加入三数取中的优化
	//第一个 中间 最后一个 选不是最大，也不是最小的那个
	int midi = GetMidIndex(a, begin, end);
	Swap(&a[keyi], &a[midi]);

	while (cur <= end)
	{
		// cur位置的值小于keyi位置值
		if (a[cur] < a[keyi]&&++prev!=cur)
			Swap(&a[prev], &a[cur]);
		cur++;
	}
	Swap(&a[prev], &a[keyi]);
	keyi = prev;
	return keyi;
}

void QuickSort(int* a, int begin, int end)
{
	assert(a);
	/*callCount++;
	printf("%p\n", &callCount);*/

	// 区间不存在，或者只有一个值则不需要在处理
	if (begin >= end)
	{
		return;
	}

	//小区间优化
	if (end - begin > 10)
	{
		int keyi = PartSort3(a, begin, end);

		QuickSort(a, begin, keyi - 1);
		QuickSort(a, keyi + 1, end);
	}
	else
	{
		InsertSort(a + begin, end - begin + 1);
	}
}

//递归大问题，极端场景下如果深度太深，会出现栈溢出
//解决方案 1、直接改循环——比如斐波那契数列、归并排序
//         2、用数据结构栈模拟递归过程
void QuickSortNonR(int* a, int begin, int end)
{
	ST st;
	StackInit(&st);
	StackPush(&st, end);
	StackPush(&st, begin);

	while (!StackEmpty(&st))
	{
		int left = StackTop(&st);
		StackPop(&st);

		int right = StackTop(&st);
		StackPop(&st);

		int keyi = PartSort3(a, left, right);
		// [left, keyi-1] keyi[keyi+1, right]
		//栈里面的区间拿出来，单趟排序分割，子区间再入栈

		if (keyi + 1 < right)
		{
			StackPush(&st, right);
			StackPush(&st, keyi + 1);
		}

		if (left < keyi - 1)
		{
			StackPush(&st, keyi - 1);
			StackPush(&st, left);
		}
	}

	StackDestroy(&st);
}

//时间复杂度 O(N*logN)
//空间复杂度 O(N)
void MergeSort1(int* a, int begin, int end, int* tmp)
{
	if (begin >= end)
		return;

	int mid = (begin + end) / 2;

	//分治递归，让子区间有序
	MergeSort1(a, begin, mid, tmp);
	MergeSort1(a, mid+1, end, tmp);

	//归并
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int i = begin1;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
		{
			tmp[i++] = a[begin1++];
		}
		else
		{
			tmp[i++] = a[begin2++];
		}
	}

	//如果其中一个还有数据
	while (begin1 <= end1)
	{
		tmp[i++] = a[begin1++];
	}

	while (begin2 <= end2)
	{
		tmp[i++] = a[begin2++];
	}

	//把数据拷贝回原数组
	memcpy(a + begin, tmp + begin, (end - begin + 1) * sizeof(int));

}
void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		printf("malloc fail\n");
		exit(-1);
	}

	MergeSort1(a, 0, n - 1, tmp);
	free(tmp);
}

//非递归 归并排序1
//void MergeSortNonR(int* a, int n)
//{
//	int* tmp = (int*)malloc(sizeof(int) * n);
//	if (tmp == NULL)
//	{
//		printf("malloc fail\n");
//		exit(-1);
//	}
//
//	int gap = 1;
//	while (gap < n)
//	{
//		for (int j = 0; j < n; j += 2 * gap)
//		{
//			int begin1 = j, end1 = j + gap - 1;
//			int begin2 = j + gap, end2 = j + 2 * gap - 1;
//			
//			//如果越界——修正边界
//			if (end1 >= n)
//			{
//				end1 = n - 1;
//				//[begin2,end2]修正为不存在的区间
//				begin2 = n;
//				end2 = n - 1;
//			}
//			else if (begin2 >= n)
//			{
//				begin2 = n;
//				end2 = n - 1;
//			}
//			else if (end2 >= n)
//			{
//				end2 = n - 1;
//			}
//
//			int i = begin1;
//			while (begin1 <= end1 && begin2 <= end2)
//			{
//				if (a[begin1] < a[begin2])
//				{
//					tmp[i++] = a[begin1++];
//				}
//				else
//				{
//					tmp[i++] = a[begin2++];
//				}
//			}
//
//			//如果其中一个还有数据
//			while (begin1 <= end1)
//			{
//				tmp[i++] = a[begin1++];
//			}
//
//			while (begin2 <= end2)
//			{
//				tmp[i++] = a[begin2++];
//			}
//		}
//		memcpy(a, tmp, sizeof(int) * n);
//		gap *= 2;
//	}
//	
//	free(tmp);
//}

//非递归 归并排序2
void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		printf("tmp malloc fail\n");
		exit(-1);
	}

	int gap = 1;
	while (gap < n)
	{
		for (int j = 0; j < n; j += 2 * gap)
		{
			int begin1 = j, end1 = j + gap - 1;
			int begin2 = j + gap, end2 = j + 2 * gap - 1;

			//end1越界或者begin2越界可以不归并
			if (end1 >= n || begin2 >= n)
			{
				break;
			}
			else if (end2 >= n)
			{
				end2 = n - 1;
			}
			
			int m = end2 - begin1 + 1;
			int i = begin1;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[i++] = a[begin1++];
				}
				else
				{
					tmp[i++] = a[begin2++];
				}
			}

			//如果其中一个还有数据
			while (begin1 <= end1)
			{
				tmp[i++] = a[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[i++] = a[begin2++];
			}
			memcpy(a+j, tmp+j, sizeof(int) * m);
		}
		gap *= 2;
	}

	free(tmp);
}

//时间复杂度：O(max(range,N))
//空间复杂度：O(range)
void CountSort(int* a, int n)
{
	int min = a[0], max = a[0];
	for (int i = 0; i < n; i++)
	{
		if (a[i] < min)
		{
			min = a[i];
		}
		if (a[i] > max)
		{
			max = a[i];
		}
	}

	//统计次数的数组
	int range = max - min + 1;
	int* count = (int*)malloc(sizeof(int)*range);
	if (count == NULL)
	{
		printf("count malloc fail");
		exit(-1);
	}
	memset(count, 0, sizeof(int) * range);

	//统计次数
	for (int i = 0; i < n; i++)
	{
		count[a[i] - min]++;
	}

	//回写排序
	int j = 0;
	for (int i = 0; i < range; i++)
	{
		//出现几次就回写几个i+min
		while (count[i]--)
		{
			a[j++] = i + min;
		}
	}
}

//Queue q0;
//Queue q1;
//Queue q2;
//Queue q3;
//Queue q4;
//Queue q5;
//Queue q6;
//Queue q7;
//Queue q8;
//Queue q9;
//
//int GetKey(int value,int k)
//{
//	int key = 0;
//	while (k >= 0)
//	{
//		key = value % 10;
//		value /= 10;
//		k--;
//	}
//	return key;
//}
//
//void Distribute(int* a, int left, int right, int k)
//{
//	for (int i = left; i < right; i++)
//	{
//		int key = GetKey(a[i], k);
//		if (key == 0)
//			QueuePush(&q0, a[i]);
//		if (key == 1)
//			QueuePush(&q1, a[i]);
//		if (key == 2)
//			QueuePush(&q2, a[i]);
//		if (key == 3)
//			QueuePush(&q3, a[i]); 
//		if (key == 4)
//			QueuePush(&q4, a[i]);
//		if (key == 5)
//			QueuePush(&q5, a[i]); 
//		if (key == 6)
//			QueuePush(&q6, a[i]); 
//		if (key == 7)
//			QueuePush(&q7, a[i]); 
//		if (key == 8)
//			QueuePush(&q8, a[i]); 
//		if (key == 9)
//			QueuePush(&q9, a[i]);
//	}
//}
//void Collect(int* a)
//{
//	int k = 0;
//	for (int i = 0; i < RADIX; i++)
//	{
//		while (!QueueEmpty(&q0))
//		{
//			a[k++] = QueueFront(&q0);
//			QueuePop(&q0);
//		}
//		while (!QueueEmpty(&q1))
//		{
//			a[k++] = QueueFront(&q1);
//			QueuePop(&q1);
//		}while (!QueueEmpty(&q2))
//		{
//			a[k++] = QueueFront(&q2);
//			QueuePop(&q2);
//		}while (!QueueEmpty(&q3))
//		{
//			a[k++] = QueueFront(&q3);
//			QueuePop(&q3);
//		}while (!QueueEmpty(&q4))
//		{
//			a[k++] = QueueFront(&q4);
//			QueuePop(&q4);
//		}while (!QueueEmpty(&q5))
//		{
//			a[k++] = QueueFront(&q5);
//			QueuePop(&q5);
//		}while (!QueueEmpty(&q6))
//		{
//			a[k++] = QueueFront(&q6);
//			QueuePop(&q6);
//		}while (!QueueEmpty(&q7))
//		{
//			a[k++] = QueueFront(&q7);
//			QueuePop(&q7);
//		}while (!QueueEmpty(&q8))
//		{
//			a[k++] = QueueFront(&q8);
//			QueuePop(&q8);
//		}while (!QueueEmpty(&q9))
//		{
//			a[k++] = QueueFront(&q9);
//			QueuePop(&q9);
//		}
//	}
//}
//void RadixSort(int* a, int left, int right)
//{
//	for (int i = 0; i < K; i++)
//	{
//		//回收数据
//		Distribute(a, left, right, i);
//		//分发数据
//		Collect(a);
//	}
//}

Queue q[10];
int GetKey(int value, int k)
{
	int key = 0;
	while (k >= 0)
	{
		key = value % 10;
		value /= 10;
		k--;
	}
	return key;
}

void Distribute(int* a, int left, int right, int k)
{
	for (int i = left; i < right; i++)
	{
		int key = GetKey(a[i], k);
		QueuePush(&q[key], a[i]);
	}
}



void Collect(int* a)
{
	int k = 0;
	for (int i = 0; i < RADIX; i++)
	{
		while (!QueueEmpty(&q[i]))
		{
			a[k++] = QueueFront(&q[i]);
			QueuePop(&q[i]);
		}
	}
}

void RadixSort(int* a, int left, int right)
{
	for (int i = 0; i < K; i++)
	{
		//回收数据
		Distribute(a, left, right, i);
		//分发数据
		Collect(a);
	}
}
