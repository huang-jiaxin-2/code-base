#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define K 3
#define RADIX 10

void PrintArray(int* a, int n);

void InsertSort(int* a, int n);//ֱ�Ӳ�������
void ShellSort(int* a, int n);//ϣ������
void HeapSort(int* a, int n);//������
void SelectSort(int* a, int n);//ѡ������
void BubbleSort(int* a, int n);//ð������
void QuickSort(int* a, int begin, int end);//��������
void QuickSortNonR(int* a, int begin, int end);//�ǵݹ��������
void MergeSort(int* a, int n);//�鲢����
void MergeSortNonR(int* a, int n);//�ǵݹ� �鲢����
void CountSort(int* a, int n);//��������
void RadixSort(int* a, int left, int right);//��������

extern int callCount;