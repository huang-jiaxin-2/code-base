#define _CRT_SECURE_NO_WARNINGS 1
#include "stack.h"

void TestStack()
{
	ST st;
	StackInit(&st);
	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	printf("%d ", StackTop(&st));
	printf("\n");
	StackPush(&st, 4);
	StackPush(&st, 5);
	StackPush(&st, 6);
	StackPop(&st);
	while (!StackEmpty(&st))
	{
		printf("%d ", StackTop(&st));
		StackPop(&st);
	}
	printf("\n");
	StackDestroy(&st);
}
int main()
{
	TestStack();
	return 0;
}