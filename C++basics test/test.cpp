#define _CRT_SECURE_NO_WARNINGS 1

#include "test.h"

//using std::cout;
//using std::endl;
//using std::cin;
//int main()
//{
//	int i;
//	double d;
//	cin >> i >> d;
//	cout << i << d << endl;
//	return 0;
//}

//namespace code
//{
//	int rand = 0;
//
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//}
//namespace code
//{
//	int a = 0;
//	int b = 0;
//}
//
//using code::a;
//
//using namespace code;
//int main()
//{
//	printf("%d\n", rand);
//	return 0;
//}

//int main()
//{
//	int ret = 1;
//	cout << (ret + '0') << endl;
//	return 0;
//}

//int main()
//{
//    int n = 0;
//    int ret = 0;
//    string s1;
//    string s2;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> s1;
//    }
//    if (s1.size() > 10)
//    {
//        s2.push_back(s1[0]);
//    }
//    ret = s1.size() - 1;
//    s2.push_back(ret + '0');
//    s2.push_back(s1[s1.size() - 1]);
//    cout << s2 << endl;
//    return 0;
//}

//void Test1(int a, int b = 2, int c = 3)
//{
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl;
//}
//int main()
//{
//	Test1(10);//至少传一个
//	Test1(10, 20);//传两个
//	Test1(10, 20, 30);//都传
//	return 0;
//}

//缺省参数不能在函数声明和定义中同时出现
//void Test1(int a = 2)
//{
//	cout << a << endl;
//}
//int main()
//{
//	Test1();
//	return 0;
//}

//函数重载
//int Add(int a, int b)
//{
//	return a + b;
//}
//double Add(double c, double d)
//{
//	return c + d;
//}
//int main()
//{
//	cout << Add(1, 2) << endl;
//	cout << Add(1.1, 2.2) << endl;
//	return 0;
//}

//void print(int i, char c)
//{
//	cout << "print(int i, char c)" << endl;
//}
//void print(char c, int i)
//{
//	cout << "print(char c, int i)" << endl;
//}
//int main()
//{
//	print(1, 'a');
//	print('a', 1);
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int& b = a;//引用
//	cout << &b << endl;//取地址
//	cout << &a << endl;
//	a++;
//	cout << a << endl;
//	b++;
//	cout << b << endl;
//	return 0;
//}

//int main()
//{
//	int a = 1;
//	int x = 10;
//	int& b = a;
//	b = x;
//	cout << a << endl;
//	cout << &a << endl;
//
//	cout << b << endl;
//	cout << &b << endl;
//
//	cout << x << endl;
//	cout << &x << endl;
//
//	return 0;
//}
//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	Swap(a, b);
//	return 0;
//}

//#include <time.h>
//struct A { int a[10000]; };
//void TestFunc1(A a) {}
//void TestFunc2(A& a) {}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestRefAndValue();
//	return 0;
//}

//int& count()
//{
//    int n = 0;
//	n++;
//	return n;
//}
//int main()
//{
//	int& ret = count();
//	cout << ret << endl;
//	cout << ret << endl;
//	cout << ret << endl;
//
//	return 0;
//}

//值和引用作为返回值类型的性能比较
//#include <time.h>
//struct A { int a[10000]; };
//A a;
//// 值返回
//A Test1() { return a; }
//// 引用返回
//A& Test2() { return a; }
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		Test1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		Test2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "Test1 time:" << end1 - begin1 << endl;
//	cout << "Test2 time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestReturnByRefOrValue();
//	return 0;
//}

//int main()
//{
//	//权限的平移
//	int a = 10;
//	int& b = a;
//
//	//权限的放大
//	const int c = 10;
//	//int& d = c;//编译报错 
//
//	//权限的缩小
//	int i = 10;
//	const int& j = i;
//
//	int ii = 10;
//	double dd = ii;
//	//double& dd = ii;//编译器报错
//	const double& dd = ii;
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int& b = a;
//	b = 20;
//
//	int* pa = &a;
//	*pa = 20;
//	return 0;
//}

//#define Add(x, y) ((y)+(y))
//int main()
//{
//	int x = 1;
//	int y = 2;
//	int ret = Add(x, y);
//	cout << ret << endl;
//	return 0;
//}

#include <stdio.h>
int main()
{
	char* str[3] = { "stra", "strb", "strc" };
	char* p = str[0];
	int i = 0;
	while (i < 3)
	{
		printf("%s ", p++);
		i++;
	}
	return 0;
}
