#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//设计一个类只能在堆上开辟空间
//1.将构造函数和拷贝构造声明成私有
//2.提供一个静态成员函数，在该函数内完成堆对象的创建
class HeapOnly
{
public:
	static HeapOnly* CreateObject()
	{
		return new HeapOnly;
	}

	HeapOnly(const HeapOnly& hp) = delete;
	HeapOnly& operator=(const HeapOnly& hp) = delete;
private:
	HeapOnly() 
	{}
	
	
};

//设计一个类只能在栈上开辟空间
//1.构造函数私有
//2.提供一个静态成员函数，完成在栈上创建对象
//3.屏蔽new
class StackOnly
{
public:
	static StackOnly CreateObject()
	{
		StackOnly st;
		return st;
	}

	/*StackOnly(const StackOnly& hp) = delete;
	StackOnly& operator=(const StackOnly& hp) = delete;*/
	void* operator new(size_t size) = delete; 
	void operator delete(void* p) = delete;
private:
	StackOnly()
	{

	}
};

//int main()
//{
//	/*HeapOnly* hp = HeapOnly::CreateObject();
//	delete hp;*/
//
//	StackOnly st1 = StackOnly::CreateObject();
//	//StackOnly st2 = new StackOnly(st1);
//	return 0;
//}

//设计一个类，只能创建一个对象（单例模式）
//1、饿汉模式 -- 一开始（main函数之前）就创建对象
//class Singleton
//{
//public:
//	static Singleton* GetInstance()
//	{
//		return _pinst;
//	}
//	void* Alloc(size_t n)
//	{
//		void* ptr = nullptr;
//		// ....
//		return ptr;
//	}
//		
//	void DelAlloc(void* ptr)
//	{
//		// ...
//	}
//private:
//	//构造函数私有化
//	Singleton()
//	{}
//
//	char* ptr = nullptr;
//	//static Singleton _inst;
//	static Singleton* _pinst;//声明
//};
//
////Singleton Singleton::_inst;
//Singleton* Singleton::_pinst = new Singleton;//定义
//////////////////////////////////////////////////////////////////

//2、懒汉模式 -- 第一次使用对象再创建实例对象
class Singleton
{
public:
	static Singleton* GetInstance()
	{
		if (_pinst == nullptr)
		{
			_pinst = new Singleton;
		}
		return _pinst;
	}
	void* Alloc(size_t n)
	{
		void* ptr = nullptr;
		// ....
		return ptr;
	}

	void DelAlloc(void* ptr)
	{
		// ...
	}
private:
	//构造函数私有化
	Singleton()
	{}

	char* ptr = nullptr;
	//static Singleton _inst;
	static Singleton* _pinst;//声明
};

//Singleton Singleton::_inst;
Singleton* Singleton::_pinst = nullptr;//定义

int main()
{
	void* ptr1 = Singleton::GetInstance()->Alloc(10);
	Singleton::GetInstance()->DelAlloc(ptr1);
	return 0;
}