#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
//class A
//{
//
//};
//class B : public A
//{
//
//};
//class Person {
//public:
//	virtual A* BuyTicket() {
//		cout << "买票-全价" << endl;
//		
//	}
//};
//class Student : public Person {
//public:
//	//虚函数重写/覆盖——三同：返回值相同。函数名相同、参数列表相同
//	//不符合重写条件就是隐藏关系
//	//特例1：子类虚函数不加virtual,依旧构成重写（实际最好加上）
//	//特例2：重写的协变，返回值可以不同，但要求返回值必须是父子关系的指针或者引用 一也可以是其它父子关系 父类必须是父类指针 子类必须是子类指针
//	 B* BuyTicket() { 
//		cout << "买票-半价" << endl;
//		
//	}
//};
//class Soldier : public Person {
//public:
//	virtual void BuyTicket() { cout << "优先买票" << endl; }
//};
//
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
////构成多态的条件：1、虚函数重写  2、父类指针或引用去调用虚函数 （缺一不可）
//int main()
//{
//	Person ps;
//	Student st;
//	//Soldier sd;
//	Func(ps);
//	Func(st);
//	//Func(sd);
//	return 0;
//}

//class A
//{
//public:
//	virtual void func(int val = 1){ std::cout << "A->" << val << std::endl; }
//	virtual void test(){ func(); }
//};
//
//class B : public A
//{
//public:
//	//void func(int val = 0){ std::cout << "B->" << val << std::endl; }
//	//虚函数重写是接口继承，重写实现
//	void func(int val){ std::cout << "B->" << val << std::endl; }
//};
//
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	return 0;
//}


//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//
//class B : public A
//{
//public:
//	//void func(int val = 0){ std::cout << "B->" << val << std::endl; }
//	//虚函数重写是接口继承，重写实现
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//
//int main(int argc, char* argv[])
//{
//	A* p = new B;
//	p->test();
//	return 0;
//}

//class A
//{
//public:
//	virtual void Func()
//	{
//		cout << "Func()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//int main()
//{
//	cout << sizeof(A) << endl;//虚函数表--虚表，虚函数会把地址放到虚函数表中，虚表的本质是一个数组——函数指针数组
//	A a;
//	return 0;
//}
//class A
//{
//public:
//	int _a;
//private:
//
//};
//int main()
//{
//
//	return 0;
//}

//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	//virtual void BuyTicket() { cout << "买票-半价" << endl; }
//};
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//int main()
//{
//	Person ps;
//	Student st;
//
//	Func(ps);
//	Func(st);
//	return 0;
//}

//class Person
//{
//public:
//	virtual ~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual ~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//};
//
//int main()
//{
//	/*Person p;
//	Student s;*/
//
//	Person* p1 = new Person;
//	delete p1;
//	Person* p2 = new Student;
//	delete p2;
//	return 0;
//}

//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; } //final修饰一个类，这个类不能被继承，修饰一个虚函数，这个虚函数不能被重写
//};

//class Car {
//public:
//	virtual void Drive() {}
//};
//class Benz :public Car {
//public:
//	//override检查子类虚函数是否完成重写
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};
//
//int main()
//{
//	int a = 0;
//	return 0;
//}

//抽象类--强制要求重写
//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//
//class BYD : public Car
//{
//public:
//
//};
//int main()
//{
//	//Car c;//不能示例化对象
//	//BYD b;
//	return 0;
//}


//class Person
//{
//public:
// 	virtual void BuyTicket() { cout << " Person买票—全价" << endl; }
//
//	virtual void Func1() {
//		cout << " Person::Func1()" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual void BuyTicket() { cout << " Student买票—半价" << endl; }
//
//	virtual void Func2() {
//		cout << " Student::Func2()" << endl;
//	}
//};
//
//class Base1 {
//public:
//	virtual void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1() { cout << "Base2::func1" << endl; }
//	virtual void func2() { cout << "Base2::func2" << endl; }
//private:
//	int b2;
//};
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//typedef void(*VFPTR)();//函数指针
//
//void Print(VFPTR* table)
//{
//	for (size_t i = 0; table[i] != nullptr; ++i)
//	{
//		printf("vft[%d]:%p->", i, table[i]);
//		VFPTR pf = table[i];
// 		pf();
//	}
//	cout << endl;
//}
//int main()
//{
//	//同一类型的对象共用一个虚表
//	//Person p1;
//	//Person p2;
//
//	////vs下 不管是否完成重写子类虚表跟父类都不一样
//	//Student s1;
//	//Student s2;
//	////Print((VFPTR*)*(int*)&p1);
//
//	//Print((VFPTR*)*(int*)&s1,3);
//	//Print((VFPTR*)*(int*)&p1,2);
//	Derive d;
//	//cout << sizeof(d) << endl;
//	//Print((VFPTR*)*(int*)&d);
//	////Print((VFPTR*)*(int*)((char*)&d + sizeof(Base1)));
//
//	//Base2* ptr2 = &d;
//	//Print((VFPTR*)*(int*)(ptr2));
//
//	Base1* ptr1 = &d;
//	ptr1->func1();
//
//	Base2* ptr2 = &d;
//	ptr2->func1();
//
//	cout << &Derive::func1 << endl;
//	return 0;
//}

//struct A
//{
//	int a;
//	float c;
//	char b;
//	
//};
//
//int main()
//{
//	cout << sizeof(A) << endl;
//}