#include <iostream>
#include <list>
#include <unordered_map>
using namespace std;

class LRUCache {
public:
    LRUCache(int capacity) :_capacity(capacity)
    {

    }

    int get(int key) {
        auto ret = _hashMap.find(key);
        if (ret != _hashMap.end())
        {
            list<pair<int, int>>::iterator it = ret->second;
            _LRUList.splice(_LRUList.begin(), _LRUList, it);
            return it->second;
        }
        else
        {
            return -1;
        }
    }

    void put(int key, int value) {
        //1.更新  2.新增
        auto ret = _hashMap.find(key);
        if (ret == _hashMap.end())
        {
            //满了,先删除LRU的数据
            if (_capacity == _hashMap.size())
            {
                pair<int, int> back = _LRUList.back();
                _hashMap.erase(back.first);
                _LRUList.pop_back();
            }
            _LRUList.push_front(make_pair(key, value));
            _hashMap[key] = _LRUList.begin();
        }
        else
        {
            list<pair<int, int>>::iterator it = ret->second;
            it->second = value;
            _LRUList.splice(_LRUList.begin(), _LRUList, it);
        }
    }
private:
    unordered_map<int, list<pair<int, int>>::iterator> _hashMap;
    list<pair<int, int>> _LRUList;
    size_t _capacity;
};



