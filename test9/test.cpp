#include <iostream>
#include <thread>
#include <queue>
#include <vector>
#include <Windows.h>
#include <condition_variable>
using namespace std;

queue<int> res;
int b = 10;

void func1()
{
	for (int i = 0; i < 10; i++)
	{
		res.push(i);
		Sleep(1);
	}
}


void func2()
{
	while (!res.empty())
	{
		int temp = res.front();
		int sum = temp + 10;
		cout << sum << endl;
		res.pop();
	}
	
}

int main()
{
	
	thread t1(func1);
	thread t2(func2);

	t1.join();
	t2.join();

	//int* p = (int*)malloc(0);
	return 0;
}