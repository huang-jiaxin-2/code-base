#define _CRT_SECURE_NO_WARNINGS 1

#include "SeqList.h"
//void TestSeqList1()
//{
//	SL sl;
//	SeqListInit(&sl);
//	SLPushBack(&sl, 1);
//	SLPushBack(&sl, 2);
//	SLPushBack(&sl, 3);
//	SLPushBack(&sl, 4);
//	SLPrint(&sl);
//
//	SLPushBack(&sl, 5);
//	SLPrint(&sl);
//
//	SLPushBack(&sl, 6);
//	SLPrint(&sl);
//
//	SLPushBack(&sl, 7);
//	SLPrint(&sl);
//
//	SLPopBack(&sl);
//	SLPrint(&sl);
//
//	SLPopBack(&sl);
//	SLPrint(&sl);
//}
//void TestSeqList2()
//{
//	SL sl;
//	SeqListInit(&sl);
//	SLPushFront(&sl, 1);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 3);
//	SLPushFront(&sl, 4);
//	SLPushFront(&sl, 5);
//	SLPrint(&sl);
//
//	SLPushBack(&sl, 5);
//	SLPushBack(&sl, 5);
//	SLPrint(&sl);
//}
//void TestSeqList3()
//{
//	SL sl;
//	//SL* psl = NULL;
//	SeqListInit(&sl);
//	SLPushFront(&sl, 1);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 3);
//	SLPushFront(&sl, 4);
//	SLPushFront(&sl, 5);
//	SLPrint(&sl);
//
//	SLPopBack(&sl);
//	SLPrint(&sl);
//
//	SLPopFront(&sl);
//	SLPopFront(&sl);
//	SLPrint(&sl);
//}
//void TestSeqList4()
//{
//	SL sl;
//	SeqListInit(&sl);
//	SLPushFront(&sl, 1);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 3);
//	SLPushFront(&sl, 4);
//	SLPushFront(&sl, 5);
//	SLPrint(&sl);
//
//	SLPushBack(&sl, 2);
//	SLPrint(&sl);
//
//	SLInsert(&sl, 3, 400);
//	SLPrint(&sl);
//
//}
//void TestSeqList5()
//{
//	SL sl;
//	SeqListInit(&sl);
//	SLPushFront(&sl, 1);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 3);
//	SLPushFront(&sl, 4);
//	SLPushFront(&sl, 5);
//	SLPrint(&sl);
//
//	SLErase(&sl, 0);
//	SLPrint(&sl);
//
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	SLPopFront(&sl);
//	SLPrint(&sl);
//
//}
//void TestSeqList6()
//{
//	SL sl;
//	SeqListInit(&sl);
//	SLPushFront(&sl, 1);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 3);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 2);
//	SLPushFront(&sl, 4);
//	SLPushFront(&sl, 5);
//	SLPrint(&sl);
//
//	/*int x;
//	printf("请输入你要删除的值:");
//	scanf("%d", &x);
//	int pos = SLFind(&sl, x);
//	if (pos != -1)
//	{
//		SLErase(&sl, pos);
//	}
//	else
//	{
//		printf("对不起，没找到：%d\n", x);
//	}
//	SLPrint(&sl);*/
//
//	int x;
//	printf("请输入你要删除的值:");
//	scanf("%d", &x);
//	int pos = SLFind(&sl, x);
//	while (pos != -1)
//	{
//		SLErase(&sl, pos);
//		pos = SLFind(&sl, x);
//	}
//	SLPrint(&sl); 
//
//	int y,z;
//	printf("请输入你要修改的值和修改后的值:");
//	scanf("%d%d", &y,&z);
//    pos = SLFind(&sl, y);
//	if (pos != -1)
//	{
//		SLModify(&sl, pos,z);
//	}
//	else
//	{
//		printf("对不起，没找到：%d\n", y);
//	}
//	SLPrint(&sl);
//
//
//}
//
//
//int main()
//{
//	TestSeqList1();
//	return 0;
//}
void menu()
{
	printf("******************************\n");
	printf("*****  1.尾插    2.头插  *****\n");
	printf("*****  3.尾删    4.头删  *****\n");
	printf("*****  5.删除    6.插入  *****\n");
	printf("*****  7.修改    8.查找  *****\n");
	printf("*****  9.打印    0.退出  *****\n");
	printf("******************************\n");
}
int main()
{
	SL sl;
	SeqListInit(&sl);
	SLPushFront(&sl, 1);
	SLPushFront(&sl, 2);
	SLPushFront(&sl, 3);
	SLPushFront(&sl, 2);
	SLPushFront(&sl, 2);
	SLPushFront(&sl, 4);
	SLPushFront(&sl, 5);
	int option = 0;
	do
	{
		menu();
		scanf("%d", &option);
		
		int x, y, z;
		int val, pos;
		switch (option)
		{
		case 1:
			printf("请连续输入你要尾插的数据，以-1结束:>");
			scanf("%d", &val);
			while (val != -1)
			{
				SLPushBack(&sl, val);
				scanf("%d", &val);
			}
			break;
		case 2:
			printf("请输入要头插的数据，以-1结束：>");
			scanf("%d", &val);
			while (val != -1)
			{
				SLPushFront(&sl, val);
				scanf("%d", &val);
			}
			break;
		case 3:
			SLPopBack(&sl);
			printf("尾删成功\n");
			break;
		case 4:
			SLPopFront(&sl);
			printf("头删成功\n");
			break;
		case 5:
			printf("请输入你要删除的值:");
			scanf("%d", &x);
			int pos = SLFind(&sl, x);
			while (pos != -1)
			{
				SLErase(&sl, pos);
				pos = SLFind(&sl, x);
			}
			printf("删除成功\n");
			break;
		case 6:
			printf("请连续输入你要插入的数据和位置:>");
			scanf("%d%d", &val,&pos);
			SLInsert(&sl, pos, val);
			SLPrint(&sl);
			break;
		case 7:
			printf("请输入你要修改的值和修改后的值:");
			scanf("%d%d", &y, &z);
			pos = SLFind(&sl, y);
			if (pos != -1)
			{
				SLModify(&sl, pos, z);
			}
			else
			{
				printf("没找到:%d\n", y);
			}
			break;
		case 8:
			printf("请输入你要查找的值:");
			scanf("%d", &y);
			pos = SLFind(&sl, y);
			if (pos == -1)
			{
				printf("找不到\n");
			}
			else
			{
				printf("%d\n", pos);
			}
			break;
		case 9:
			SLPrint(&sl);
			break;
		default:
			printf("已退出\n");
			break;
		}
	} while (option != 0);
	SLDestory(&sl);
	return 0;
}
