#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//#define N 200
//typedef int SLDataType;
//struct SeqList
//{
//	SLDataType a[N];
//	int size;
//};//静态顺序表
//  //存在问题N太小，可能不够用，N太大，可能浪费空间
typedef int SLDataType;


typedef struct SeqList
{
	SLDataType* a;//指向动态数组指针
	int size;//数据个数
	int capacity;//容量-空间大小
}SL;

//顺序表实现增删查改

void SeqListInit(SL* ps);//顺序表初始化

void SLPrint(SL* ps);//打印数据

void SLCheckCapacity(SL* ps);//检查容量
//时间复杂度O(1)
void SLPushBack(SL* ps, SLDataType x);//尾插
void SLPopBack(SL* ps);//尾删
//时间复杂度O(n)
void SLPushFront(SL* ps, SLDataType x);//头插
void SLPopFront(SL* ps);//头删

void SLInsert(SL* ps, int pos, SLDataType x);//任意位置插

void SLErase(SL* ps, int pos);//任意位置删

int SLFind(SL* ps, SLDataType x);//查找

void SLModify(SL* ps, int pos, SLDataType x);//修改

void SLDestory(SL* ps);//销毁
