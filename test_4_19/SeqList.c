#define _CRT_SECURE_NO_WARNINGS 1

#include "SeqList.h"
void SeqListInit(SL* ps)
{
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

void SLPrint(SL* ps)
{
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SLCheckCapacity(SL* ps)
{
	//检查容量空间，满了就扩容
	if (ps->size == ps->capacity)
	{
		int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SLDataType* tmp = (SLDataType*)realloc(ps->a, newCapacity * sizeof(SLDataType));
		if (tmp == NULL)
		{
			printf("realloc fail\n");
			exit(-1);//结束程序
		}
		ps->a = tmp;
		ps->capacity = newCapacity;
		//printf("扩容成功!\n");
	}
}

void SLErase(SL* ps, int pos)//任意位置删
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);//不用等于size

	/*int begin = pos;
	while (begin < ps->size - 1)
	{
		ps->a[begin] = ps->a[begin + 1];
		begin++;
	}
	ps->size--;*/

	int begin = pos + 1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		begin++;
	}
	ps->size--;
}

void SLInsert(SL* ps, int pos, SLDataType x)//任意位置插
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	SLCheckCapacity(ps);
	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		end--;
	}
	ps->a[pos] = x;
	ps->size++;
}

//尾插
void SLPushBack(SL* ps, SLDataType x)
{
	//1
	/*SLCheckCapacity(ps);
	ps->a[ps->size] = x;
	ps->size++;*/
	//2
	//直接调用
	SLInsert(ps, ps->size, x);
}

//尾删
void SLPopBack(SL* ps)
{
	//assert(ps);
	//ps->a[ps->size - 1] = 0;
	// 温柔检查
	//if (ps->size == 0)
	//{
	//	printf("SepList is empty\n");
	//	//exit(-1);
	//	return;
	//}
	// 暴力检查
	/*assert(ps->size>0);
	ps->size--;*/
	SLErase(ps, ps->size - 1);
}

//头插
void SLPushFront(SL* ps, SLDataType x)
{
	//SLCheckCapacity(ps);
	////挪动数据
	//int end=ps->size-1;
	//while (end >= 0)
	//{
	//	ps->a[end + 1] = ps->a[end];
	//	--end;
	//}
	//ps->a[0] = x;
	//ps->size++;
	SLInsert(ps, 0, x);
}

//头删
void SLPopFront(SL* ps)
{
	/*assert(ps);
	assert(ps->size > 0);
	int begin = 1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		begin++;
	}
	ps->size--;*/
	SLErase(ps, 0);
}


int SLFind(SL* ps, SLDataType x)//查找
{
	assert(ps);
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		if (ps->a[i] == x)
		{
			return i;//返回下标
		}
	}
	return -1;
}

void SLModify(SL* ps, int pos, SLDataType x)//修改
{
	assert(ps);
	assert(pos > 0 && pos < ps->size);

	ps->a[pos] = x;
}

void SLDestory(SL* ps)//销毁
{
	assert(ps);
	if (ps->a)
	{
		free(ps->a);
		ps->a = NULL;
		ps->capacity = ps->size = 0;
	}
}