#include <iostream>
using namespace std;

//条款9:绝不在构造和析构过程中调用virtual函数
//class A
//{
//public:
//	A()
//	{
//		Animal();
//	}
//
//	virtual void Animal()const
//	{
//		cout << "喵~" << endl;
//	}
//};
//
//class B : public A
//{
//public:
//	B()
//	{
//		Animal();
//	}
//	virtual void Animal()const
//	{
//		cout << "旺~" << endl;
//	}
//};
//
//int main()
//{
//	B b;
//	//b.Animal();
//	return 0;
//}

//内存泄漏问题
//int main()
//{
//	char* p = new char[1024 * 1024 * 1024];
//	cout << (void*)p << endl;
//	return 0;
//}

//只能在堆上创建对象
//class HeapOnly
//{
//public:
//	/*static void Delete(HeapOnly* p)
//	{
//		delete p;
//	}*/
//
//	void Delete()
//	{
//		delete this;
//	}
//
//private:
//	~HeapOnly() {
//		cout << "~HeapOnly()" << endl;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	//HeapOnly hp1;
//	HeapOnly* ptr = new HeapOnly;
//	ptr->Delete();
//	return 0;
//}

//class HeapOnly
//{
//public:
//	static HeapOnly* CreateObj()
//	{
//		return new HeapOnly;
//	}
//
//	HeapOnly(const HeapOnly& hp) = delete;
//	HeapOnly& operator=(const HeapOnly& hp) = delete;
//private:
//	//构造函数私有
//	HeapOnly()
//		:_a(0)
//	{
//
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	//HeapOnly hp1;
//	return 0;
//}

//只能在栈上创建对象
//class StackOnly
//{
//public:
//	static StackOnly CreateObj()
//	{
//		StackOnly st;
//		return st;
//	}
//
//	void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;
//private:
//	//构造函数私有
//	StackOnly()
//		:_a(0)
//	{}
//private:
//	int _a;
//};
// 
//int main()
//{
//	StackOnly s = StackOnly::CreateObj();
//	return 0;
//}

//饿汉模式 — 一开始(main函数之前)就创建出对象
//优点：简单、没有线程安全问题
//缺点：1、如果一个程序中有多个单例，并且有先后创建初始化顺序要求时，饿汉无法控制
//      2、饿汉单列类，初始化时任务多会影响程序启动顺序
//class Singleton
//{
//public:
//	static Singleton* GetInstance()
//	{
//		return &m_instance;
//	}
//
//	//C++11 防拷贝
//	Singleton(Singleton const&) = delete;
//	Singleton& operator=(Singleton const&) = delete;
//private:
//	//构造函数私有
//	Singleton(){}
//
//	static Singleton m_instance;
//};
//
//Singleton Singleton::m_instance;
//
//int main()
//{
//	Singleton* s = Singleton::GetInstance();
//	return 0;
//}

//懒汉模式
class Singleton
{
public:
	static Singleton* GetInstance()
	{
		if (m_instance == nullptr)
		{
			m_instance = new Singleton();
		}
		return m_instance;
	}

	//C++11 防拷贝
	Singleton(Singleton const&) = delete;
	Singleton& operator=(Singleton const&) = delete;
private:
	//构造函数私有
	Singleton() {}

	static Singleton* m_instance;
};

Singleton* Singleton::m_instance = nullptr;

int main()
{
	Singleton* s = Singleton::GetInstance();
	return 0;
}