#include <stdio.h>
int main()
{
    int n;
    int i = 0;
    while (scanf("%d", &n) == 1)
    {
        for (i = 0; i < n; i++)
        {
            int j = 0;
            for (j = 0; j <=i; j++)
            {
                if (i == n - 1 || j == 0 || i == j)
                    printf("* ");
                else
                    printf("  ");
            }
            printf("\n");
        }
    }
    return 0;
}