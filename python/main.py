import os

inputPath = input('请输入要搜索的路径:')
pattern = input('请输入要搜索的关键词:')

for dirpath, dirnames, filenames in os.walk(inputPath):
    for f in filenames:
        if pattern in f:
            print(f'{dirpath}/{f}')
