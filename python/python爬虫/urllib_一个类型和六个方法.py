import urllib.request

url = 'https://www.baidu.com'

response = urllib.request.urlopen(url)
#类型：HTTPResponse
#print(type(response))

#六个方法

#按字节读
# content = response.read(5)
# print(content)

#读取一行
# content = response.readline()
# print(content)

#读取多行
# content = response.readlines()
# print(content)

#返回状态码
#print(response.getcode())

#获得URL地址
#print(response.geturl())

#获取一个状态信息
print(response.getheaders())

