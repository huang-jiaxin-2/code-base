import urllib.request

#https UA反爬
url = 'https://www.baidu.com'

headers = {
    'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) '
                  'Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/115.0.0.0 '
}

#请求对象定制  def __init__(self, url, data=None, headers={},origin_req_host=None, unverifiable=False,method=None):
request = urllib.request.Request(url=url, headers=headers)

response = urllib.request.urlopen(request)

content = response.read().decode('utf-8')

print(content)
