
def reverseWords(s: str):
    tokens = s.split(' ')
    tokens.reverse()
    return ' '.join(tokens)


#print(reverseWords('I am a student.'))

def rotetesString(s, goal):
    if len(s) != len(goal):
        return False
    return goal in (s + s)


# print(rotetesString("abcde", "cdeab"))
# print(rotetesString("abcde", "edcba"))


def countPrefixes(words: list, s: str):
    count = 0
    for word in words:
        if s.startswith(word):
            count += 1
    return count


print(countPrefixes(['a', 'b', 'c', 'ab', 'bc', 'abc'], 'abc'))
print(countPrefixes(['a', 'a'], 'aa'))