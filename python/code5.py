# import random
#
# from pynput import keyboard
# from playsound import playsound
#
# soundlist = ['sound/1.mp3', 'sound/2.mp3', 'sound/3.mp3']
# count = 0
#
# #播放音频会导致按键卡顿
# def on_release(key):
#     print(key)
#     global count
#     count += 1
#     if count % 10 == 0:
#        # playsound('sound/1.mp3')
#         i = random.randint(0, len(soundlist) - 1)
#         playsound(soundlist[i])
#
#
# listener = keyboard.Listener(on_release=on_release)
# listener.start()
# listener.join()

import random
from pynput import keyboard
from playsound import playsound
from threading import Thread

soundlist = ['sound/1.mp3', 'sound/2.mp3', 'sound/3.mp3']
count = 0


def on_release(key):
    print(key)
    global count
    count += 1
    if count % 20 == 0:
        i = random.randint(0, len(soundlist) - 1)
        t = Thread(target=playsound, args=(soundlist[i], 1))
        t.start()


listener = keyboard.Listener(on_release=on_release)
listener.start()
listener.join()



