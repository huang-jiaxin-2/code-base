#pragma once
#include <iostream>
#include <queue>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <cstdio>
#include <Windows.h>
#include "Task.hpp"

using namespace std;

typedef void (*fun_t)(void*);
const int thread_num = 5;


template <class T>
class ThreadPool
{
public:
	bool Is_Empty()
	{
		return _task_queue.empty();
	}
	T getTask()
	{
		T t = _task_queue.front();
		_task_queue.pop();
		return t;
	}
public:
	//消费者
	static void handle(void* args)
	{
		ThreadPool* tp = static_cast<ThreadPool*> (args);
		while (true)
		{
			T task;
			/*获取任务*/
			{
				unique_lock<mutex> lck(tp->_mtx);
				while (tp->Is_Empty()) tp->_cond.wait(lck);
				task = tp->getTask();
			}
			task(this_thread::get_id());
		}

	}

	ThreadPool(int num = thread_num):_num(num)
	{
		_threads.resize(_num);
		for (int i = 0; i < _num; i++)
		{
			_threads[i] = thread(handle, this);
		}
	}
	
	void run()
	{
		for (int i = 0; i < _num; i++)
		{
			char nameBuffer[64];
			snprintf(nameBuffer, sizeof nameBuffer, "Thread-%d", i + 1);
			_name = nameBuffer;
			cout << _name << "[" << _threads[i].get_id() << "]" << ":" << "启动成功" << endl;
		}
	}

	//生产者
	void pushTask(const T& t)
	{
		lock_guard<mutex> lck(_mtx);
		_task_queue.push(t);
		_cond.notify_one();
	}

	~ThreadPool()
	{
		for (int i = 0; i < _num; i++)
		{
			if (_threads[i].joinable())
			{
				cout << _threads[i].get_id() << "->资源释放成功" << endl;
				_threads[i].detach();
			}
		}
	}

private:
	vector<thread> _threads;
	int _num;//线程个数
	queue<T> _task_queue;
	mutex _mtx;//互斥量
	condition_variable _cond;//条件变量
	string _name;//线程名
};