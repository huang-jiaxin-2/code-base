#include "ThreadPool.hpp"
#include "Task.hpp"
#include <ctime>

//int ticket = 1000;
////mutex mtx;
//
//void getTickets(int id, mutex& mtx)
//{
//	while (true)
//	{
//		lock_guard<mutex> lck(mtx);
//		if (ticket > 0)
//		{
//			cout << this_thread::get_id() << ":" << ticket << endl;
//			ticket--;
//		}
//		else
//		{
//			break;
//		}
//		this_thread::sleep_for(chrono::milliseconds(1));
//		/*Sleep(10);*/
//	}
//}
//
//int main()
//{
//	mutex mtx;
//	thread t1(getTickets, 1, ref(mtx));//直接传的话还是一个拷贝，要加ref修饰
//	thread t2(getTickets, 2, ref(mtx));
//
//	t1.join();
//	t2.join();
//	return 0;
//}

int main()
{
	srand(time(nullptr));
	cout << this_thread::get_id() << endl;
	ThreadPool<Task>* t = new ThreadPool<Task>();
	t->run();
	
	int time = 0;
	while(true)
	{
		int x = rand() % 100 + 1;
		this_thread::sleep_for(std::chrono::microseconds(3000));
		int y = rand() % 300 + 1;
		Task task(x, y, [](int x, int y)->int {return x + y; });
		cout << "制作任务完成: " << x << "+" << y << "=?" << endl;
		t->pushTask(task);
		this_thread::sleep_for(std::chrono::milliseconds(100));

		if (time++ == 20)
		{
			break;
		}
	}
	this_thread::sleep_for(std::chrono::seconds(3));
	delete t;
	return 0;
}

