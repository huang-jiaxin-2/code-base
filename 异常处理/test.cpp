#include <iostream>
#include <string>
#include <Windows.h>
using namespace std;


// 服务器开发中通常使用的异常继承体系
// 模拟服务器出现异常的情况
class Exception
{
public:
	Exception(const string& errmsg, int id)
		:_errmsg(errmsg)
		, _id(id)
	{}
	virtual string what() const
	{
		return _errmsg;
	}

	int getid()const
	{
		return _id;
	}
protected:
	string _errmsg;
	int _id;
};

////处理sql异常
//class SqlException : public Exception
//{
//public:
//	SqlException(const string& errmsg, int id, const string& sql)
//		:Exception(errmsg, id)
//		, _sql(sql)
//	{}
//	
//	virtual string what() const
//	{
//		string str = "SqlException:";
//		str += _errmsg;
//		str += "->";
//		str += _sql;
//		return str;
//	}
//private:
//	const string _sql;
//};
//
////处理缓存异常
//class CacheException : public Exception
//{
//public:
//	CacheException(const string& errmsg, int id)
//		:Exception(errmsg, id)
//	{}
//	virtual string what() const
//	{
//		string str = "CacheException:";
//		str += _errmsg;
//		return str;
//	}
//};
//
//处理http服务器异常
class HttpServerException : public Exception
{
public:
	HttpServerException(const string& errmsg, int id, const string& type)
		:Exception(errmsg, id)
		, _type(type)
	{}
	virtual string what() const
	{
		string str = "HttpServerException:";
		str += _type;
		str += ":";
		str += _errmsg;
		return str;
	}
private:
	const string _type;
};

////模拟出现异常的情况
//void SQLMgr()
//{
//	srand(time(0));
//	if (rand() % 7 == 0)
//	{
//		throw SqlException("权限不足", 300, "select * from name = '张三'");
//	}
//}
//
//void CacheMgr()
//{
//	srand(time(0));
//	if (rand() % 5 == 0)
//	{
//		throw CacheException("权限不足", 200);
//	}
//	else if (rand() % 6 == 0)
//	{
//		throw CacheException("数据不存在", 201);
//	}
//	SQLMgr();
//}

//void HttpServer()
//{
//	srand(time(0));
//	if (rand() % 3 == 0)
//	{
//		throw HttpServerException("网络错误", 100, "get");
//	}
//	else if (rand() % 4 == 0)
//	{
//		throw HttpServerException("权限不足", 101, "post");
//	}
//	CacheMgr();
//}

void SeedMsg(const string& s)
{
	// 出现网络错误重试发送5次
	srand(time(0));
	if (rand() % 3 == 0)
	{
		throw HttpServerException("网络错误", 100, "get");
	}
	else if (rand() % 4 == 0)
	{
		throw HttpServerException("权限不足", 101, "post");
	}

	cout << "发送成功:" << s << endl;
}

void HttpServer()
{
	// 出现网络错误重试5次
	string str = "可以做我女朋友吗？";
	int n = 5;
	while (n--)
	{
		try
		{
			SeedMsg(str);
			// 没有发生异常
			break;
		}
		catch (const Exception& e)
		{
			// 网络错误且在重试的5次内
			if (e.getid() == 100 && n > 0)
			{
				continue;
			}
			else
			{
				throw e; // 重新抛出
			}
		}
	}
}

int main()
{
	while (1)
	{
		Sleep(1000);
		try {
			HttpServer();
		}
		catch (const Exception& e)
		{
			// 多态
			cout << e.what() << endl;
		}
		catch (...)
		{
			cout << "未知错误" << endl;
		}
	}
	return 0;
}

//class Exception
//{
//public:
//	Exception(const string& errmsg, int errid)
//		:_errmsg(errmsg)
//		, _errid(errid)
//	{}
//	virtual string what() const
//	{
//		return _errmsg;
//	}
//
//	int GetErrid()const
//	{
//		return _errid;
//	}
//protected:
//	string _errmsg;
//	int _errid;
//};

//double Division(int a, int b)
//{
//	//当b == 0时抛异常
//	if (b == 0)
//	{
//		Exception e("除0错误", 1);
//		throw e;//会将拷贝对象抛给catch
//	}
//	else
//		return ((double)a / (double)b);
//}
//
//void Test()
//{
//	int a, b;
//	cin >> a >> b;
//	try
//	{
//		cout << Division(a, b) << endl;
//	}
//	catch(const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//}

//int main()
//{
//	while (1)
//	{
//		try
//		{
//			Test();
//		}
//		catch (const char* errmsg)
//		{
//			cout << errmsg << endl;
//		}
//		catch (...)
//		{
//			cout << "未知错误" << endl;
//		}
//	}
//	
//	return 0;
//}