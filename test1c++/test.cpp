#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main()
//{
//	printf("hello world");
//}

#include <iostream>//io流
#include <assert.h>
using namespace std;//std是c++标准库的命名空间
//1、项目中，尽量不要使用using namespace std
//2、日常练习用using namespace std
//3、项目中指定命名空间访问+展开常用
//using std::cout;
//using std::endl;
//int main()
//{
//	cout << "hello world" << endl;
//	return 0;
//}
//int main()
//{
//	//<<是流插入运算符
//	cout << "hello" << endl;//endl表示换行
//	cout << "hello" << '\n';
//	//自动识别类型
//	int i = 10;
//	double d = 11.23;
//	//>>是流提取
//	cin >> i ;
//	cout << i << endl;
//}

//::域作用限定符
//命名空间——防止命名冲突 （变量、函数、命名空间嵌套）
//命名空间域

//namespace N1
//{
//	int Add(int x, int y)
//	{
//		return x + y;
//	}
//}
//
//namespace N2
//{
//	int Add(int x, int y)
//	{
//		return x - y;
//	}
//}
//using namespace N1;
//
//int main()
//{
//	int a = Add(10, 20);
//	int b = N2::Add(10, 20);
//	int c = 10;
//	float d = 11.11;
//	cout << b << " " << "d=" << d << " ";
//	cout << a << " " << c << endl;
//}

//缺省参数
//void Fun(int a = 10)
//{
//	cout << a << endl;
//}
//int main()
//{
//	Fun(1);
//	Fun(2);
//	Fun(3);
//	Fun();
//
//	return 0;
//}

//全缺省参数
//void TestFun(int a = 10, int b = 20, int c = 30)//从左往右缺省
//{
//	cout << "a=" << a << endl;
//	cout << "b=" << b << endl;
//	cout << "c=" << c << endl<<endl;
//}

//半缺省--缺省部分参数
//void TestFunc(int a, int b = 20, int c = 30)//必须从右往左连续缺省，不能间隔
//{
//	cout << "a=" << a << endl;
//	cout << "b=" << b << endl;
//	cout << "c=" << c << endl << endl;
//}

//struct Stack
//{
//	int* a;
//	int size;
//	int capacity;
//};
//void StackInit(struct Stack* ps, int capacity = 4)
//{
//	ps->a = (int*)malloc(sizeof(int) * capacity);
//	assert(ps->a);
//	ps->size = 0;
//	ps->capacity = capacity;
//}
//缺省参数不能在函数声明和定义中同时出现
//分离参数时：声明给缺省参数
//int main()
//{
//	//TestFun(1);//从左往右给
//	//TestFun(1,2);//不能只传给b或c
//	//TestFun(1, 2, 3);
//	//TestFun();
//	/*TestFunc(1);
//	TestFunc(2);
//	TestFunc(1,2);
//	TestFunc(1,2,3);*/
//	struct Stack str1;
//	//知道我一定会插入100个数据，就可以显示传参数100
//	//提前开好空间，插入数据避免扩容
//	StackInit(&str1, 100);
//	return 0;
//}

//int main()
//{
//	const int c = 10;
//	const int& b = c;
//	cout << typeid(c).name() << endl;
//	cout << typeid(c).name() << endl;
//
//}

//函数重载
//返回值不同不够成重载
//调用时也无法区分
//int Add(int left, int right)
//{
//	return left + right;
//}
//double Add(double left, double right)
//{
//	return left + right;
//}
//int main()
//{
//	cout << Add(1, 2) << endl;
//	cout << Add(1.1, 2.2) << endl;
//	return 0;
//}

//函数重载的意义就是让用的地方很方便，就像用同一个函数一样
//void Swap(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//void Swap(double* p1, double* p2)
//{
//	double tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//int main()
//{
//	int a = 10, b = 20;
//	double c = 20.1, d = 10.2;
//	Swap(&a, &b);
//	Swap(&c, &d);
//	cout << a << endl;
//	cout << c << endl;
//	return 0;
//}

//引用 &
//给已知变量取别名
//共用同一块内存空间
//int main()
//{
//	int a = 10;
//	int& b = a;//引用
//	cout << &b << endl;//取地址 
//	cout << &a << endl;
//
//	a++;
//	cout << a << endl;
//
//	b++;
//	cout << b << endl;
//	return 0;
//}
//引用特性
//1、引用在定义时必须初始化
//2、一个变量可以有多个引用
//3、引用一旦引用一个实体，就不能再引用其它实体
//int main()
//{
//	int a = 10;
//	//1、引用在定义时必须初始化
//	//int& b = a;
//	
//	//2、一个变量可以有多个引用
//	//实际上都是同一个
//	/*int& b = a;
//	int& c = a;
//	int& d = a;*/
//
//	//3、引用一旦引用一个实体，就不能再引用其它实体
//	int& b = a;
//	int x = 20;
//	b = x;//意思是将x的值赋值给b,而不是b是a的别名
//	cout << b << endl;
//	cout << a << endl;
//
//	return 0;
//}

//引用的使用场景
//1、做参数——a、输出型参数 b、大对象传参，提高效率
// 2、做返回值——输出型返回值，调用者可以修改返回对象  b、减少拷贝，提高效率

//b、大对象传参，提高效率
//传值和传引用效率比较
//#include <time.h>
//struct A { int a[10000]; };
//void TestFunc1(A a) {}
//void TestFunc2(A& a) {}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
////a、输出型参数
////void Swap(int& a, int& b)
////{
////	int tmp = a;
////	a = b;
////	b = tmp;
////}
//int main()
//{
//	//int x = 1, y = 2;
//	//Swap(x, y);
//	//cout << x << " " << y << endl;
//	TestRefAndValue();
//	return 0;
//}

// 2、做返回值——输出型返回值，调用者可以修改返回对象  b、减少拷贝，提高效率
int& count()
{
	static int n = 0;
	n++;

	return n;
}
int main()
{
	int& ret = count();
	cout << ret << endl;
	cout << ret << endl;

	return 0;
}